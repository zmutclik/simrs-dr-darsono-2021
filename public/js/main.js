jQuery(document).ready(function() {
    moment().format();
    moment.locale('id');

    $(".hBack").on("click", function(e) {
        window.location.href = url_path;
    });

    $(document).ajaxStart(function() {
        $.LoadingOverlay("show");
    });
    $(document).ajaxStop(function() {
        $.LoadingOverlay("hide");
    });
});

function updatemyatoken(data) {
    $("." + myatoken).val(data)

}

function FailAjax() {
    // location.reload()
}

function SuccesAjax() {}