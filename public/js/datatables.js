$.fn.dataTable.ext.buttons.tambah = {
    text: 'Tambah',
    action: function(e, dt, node, config) {
        window.location.href = url_path + "/new";
    }
};

$.extend(true, $.fn.dataTable.defaults, {
    bSort: true,
    bAutoWidth: false,
    bJQueryUI: true,
    bProcessing: true,
    sPaginationType: "full_numbers",
    bServerSide: true,
    bPaginate: true,
    aLengthMenu: [
        [10, 25, 50, 100, 500, 1000],
        [10, 25, 50, 100, 500, 1000],
    ],
    sAjaxSource: url_path + "/Table",
    language: {
        url: window.location.origin + "/js/datatables-Indonesian-Alternative.json",
    },
    fnServerData: function(sSource, aoData, fnCallback) {
        aoData.push({ name: "bPaginate", value: true }); ////TABAHAN PENTING @SEMUT CILIK
        aoData.push({
            name: $("." + myatoken).attr("name"),
            value: $("." + myatoken).attr("value"),
        });
        $.ajax({ dataType: "json", type: "POST", url: sSource, data: aoData })
            .done(fnCallback)
            .fail(FailAjax)
            .always(DataTableMyatoken);
    },
    columnDefs: [{
        sClass: "center",
        searchable: false,
        orderable: false,
        bSortable: false,
        targets: -1,
        sWidth: "15%",
        render: function(data, type, row, meta) {
            return '<div class="btn-group" role="group"> <button type="button" class="btn btn-outline-secondary editor_remv">HAPUS</button> <button type="button" class="btn btn-outline-secondary editor_edit">KOREKSI</button> <button type="button" class="btn btn-outline-secondary editor_show">LIHAT</button> </div>'
        }
    }]
});


function form_edit(id) { window.location.href = url_path + "/" + id + "/edit"; }

function form_delete(id, backToHome = false) {
    swal({
            title: "Apakah Yakin?",
            text: "Apakah anda yakin untuk menghapus data ini !",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var data = {
                    [$("." + myatoken).attr('name')]: $("." + myatoken).attr('value')
                }
                $.ajax({ 'type': 'POST', 'url': url_path + "/" + id + "/delete", 'data': data })
                    .done(function(data) {
                        swal("Sukses Menghapus", {
                            icon: "success",
                        });
                        setTimeout(function() {
                            if (backToHome) window.location.href = url_path;
                            else oTable.ajax.reload();
                        }, 500);
                    }).fail(FailAjax).always(DataTableMyatoken);
            }
        });
}

function DataTableMyatoken(data) {
    updatemyatoken(data.token)
}