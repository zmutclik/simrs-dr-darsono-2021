jQuery(document).ready(function () {

    jQuery('ul.sf-menu').superfish({
        delay: 100,
        animation: {
            opacity: 'show',
            height: 'show'
        },
        speed: 'normal',
        cssArrows: true
    });

    $('ul.sf-menu > li > a').on('mouseover', function (event) {
        return false;
    });

    $('ul.sf-menu > li > a.menuparent').on('click', function (event) {
        // Close any open dropdown menus.
        $.each($(this), function (index, element) {
            $(element).parent().parent().find('li.menuparent > ul').addClass('sf-hidden');
        });
        // Open the clicked dropdown menu.
        $(this).next().removeClass('sf-hidden');
        event.preventDefault();
    });
});