var oTable;

jQuery(document).ready(function() {

    oTable = $("#data_table").DataTable({
        sDom: '<"H"Bfr>t<"F"ip>',
        buttons: ["pageLength", "tambah"],
        aoColumns: [
            { data: "kelompok" },
            { data: "nama" },
            { data: "type" },
            { data: "formula" },
            {
                data: "formula_prosen",
                render: $.fn.dataTable.render.number(',', '.', 3),
                className: 'right',
            },
            { data: "id" },
        ],
    });

    $("#data_table").on("click", "button.editor_show", function() {
        var data = oTable.row($(this).parents('tr')).data();
        window.location.href = url_path + "/" + data.id + "/show";
    });

    $("#data_table").on("click", "button.editor_edit", function() {
        var data = oTable.row($(this).parents('tr')).data();
        form_edit(data.id);
    });

    $("#data_table").on("click", "button.editor_remv", function() {
        var data = oTable.row($(this).parents('tr')).data();
        form_delete(data.id);
    });
});