var oTable;

jQuery(document).ready(function() {

    oTable = $("#data_table").DataTable({
        sDom: '<"H"Bfr>t<"F"ip>',
        buttons: ["pageLength", "tambah"],
        aoColumns: [
            { data: "pasien" },
            { data: "alamat_pasien" },
            { data: "tlp" },
            { data: "lahir" },
            { data: "id" },
        ],
    });

    $("#data_table").on("click", "button.editor_show", function() {
        var data = oTable.row($(this).parents('tr')).data();
        window.location.href = url_path + "/" + data.id + "/show";
    });

    $("#data_table").on("click", "button.editor_edit", function() {
        var data = oTable.row($(this).parents('tr')).data();
        form_edit(data.id);
    });

    $("#data_table").on("click", "button.editor_remv", function() {
        var data = oTable.row($(this).parents('tr')).data();
        form_delete(data.id);
    });

    $('#agama').select2({
        placeholder: "Agama",
    });

    $('#marital').select2({
        placeholder: "Status Kawin",
    });

    $('#pekerjaan').select2({
        placeholder: "Status Pekerjaan",
    });

    $('#pendidikan').select2({
        placeholder: "Pendidikan Trahir",
    });

    $('#suku_etnis').select2({
        placeholder: "Asal Suku / Etnis",
    });

    $('#bahasa').select2({
        placeholder: "Bahasa Utama Yang Digunakan",
    });

    $('#provinsi').select2({
        ajax: {
            url: url_path + "/Wilayah",
            dataType: 'json',
            data: function(params) {
                var query = {
                    kode: 2,
                    q: params.term,
                    parent: ''
                }
                return query;
            }
        },
        placeholder: "Provinsi",
    });

    $('#kabupaten').select2({
        ajax: {
            url: window.location.origin + "/Pasien/Wilayah/",
            dataType: 'json',
            data: function(params) {
                var query = {
                    kode: 5,
                    q: params.term,
                    parent: $('#provinsi').val()
                }
                return query;
            }
        },
        placeholder: "Kabupaten/Kota",
    });

    $('#kecamatan').select2({
        ajax: {
            url: window.location.origin + "/Pasien/Wilayah/",
            dataType: 'json',
            data: function(params) {
                var query = {
                    kode: 8,
                    q: params.term,
                    parent: $('#kabupaten').val()
                }
                return query;
            }
        },
        placeholder: "Kecamatan",
    });

    $('#kelurahan').select2({
        ajax: {
            url: window.location.origin + "/Pasien/Wilayah/",
            dataType: 'json',
            data: function(params) {
                var query = {
                    kode: 13,
                    q: params.term,
                    parent: $('#kecamatan').val()
                }
                return query;
            }
        },
        placeholder: "Kelurahan",
    });

    $('#provinsi').on('select2:select', function(e) {
        var data = e.params.data;
        $('#kode_wilayah').val(data.id)
        $('#kabupaten').val(null).trigger('change');
        $('#kecamatan').val(null).trigger('change');
        $('#kelurahan').val(null).trigger('change');
    });

    $('#kabupaten').on('select2:select', function(e) {
        var data = e.params.data;
        $('#kode_wilayah').val(data.id)
        $('#kecamatan').val(null).trigger('change');
        $('#kelurahan').val(null).trigger('change');
    });

    $('#kecamatan').on('select2:select', function(e) {
        var data = e.params.data;
        $('#kode_wilayah').val(data.id)
        $('#kelurahan').val(null).trigger('change');
    });

    $('#kelurahan').on('select2:select', function(e) {
        var data = e.params.data;
        $('#kode_wilayah').val(data.id)
    });

    $('#reservationdate').datetimepicker({
        format: 'DD MMM YYYY',
        viewMode: 'years',
    });

});