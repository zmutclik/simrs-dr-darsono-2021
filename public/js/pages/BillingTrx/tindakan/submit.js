$(document).ready(function () {

    $("#form").validate({
        rules: {
            tindakan_tanggal: {
                required: true,
            },
            tindakan: {
                required: true,
            },
            tindakan_jumlah: {
                required: true,
                number: true,
                min: 1
            },
            tindakan_biaya: {
                required: true,
                number: true,
                min: 1
            },
        },

        errorElement: "small",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");

            if ($(element).closest("div").hasClass("input-group"))
                error.insertAfter($(element).closest("div").next('.invalid-feedback'));
            else
                error.insertAfter($(element).next('.invalid-feedback'));
        },
        highlight: function (element, errorClass, validClass) {
            var elem = $(element);
            if (elem.hasClass("select2-hidden-accessible")) {
                $("#select2-" + elem.attr("id") + "-container").parent().addClass('is-invalid').removeClass("is-valid");
            } else
                $(element).addClass("is-invalid").removeClass("is-valid");
        },
        unhighlight: function (element, errorClass, validClass) {
            var elem = $(element);
            if (elem.hasClass("select2-hidden-accessible")) {
                $("#select2-" + elem.attr("id") + "-container").parent().addClass("is-valid").removeClass('is-invalid');
            } else
                $(element).addClass("is-valid").removeClass("is-invalid");
        }
    });


    $('#form').on('click', '#btn_submit', function () {

        if ($('#form').valid()) {
            $('#form').submit();
        }
    });

});