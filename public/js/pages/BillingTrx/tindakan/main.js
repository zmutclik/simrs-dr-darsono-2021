var oTable;

jQuery(document).ready(function () {
    var groupColumn = 0;
    oTable = $("#data_table").DataTable({
        sDom: '<"H"r>t<"F">',
        "ordering": false,
        aLengthMenu: [
            [9000],
            [9000],
        ],
        aoColumns: [
            { data: "tempat_layanan", "visible": false },
            { data: "tanggal", sWidth: "15%", sClass: "center" },
            {
                data: function (source, type, val) {
                    return "<table class=\"table table-borderless mb-0 mt-n1 mb-n1\"><tbody><tr><td class=\"p-0\"><strong>" + source.tindakan + "</strong></td></tr><tr><td class=\"p-0 text-muted\">" + source.pelaksana + "</td></tr></tbody></table>";
                }
            },
            { data: "qty", sWidth: "5%", sClass: "center" },
            { data: "sub_total", sWidth: "12%", sClass: "right", render: $.fn.dataTable.render.number(',', '.', 0) },
            { data: "id" },
        ],
        columnDefs: [{
            sClass: "center",
            searchable: false,
            orderable: false,
            bSortable: false,
            targets: -1,
            sWidth: "5%",
            render: function (data, type, row, meta) {
                return '<div class="btn-group" role="group"> <button type="button" class="btn btn-outline-secondary editor_remv">HAPUS</button> </div>'
            }
        }],
        "drawCallback": function (settings) {
            var api = this.api();
            var rows = api.rows({ page: 'current' }).nodes();
            var last = null;

            api.column(groupColumn, { page: 'current' }).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                        '<tr class="group"><td colspan="6"><b>' + group + '</b></td></tr>'
                    );

                    last = group;
                }
            });
        }
    });
    $('#tempat_layanan').select2({ placeholder: "Tempat Layanan", });
    $('#dokter_dpjp').select2({ placeholder: "Dokter Penanggung Jawab Pelayanan", });



    $.get(url_path + "/tindakan/",
        function (data) {
            $('#tindakan').select2({
                placeholder: "Tindakan",
                data: data,
            })
        });


    $('#tindakan').on('select2:select', function (e) {
        var data = e.params.data;
        // console.log(data)
        // form_tindakan_pelaksana
        $('#tindakan_jumlah').val('1');
        $('#tindakan_biaya').val(data.tarif);
        $('.form_tindakan_pelaksana').html("");
        $.get(window.location.origin + "/kunjungan/tindakan/" + data.uuid + "/tindakanKelompok/",
            function (data) {
                $.each(data, function (index, value) {
                    var d = $("<div></div>");
                    d.addClass("form-group row col-md-12");
                    var l = $("<label></label>");
                    l.addClass("col-sm-3 col-form-label");
                    l.attr("for", "tindakan_pelaksana[" + value.pelaksana_layanan.id + "]");
                    l.html(value.pelaksana_layanan.nama);
                    var di = $("<div></div>");
                    di.addClass("col-sm-9");
                    var s = $("<select></select>");
                    s.css('width', '100%');
                    s.attr('required', 'required');
                    s.attr("name", "tindakan_pelaksana[" + value.pelaksana_layanan.id + "]");
                    s.attr("id", "tindakan_pelaksana_" + value.pelaksana_layanan.id);
                    s.addClass("tindakan_pelaksana");
                    di.append(s);
                    d.append(l);
                    d.append(di);
                    s.select2({ placeholder: value.pelaksana_layanan.nama, data: value.options });
                    s.rules("add", { required: true, });
                    $('.form_tindakan_pelaksana').append(d);
                });
            });
    });


    $('#tindakan_pelaksana').select2({ placeholder: "Pelaksana Tindakan", });

});