var oTable;
var oTableSearchPasien;

jQuery(document).ready(function () {
    $('#tgl_mulai').val();

    oTableSearchPasien = $("#searchPasien_table").DataTable({
        sDom: '<"H"r>t<"F">',
        sAjaxSource: "/pasien/Table",
        aLengthMenu: [
            [10],
            [10],
        ],
        fnServerData: function (sSource, aoData, fnCallback) {
            aoData.push({ name: "bPaginate", value: true }); ////TABAHAN PENTING @SEMUT CILIK
            aoData.push({ name: "searchnama", value: $('#FormSearch').val() }); ////TABAHAN PENTING @SEMUT CILIK
            aoData.push({ name: "searchalamat", value: $('#FormSearchAlamat').val() }); ////TABAHAN PENTING @SEMUT CILIK
            aoData.push({
                name: $("." + myatoken).attr("name"),
                value: $("." + myatoken).attr("value"),
            });
            $.ajax({ dataType: "json", type: "POST", url: sSource, data: aoData })
                .done(fnCallback)
                .fail(FailAjax)
                .always(DataTableMyatoken);
        },
        aoColumns: [
            { data: "pasien" },
            { data: "alamat_pasien" },
            { data: "umur" },
            { data: "no_rm" },
        ],
        columnDefs: [{
            sClass: "center",
            searchable: false,
            orderable: false,
            bSortable: false,
            targets: -1,
            sWidth: "5%",
            render: function (data, type, row, meta) {
                return '<div class="btn-group" role="group"> <button type="button" class="btn btn-outline-secondary editor_lihat">LIHAT</button> </div>'
            }
        }],
    });

    $('#no_rm').keydown(function (e) {
        $(this).val($(this).val().replace(/[A-Za-z-.*+?^${}()|[\]\/\\]+$/, ""));
        if (e.keyCode == 13) {
            // window.location.href = url_path + "?norm=" + $(this).val();
            $.get(window.location.origin + "/pasien/uuid/" + $(this).val(),
                function (data) {
                    window.location.href = window.location.origin + "/kunjungan/daftar/" + data.uuid
                })
        }
    });

    $("#searchPasien_table").on("click", "button.editor_lihat", function () {
        var data = oTableSearchPasien.row($(this).parents('tr')).data();
        window.location.href = window.location.origin + "/kunjungan/daftar/" + data.uuid;
    });

    $("#FormSearchPasien").on("change", "input", function () {
        oTableSearchPasien.ajax.reload();
    });

    $('#masuk_diag').select2({
        ajax: {
            url: window.location.origin + "/ICD/",
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                }
                return query;
            }
        },
        placeholder: "ICD 10",
    });

    $('#id_penjamin').select2({ placeholder: "Penjamin Pasien", });
    $('#penjamin_kelas').select2({ placeholder: "Kelas Penjamin", });
    $('#no_rujukan').select2({
        ajax: {
            url: function (params) {
                return window.location.origin + "/vclaim/rujukan/search/" + $('#no_anggota').val();
            },
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                }
                return query;
            },
        },
        placeholder: "No Rujukan JKN",
        templateSelection: function (repo) {
            $('#tgl_rujukan').val(moment(repo.tglKunjungan, "YYYY-MM-DD h:mm:ss").format('LL'))
            return repo.full_name || repo.text;
        }
    });
    $('#skdp').select2({
        placeholder: "No SKDP/Kontrol",
        ajax: {
            url: function (params) {
                return window.location.origin + "/surat/skdp/search/" + $('#no_rm').val();
            },
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                }
                return query;
            },
        },
        templateSelection: function (repo) {
            return repo.full_name || repo.text;
        }
    });
    $('#skdp_dr_perujuk').select2({ placeholder: "Dokter Perujuk", });
    $('#lakaLantas').select2({ placeholder: "Penjamin Laka", });
    $('#kdPropinsi').select2({
        ajax: {
            url: window.location.origin + "/vclaim/referensi/propinsi/",
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                }
                return query;
            }
        },
        placeholder: "Propinsi",
    });
    $('#kdKabupaten').select2({
        ajax: {
            url: function (params) {
                return window.location.origin + "/vclaim/referensi/kabupaten/" + $('#kdPropinsi').val();
            },
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                }
                return query;
            }
        },
        placeholder: "Kabupaten/Kota",
    });
    $('#kdKecamatan').select2({
        ajax: {
            url: function (params) {
                return window.location.origin + "/vclaim/referensi/kecamatan/" + $('#kdKabupaten').val();
            },
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term,
                }
                return query;
            }
        },
        placeholder: "Kecamatan",
    });
    $('#noSepSuplesi').select2({ placeholder: "SEP Suplisi", });


    $('#masuk_asal').select2({ placeholder: "Asal Masuk", });
    $('#kasus').select2({ placeholder: "Kasus", });
    $('#id_tempat_layanan').select2({ placeholder: "Tempat Layanan", });
    // $('#id_tindakan').select2({ placeholder: "Tindakan Pendaftaran", });


    $('#reservationdate').datetimepicker({
        format: 'DD MMM YYYY HH:mm',
    });


    $("#panelpenjamin").on("change", "#id_penjamin", function () {
        if ($('#id_penjamin').val() === '2') {
            $('.panelbpjs').show()
            $.getJSON(window.location.origin + "/pasien/penjamin/" + $('#no_rm').val() + "/" + $('#id_penjamin').val(), function (data) {
                if (data.metaData.code === 200 && data.metaData.count > 0) {
                    $('#no_anggota').val(data.response.no_anggota);
                    $('#penjamin_jenis').val(data.response.nama);

                    $.get(window.location.origin + "/vclaim/peserta/" + data.response.no_anggota,
                        function (data) {
                            $('#penjamin_jenis').val(data.response.jenisPeserta.nama)
                        })
                    $.get(window.location.origin + "/vclaim/rujukan/peserta/" + data.response.no_anggota)
                }
            })
        } else {
            $('.panelbpjs').hide()
        }
        if ($("#isLakaLantas").is(':checked')) {
            $('.panelbpjslaka').show()
        } else {
            $('.panelbpjslaka').hide()
        }
    });

    $('#no_anggota').keydown(function (e) {
        $(this).val($(this).val().replace(/[A-Za-z-.*+?^${}()|[\]\/\\]+$/, ""));
        if (e.keyCode == 13) {
            $.get(window.location.origin + "/vclaim/peserta/" + $(this).val(),
                function (data) {
                    console.log(data.response)
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Nama di Billing Berbeda dgn Nama dari BPJS ( ' + data.response.nama + ' )!',
                        // footer: '<a href>Why do I have this issue?</a>'
                    })
                    $('#penjamin_jenis').val(data.response.jenisPeserta.nama)
                })

            $.get(window.location.origin + "/vclaim/rujukan/peserta/" + $(this).val())
        }
    });

    $("#panelpenjamin").on("click", "#isLakaLantas", function () {
        if ($("#isLakaLantas").is(':checked')) {
            $('.panelbpjslaka').show()
        } else {
            $('.panelbpjslaka').hide()
        }
    });

    setTimeout(function () {
        if ($('#no_rm').val().length === 6) {
            $('.form-btn').show()
        }
        if ($('#id_penjamin').val() === '2') {
            $('.panelbpjs').show()
        } else {
            $('.panelbpjs').hide()
        }
        if ($("#isLakaLantas").is(':checked')) {
            $('.panelbpjslaka').show()
        } else {
            $('.panelbpjslaka').hide()
        }
    }, 100);
});