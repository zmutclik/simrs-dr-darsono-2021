$(document).ready(function() {

    $("#form").validate({
        rules: {
            no_rm: {
                required: true,
                minlength: 6,
                maxlength: 6,
            },
            telp: true,
            id_penjamin: true,
            penjamin_kelas: true,
            no_anggota: {
                required: {
                    depends: dependsJKN
                },
                minlength: {
                    param: 13,
                    depends: dependsJKN
                },
                maxlength: {
                    param: 13,
                    depends: dependsJKN
                },
            },
            sep_rj: {
                required: {
                    depends: dependsJKN
                },
                minlength: {
                    param: 19,
                    depends: dependsJKN
                },
                maxlength: {
                    param: 19,
                    depends: dependsJKN
                },
            },
            no_rujukan: {
                required: {
                    depends: function() {
                        var fruits = ["1", "3", "24", "25"];

                        if ($('#id_penjamin').val() === '2')
                            if (!fruits.includes($('#id_tempat_layanan').val()))
                                return true;
                            else
                                return false;
                        else
                            return false;
                    }
                }
            },
            tglKejadian: {
                required: '#isLakaLantas:checked',
            },
            lakaLantas: {
                required: '#isLakaLantas:checked',
            },
            kdPropinsi: {
                required: '#isLakaLantas:checked',
            },
            kdKabupaten: {
                required: '#isLakaLantas:checked',
            },
            kdKecamatan: {
                required: '#isLakaLantas:checked',
            },
            noSepSuplesi: {
                required: '#suplesi:checked',
            },
        },
        messages: {
            no_rm: {
                required: "Pasien Harus dipilih Terlebih dahulu.",
                minlength: "Jumlah Karakter NORM harus 6",
                maxlength: "Jumlah Karakter NORM harus 6"
            },
            telp: {
                required: "Nomor Telp wajib Terisi.",
            },
            no_anggota: {
                required: "Nomor Anggota BPJS wajib Terisi.",
                minlength: "Jumlah Karakter harus 13",
                maxlength: "Jumlah Karakter harus 13"
            },
        },
        errorElement: "small",
        errorPlacement: function(error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");

            if ($(element).closest("div").hasClass("input-group"))
                error.insertAfter($(element).closest("div").next('.invalid-feedback'));
            else
                error.insertAfter($(element).next('.invalid-feedback'));
        },
        highlight: function(element, errorClass, validClass) {
            var elem = $(element);
            if (elem.hasClass("select2-hidden-accessible")) {
                $("#select2-" + elem.attr("id") + "-container").parent().addClass('is-invalid').removeClass("is-valid");
            } else
                $(element).addClass("is-invalid").removeClass("is-valid");
        },
        unhighlight: function(element, errorClass, validClass) {
            var elem = $(element);
            if (elem.hasClass("select2-hidden-accessible")) {
                $("#select2-" + elem.attr("id") + "-container").parent().addClass("is-valid").removeClass('is-invalid');
            } else
                $(element).addClass("is-valid").removeClass("is-invalid");
        }
    });

    $('select').on('change', function() { // when the value changes
        $('#form').valid(); // trigger validation on this element
    });

    $('#form').on('click', '#btn_submit', function() {
        if ($('#form').valid()) {
            $('#form').submit();
        }
    });

});

function dependsJKN() {
    if ($('#id_penjamin').val() === '2')
        return true;
    else
        return false;
}