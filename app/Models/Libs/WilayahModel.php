<?php

namespace App\Models\Libs;

use CodeIgniter\Model;

class WilayahModel extends Model
{
    protected $DBGroup          = 'DBReferensi';
    protected $table            = 'wilayah_2020';
    protected $primaryKey       = 'kode';
    protected $allowedFields    = [];
    protected $validationRules  = [];

    protected $useSoftDeletes   = false;
    protected $useTimestamps    = false;
    /*
protected $beforeInsert = ['CreateBy'];
protected $beforeDelete = ['DeleteBy'];
protected function CreateBy(array $data){$data['data']['created_by'] = user();return $data;}
protected function DeleteBy(array $data){$data['data']['deleted_by'] = user();return $data;}
*/
}
