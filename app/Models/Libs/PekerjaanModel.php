<?php

namespace App\Models\Libs;

use CodeIgniter\Model;

class PekerjaanModel extends Model
{
    protected $DBGroup          = 'DBReferensi';
    protected $table            = 'pekerjaan';
    protected $allowedFields    = [];
    protected $validationRules  = [];

    protected $useSoftDeletes   = false;
    protected $useTimestamps    = false;
    public $data            = [];
    protected $afterInsert  = ['_afterInsert'];
    /*
protected $beforeInsert = ['CreateBy'];
protected $beforeDelete = ['DeleteBy'];
protected function CreateBy(array $data){$data['data']['created_by'] = username();return $data;}
protected function DeleteBy(array $data){$data['data']['deleted_by'] = username();return $data;}
*/
    protected function _afterInsert(array $data)
    {
        $this->data = $data['data'];
    }


    public function options()
    {
        $options = $this->findAll();
        $data = [];
        $data[''] = '';
        foreach ($options as $value)
            $data[$value['nama']] = $value['nama'];
        return $data;
    }
}
