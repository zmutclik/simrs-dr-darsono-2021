<?php

namespace App\Models\Kunjungan;

use CodeIgniter\Model;

class TLModel extends Model
{
    protected $DBGroup          = 'DBTrx';
    protected $table            = 'kunjungan_tl';
    protected $allowedFields    = [];
    protected $validationRules  = [];

    protected $useSoftDeletes   = true;
    protected $useTimestamps    = true;

    protected $beforeInsert = ['CreateBy'];
    protected $beforeDelete = ['DeleteBy'];
    
    protected function CreateBy(array $data)
    {
        $data['data']['created_by'] = username();
        $data['data']['uuid'] = uuid();
        return $data;
    }
    protected function DeleteBy(array $data)
    {
        $data['data']['deleted_by'] = username();
        return $data;
    }
}
