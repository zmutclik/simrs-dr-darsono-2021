<?php

namespace App\Models\Kunjungan;

use CodeIgniter\Model;
use App\Models\Kunjungan\KunjKeuanganModel;
use App\Models\Kunjungan\KunjRMModel;

class KunjModel extends Model
{
    protected $DBGroup          = 'DBTrx';
    protected $table            = 'kunjungan';
    protected $allowedFields    = ['id','uuid', 'no_rm', 'is_pulang', 'tgl_mulai', 'tgl_selesai', 'id_penjamin', 'penjamin_kelas', 'perawatan_kelas', 'dpjp', 'is_baru', 'is_inap', 'status', 'keterangan'];
    protected $validationRules  = [
        'no_rm'             => 'required',
        'tgl_mulai'         => 'required',
        'perawatan_kelas'   => 'required',

    ];

    protected $useSoftDeletes   = true;
    protected $useTimestamps    = true;

    protected $beforeInsert = ['CreateBy'];
    protected $beforeDelete = ['DeleteBy'];
    protected $afterInsert  = ['_afterInsert'];

    public $data            = [];

    protected function CreateBy(array $data)
    {
        $data['data']['created_by'] = username();
        return $data;
    }
    protected function DeleteBy(array $data)
    {
        $data['data']['deleted_by'] = username();
        return $data;
    }

    protected function _afterInsert(array $data)
    {
        $this->data = $data;
    }
}
