<?php

namespace App\Models\Karyawan;

use CodeIgniter\Model;

class KaryawanModel extends Model
{
    protected $DBGroup = 'DBKaryawan';
    protected $table      = 'karyawan';

    protected $allowedFields = ['username', 'email', 'nip', 'nama', 'alamat', 'jkl', 'jabatan', 'tgl_masuk', 'img_profile'];

    protected $validationRules = [
        'nama'      => 'required|min_length[3]|max_length[50]',
        // 'email'     => 'required|valid_email|is_unique[karyawan.email]',
        'alamat'    => 'required',
        'jkl'       => 'required',
        'jabatan'   => 'required',
        'tgl_masuk' => 'required',
    ];

    protected $useSoftDeletes   = true;
    protected $useTimestamps    = true;

}
