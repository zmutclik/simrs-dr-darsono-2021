<?php

namespace App\Models;

use CodeIgniter\Model;

class AuthPermissionsModel extends Model
{
    protected $DBGroup          = '';
    protected $table            = 'auth_permissions';
    protected $allowedFields    = ['id', 'name', 'description'];
    protected $validationRules  = [
        'name'      => 'required|min_length[3]|max_length[50]',
    ];

    // protected $useSoftDeletes = true;
    // protected $useTimestamps = true;
}
