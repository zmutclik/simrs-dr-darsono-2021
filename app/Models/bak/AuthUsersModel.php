<?php

namespace App\Models;

use CodeIgniter\Model;

class AuthUsersModel extends Model
{
    protected $DBGroup          = '';
    protected $table            = 'users';
    protected $allowedFields    = [];
    protected $validationRules  = [];

    protected $useSoftDeletes   = false;
    protected $useTimestamps    = false;
}
