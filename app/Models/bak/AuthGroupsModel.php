<?php

namespace App\Models;

use CodeIgniter\Model;

class AuthGroupsModel extends Model
{
    protected $DBGroup          = '';
    protected $table            = 'auth_groups';
    protected $allowedFields    = ['id', 'name', 'description'];
    protected $validationRules  = [
        'name'    => 'required',
    ];
}
