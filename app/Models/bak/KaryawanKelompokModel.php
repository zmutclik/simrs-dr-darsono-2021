<?php

namespace App\Models;

use CodeIgniter\Model;

class KaryawanKelompokModel extends Model
{
    protected $DBGroup          = 'DBMaster';
    protected $table            = 'karyawan_kelompok';
    protected $allowedFields    = ['id', 'nama', 'is_khusus'];
    protected $validationRules  = [
        'nama'    => 'required',
    ];

    protected $useSoftDeletes   = true;
    protected $useTimestamps    = true;
}
