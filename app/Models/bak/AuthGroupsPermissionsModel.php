<?php

namespace App\Models;

use CodeIgniter\Model;

class AuthGroupsPermissionsModel extends Model
{
    protected $DBGroup          = '';
    protected $table            = 'auth_groups_permissions';
    protected $allowedFields    = ['id', 'group_id', 'permission_id'];
    protected $validationRules  = [
        'group_id'    => 'required',
        'permission_id'    => 'required',
    ];

    protected $useSoftDeletes   = false;
    protected $useTimestamps    = false;
}
