<?php

namespace App\Models;

use CodeIgniter\Model;

class AuthGroupsUsersModel extends Model
{
    protected $DBGroup          = '';
    protected $table            = 'auth_groups_users';
    protected $allowedFields    = ['group_id', 'user_id'];
    protected $validationRules  = [
        'group_id'    => 'required',
        'user_id'    => 'required',
    ];

    protected $useSoftDeletes   = false;
    protected $useTimestamps    = false;
}
