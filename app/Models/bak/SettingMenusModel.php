<?php

namespace App\Models;

use CodeIgniter\Model;

class SettingMenusModel extends Model
{
    protected $DBGroup          = '';
    protected $table            = 'menus';
    protected $allowedFields    = ['id', 'parent', 'name', 'link', 'icon', 'slug', 'number'];
    protected $validationRules  = [
        'name'      => 'required|min_length[3]|max_length[50]',
        'link'      => 'required',
    ];

    protected $useSoftDeletes   = true;
    protected $useTimestamps    = true;
}
