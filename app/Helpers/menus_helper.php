<?php

if (!function_exists('menus_build')) {
    /**
     * Searches an array through dot syntax. Supports
     * wildcard searches, like foo.*.bar
     *
     * @param string $index
     * @param array  $array
     *
     * @return mixed|null
     */
    function menus_build()
    {
        $db = \Config\Database::connect();
        $query = $db->query('SELECT * from menus where `active`="Y" and deleted_at is null order by `number`');
        $results = $query->getResultArray();

        return menus_generate($results);
    }
}

if (!function_exists('menus_generate')) {
    function menus_generate($rows, $parent = 0)
    {
        if ($parent == 0)
            $result = '<ul class="sf-menu sf-js-enabled sf-arrows navbar-nav mr-auto">';
        else
            $result = "<ul>";


        foreach ($rows as $row) {

            if ($row['parent'] == $parent) {

                $is_have_child = menus_children($rows, $row['id']);

                $url   = (empty($row['link'])) ? '#' : site_url($row['link']);
                $li    = '<li class="nav-item">';
                $aLink = '<a class="nav-link" href="' . $url . "\">{$row['name']}</a>";

                if ($parent > 0)
                    $aLink = '<a class="dropdown-item" href="' . $url . "\">{$row['name']}</a>";

                if ($is_have_child) {
                    $li    = '<li class="nav-item dropdown">';
                    $aLink = '<a class="nav-link dropdown-toggle" href="' . $url . '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' . $row['name'] . '</a>';
                    if ($parent > 0)
                        $aLink = '<a class="dropdown-item dropdown-toggle" href="' . $url . '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' . $row['name'] . '</a>';
                }

                $result .= $li . $aLink;

                if ($is_have_child) {
                    $result .= menus_generate($rows, $row['id']);
                }

                $result .= "</li>";
            }
        }
        $result .= "</ul>";
        return $result;
    }
}

if (!function_exists('menus_children')) {
    function menus_children($rows, $id)
    {
        foreach ($rows as $row)
            if ($row['parent'] == $id)
                return true;
        return false;
    }
}
