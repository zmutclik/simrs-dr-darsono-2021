<?php

if (!function_exists('my_uri')) {
    function my_uri($path_tambahan = '')
    {
        $request = \Config\Services::request();

        $patern_uri     = ['save', 'edit', 'new', 'show'];
        $patern_uri_id  = ['edit',  'show'];
        $data_uri       = base_url(uri_string());
        $TotalSegments  = $request->uri->getTotalSegments();
        $lastSegment    = $request->uri->setSilent()->getSegment($TotalSegments);

        if (in_array($lastSegment, $patern_uri_id))
            $TotalSegments = $TotalSegments - 1;

        if (in_array($lastSegment, $patern_uri)) {
            // $data_uri   = base_url($request->uri->getSegment(1) . '/' . $request->uri->getSegment(2));
            for ($i = 1; $i < $TotalSegments; $i++) {
                if ($i == 1)
                    $data_uri   = $request->uri->getSegment(1);
                else
                    $data_uri   = $data_uri . '/' . $request->uri->getSegment($i);
            }
            $data_uri =  base_url($data_uri);
        }

        if (empty($path_tambahan))
            return $data_uri;
        else
            return $data_uri . '/' . $path_tambahan;
    }
}
