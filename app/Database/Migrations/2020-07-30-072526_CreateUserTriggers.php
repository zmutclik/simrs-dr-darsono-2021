<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateUserTriggers extends Migration
{
	public function up()
	{
		$this->db->query("
		CREATE DEFINER=`root`@`127.0.0.1` TRIGGER `_users_before_insert` 
		AFTER INSERT ON `users` FOR EACH ROW 
			INSERT INTO `db_master`.`karyawan` (`id`, `username`,created_at) VALUES (NEW.id, NEW.username, NEW.created_at);
");
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->db->query("DROP TRIGGER _users_before_insert;");
	}
}
