<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateKaryawanTables extends Migration
{
	protected $DBGroup = 'DBMaster';

	public function up()
	{

		$fields = [
			'id'          => [
				'type'           => 'INT',
				'unsigned'       => true,
				'auto_increment' => true
			],
			'username'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
				'unique'         => true,
			],
			'email'      => [
				'type'           => 'VARCHAR',
				'constraint'     => 100,
				'unique'         => true,
			],
			'nip' => [
				'type'           	=> 'VARCHAR',
				'constraint'     	=> 100,
			],
			'nama' => [
				'type'           	=> 'VARCHAR',
				'constraint'     	=> 100,
			],
			'alamat' => [
				'type'           	=> 'VARCHAR',
				'constraint'     	=> 255,
			],
			'jkl' => [
				'type'           	=> 'CHAR',
				'constraint'     	=> 2,
			],
			'lahir_tgl' => [
				'type'           	=> 'DATE'
			],
			'lahir_tempat' => [
				'type'           	=> 'VARCHAR',
				'constraint'     	=> 100,
			],
			'agama' => [
				'type'           	=> 'VARCHAR',
				'constraint'     	=> 50,
			],
			'jabatan' => [
				'type'           	=> 'VARCHAR',
				'constraint'     	=> 50,
			],
			'gaji' => [
				'type'           	=> 'DOUBLE',
			],
			'no_rekening' => [
				'type'           	=> 'VARCHAR',
				'constraint'     	=> 50,
			],
			'tgl_masuk' => [
				'type'           	=> 'DATE',
			],
			'img_profile' => [
				'type'           	=> 'VARCHAR',
				'constraint'     	=> 255,
			],
			'created_at' => [
				'type'           	=> 'DATETIME',
				'null'           => true,
			],
			'updated_at' => [
				'type'           	=> 'DATETIME',
				'null'           => true,
			],
			'deleted_at' => [
				'type'           	=> 'DATETIME',
				'null'           => true,
			],
		];

		$this->forge->addField($fields);
		$this->forge->addKey('id', true);
		$this->forge->addKey('jabatan');
		$this->forge->createTable('karyawan');

	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('karyawan');
	}
}
