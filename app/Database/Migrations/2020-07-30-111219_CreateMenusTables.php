<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateMenusTables extends Migration
{
	public function up()
	{
		$fields = [
			'id'          => [
				'type'           => 'INT',
				'constraint'	 => 10,
				'auto_increment' => true,
			],
			'parent'          => [
				'type'           => 'INT',
				'constraint'	 => 10,
				'null'			 => true,
			],
			'name'          => [
				'type'           => 'VARCHAR',
				'constraint'	 => 50,
			],
			'link'          => [
				'type'           => 'VARCHAR',
				'constraint'	 => 255,
			],
			'icon'          => [
				'type'           => 'VARCHAR',
				'constraint'	 => 50,
			],
			'slug'          => [
				'type'           => 'VARCHAR',
				'constraint'	 => 50,
			],
			'number'          => [
				'type'           => 'INT',
			],
			'active'          => [
                'type'           => 'ENUM',
                'constraint'     => ['Y', 'N'],
                'default'        => 'Y',
			],
		];


		$this->forge->addField($fields);
		$this->forge->addKey('id', true);
		// $this->forge->addForeignKey('parent','menus','id');
		$this->forge->createTable('menus');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('menus');
	}
}
