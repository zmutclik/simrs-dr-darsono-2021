<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class MenusDefault extends Seeder
{
	public function run()
	{
		$data = array(
			array(
				"id" => 1,
				"parent" => NULL,
				"name" => "HOME",
				"link" => "#",
				"icon" => "",
				"slug" => "",
				"number" => 1,
				"active" => "Y",
			),
			array(
				"id" => 2,
				"parent" => NULL,
				"name" => "SETTING",
				"link" => "#",
				"icon" => "",
				"slug" => "",
				"number" => 2,
				"active" => "Y",
			),
			array(
				"id" => 3,
				"parent" => NULL,
				"name" => "LOGOUT",
				"link" => "logout",
				"icon" => "",
				"slug" => "",
				"number" => 3,
				"active" => "Y",
			),
			array(
				"id" => 4,
				"parent" => 2,
				"name" => "USERS",
				"link" => "#",
				"icon" => "",
				"slug" => "",
				"number" => 1,
				"active" => "Y",
			),
			array(
				"id" => 5,
				"parent" => 4,
				"name" => "PROFIL",
				"link" => "profile",
				"icon" => "",
				"slug" => "",
				"number" => 1,
				"active" => "Y",
			),
		);

		foreach ($data as $item)
			$this->db->table('menus')->insert($item);
	}
}
