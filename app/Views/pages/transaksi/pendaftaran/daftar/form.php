<?= $this->extend('template/index') ?>

<?= $this->section('content_header') ?>
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h3">Pendaftaran Rawat Jalan / IGD</h1>
    <div class="btn-toolbar mb-2 mb-md-0"></div>
</div>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <?= form_open($urlForm, $form['form']) ?>
                <input type="hidden" class="<?= md5(csrf_hash()) ?>" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Pasien</h3>
                        <div class="card-tools"><button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fas fa-minus"></i></button><button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove"><i class="fas fa-times"></i></button></div>
                    </div>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group row col-md-6">
                                <?= form_label('NORM', 'no_rm',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="input-group col-sm-3">
                                    <?= form_input($form['no_rm']) ?>
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="button" data-toggle="modal" data-target="#FormSearchPasien" style="border: 1px solid #ced4da;">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="invalid-feedback"><?= esc($errors["no_rm"]) ?></div>
                            </div>

                            <div class="form-group row col-md-6 ml-3">
                                <?= form_label('Telp', 'telp',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['telp']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["telp"]) ?></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group row col-md-6">
                                <?= form_label('Nama', 'nama',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['nama']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["nama"]) ?></div>
                                </div>
                            </div>

                            <div class="form-group row col-md-6 ml-3">
                                <?= form_label('Alamat', 'alamat',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['alamat']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["alamat"]) ?></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Kunjungan</h3>
                        <div class="card-tools"><button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fas fa-minus"></i></button><button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove"><i class="fas fa-times"></i></button></div>
                    </div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group row col-md-12">
                                    <?= form_label('Tanggal', 'tgl_mulai',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">

                                        <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                            <?= form_input($form['tgl_mulai']) ?>
                                            <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                        <div class="invalid-feedback"><?= esc($errors["tgl_mulai"]) ?></div>
                                    </div>
                                </div>

                                <div class="form-group row col-md-12">
                                    <?= form_label('Asal Masuk', 'masuk_asal',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_dropdown($form['masuk_asal'], $masuk_asal_options, $form['masuk_asal']['value']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["masuk_asal"]) ?></div>
                                    </div>
                                </div>

                                <div class="form-group row col-md-12">
                                    <?= form_label('Diag Masuk', 'masuk_diag',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_dropdown($form['masuk_diag'], $masuk_diag_options, $form['masuk_diag']['value']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["masuk_diag"]) ?></div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-6">
                                <div class="form-group row">
                                    <?= form_label('Kasus', 'kasus',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_dropdown($form['kasus'], $kasus_options, $form['kasus']['value']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["kasus"]) ?></div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <?= form_label('Tempat Layanan', 'id_tempat_layanan',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_dropdown($form['id_tempat_layanan'], $id_tempat_layanan_options, $form['id_tempat_layanan']['value']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["id_tempat_layanan"]) ?></div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <?= form_label('Tindakan', 'id_tindakan',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_dropdown($form['id_tindakan'], $id_tindakan_options, $form['id_tindakan']['value']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["id_tindakan"]) ?></div>
                                    </div>
                                </div>

                            </div>

                            <div class="row col-12 form-btn" style="display: none;">
                                <div class="col-11"></div>
                                <div class="col-1 float-right">
                                    <?= form_button($form['buttons']['submit']) ?>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <?= form_close() ?>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="FormSearchPasien" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pencarian Pasien</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="FormSearch" class="col-sm-2 col-form-label">Penelusuran</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="FormSearch">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="FormSearchAlamat" class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="FormSearchAlamat">
                    </div>
                </div>
                <table id="searchPasien_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>PASIEN</th>
                            <th>ALAMAT</th>
                            <th>UMUR</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="dataTables_empty">Loading data from server</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection() ?>