<?= $this->extend("template/index") ?>

<?= $this->section('content_header') ?>
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h3">Tindakan</h1>
    <div class="btn-toolbar mb-2 mb-md-0"></div>
</div>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Tindakan</h3>
                        <div class="card-tools"><button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fas fa-minus"></i></button><button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove"><i class="fas fa-times"></i></button></div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <?= form_open($urlForm, $form['form']) ?>
                            <div class="form-group row">
                                <?= form_label('Kelompok Tindakan', 'id_kelompok',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_dropdown($form['id_kelompok'], $id_kelompok_options, $form['id_kelompok']['value']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["id_kelompok"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Kode Panggil', 'kode_panggil',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['kode_panggil']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["kode_panggil"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Nama', 'nama',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['nama']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["nama"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Tipe Kelas', 'type_kelas',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_dropdown($form['type_kelas'], $type_kelas_options, $form['type_kelas']['value']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["type_kelas"]) ?></div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <?= form_label('Tarif Kls.3', 'tarif_3',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['tarif_3']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["tarif_3"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Tarif Kls.2', 'tarif_2',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['tarif_2']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["tarif_2"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Tarif Kls.1', 'tarif_1',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['tarif_1']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["tarif_1"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Tarif Kls.VIP', 'tarif_vip',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['tarif_vip']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["tarif_vip"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="offset-sm-2 col-sm-10">
                                    <?= form_button($form['buttons']['back']) ?>
                                    <?= form_button($form['buttons']['submit']) ?>
                                </div>
                            </div>

                            <?= form_close() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>