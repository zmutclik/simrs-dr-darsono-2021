<?= $this->extend('template/index') ?>

<?= $this->section('content_header') ?>
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h3">Tindakan Formula</h1>
    <div class="btn-toolbar mb-2 mb-md-0"></div>
</div>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">List Tindakan Formula</h3>
                    </div>
                    <div class="card-body">
                        <table id="data_table" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>KELOMPOK</th>
                                    <th>NAMA</th>
                                    <th>TYPE</th>
                                    <th>FORMULA</th>
                                    <th>PROSEN</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="dataTables_empty">Loading data from server</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>