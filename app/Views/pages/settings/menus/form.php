<?= $this->extend("template/index") ?>

<?= $this->section('content_header') ?>
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h3">Menus</h1>
    <div class="btn-toolbar mb-2 mb-md-0"></div>
</div>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Menu</h3>
                        <div class="card-tools"><button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fas fa-minus"></i></button><button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove"><i class="fas fa-times"></i></button></div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <?= form_open($urlForm, $form['form']) ?>
                            <div class="form-group row">
                                <?= form_label('Nama', 'name',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['name']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["name"]) ?></div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <?= form_label('Menu Parent', 'parent',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_dropdown($form['parent'], $parent_options, $form['parent']['value']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["parent"]) ?></div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <?= form_label('Link/URL', 'link',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['link']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["link"]) ?></div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <?= form_label('Urutan Menu', 'number',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['number']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["number"]) ?></div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="offset-sm-2 col-sm-10">
                                    <?= form_button($form['buttons']['back']) ?>
                                    <?= form_button($form['buttons']['submit']) ?>
                                </div>
                            </div>

                        </div>

                        <?= form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<?= $this->endSection() ?>