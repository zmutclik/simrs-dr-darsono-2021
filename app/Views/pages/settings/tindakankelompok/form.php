<?= $this->extend("template/index") ?>

<?= $this->section('content_header') ?>
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h3">Tindakan Kelompok</h1>
    <div class="btn-toolbar mb-2 mb-md-0"></div>
</div>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Tindakan Kelompok</h3>
                        <div class="card-tools"><button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fas fa-minus"></i></button><button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove"><i class="fas fa-times"></i></button></div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <?= form_open($urlForm, $form['form']) ?>
                            <div class="form-group row">
                                <?= form_label('Jenis Tindakan', 'jenis_tindakan',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_dropdown($form['jenis_tindakan'], $jenis_tindakan_options, $form['jenis_tindakan']['value']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["jenis_tindakan"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Nama', 'nama',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['nama']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["nama"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="offset-sm-2 col-sm-10">
                                    <?= form_button($form['buttons']['back']) ?>
                                    <?= form_button($form['buttons']['submit']) ?>
                                </div>
                            </div>

                            <?= form_close() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>