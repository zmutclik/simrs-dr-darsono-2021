<?= $this->extend("template/index") ?>

<?= $this->section('content_header') ?>
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h3">Data Karyawan</h1>
    <div class="btn-toolbar mb-2 mb-md-0"></div>
</div>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Karyawan</h3>
                        <div class="card-tools"><button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fas fa-minus"></i></button><button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove"><i class="fas fa-times"></i></button></div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <?= form_open($urlForm, $form['form']) ?>
                            <div class="form-group row">
                                <?= form_label('Nama Lengkap', 'nama',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['nama']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["nama"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Alamat', 'alamat',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['alamat']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["alamat"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Jenis Kelamin', 'FieldName',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <div class="custom-control custom-radio">
                                        <?= form_radio($form['jklL']) ?>
                                        <?= form_label('Laki-laki', 'jklL',  ['class' => 'form-check-label']); ?>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <?= form_radio($form['jklP']) ?>
                                        <?= form_label('Perempuan', 'jklP',  ['class' => 'form-check-label']); ?>
                                    </div>
                                    <div class="invalid-feedback"><?= esc($errors["jkl"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Jabatan', 'jabatan',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['jabatan']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["jabatan"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Tanggal Masuk', 'tgl_masuk',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                        <?= form_input($form['tgl_masuk']) ?>
                                        <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                    <div class="invalid-feedback"><?= esc($errors['tgl_masuk']) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="offset-sm-2 col-sm-10">
                                    <?= form_button($form['buttons']['back']) ?>
                                    <?= form_button($form['buttons']['submit']) ?>
                                </div>
                            </div>

                            <?= form_close() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>