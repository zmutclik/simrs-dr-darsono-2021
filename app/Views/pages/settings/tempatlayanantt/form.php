<?= $this->extend("template/index") ?>

<?= $this->section('content_header') ?>
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h3">Kamar Tempat Layanan</h1>
    <div class="btn-toolbar mb-2 mb-md-0"></div>
</div>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form Kamar</h3>
                        <div class="card-tools"><button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fas fa-minus"></i></button><button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove"><i class="fas fa-times"></i></button></div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <?= form_open($urlForm, $form['form']) ?>
                            <div class="form-group row">
                                <?= form_label('Tempat Layanan', 'id_tl',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_dropdown($form['id_tl'], $id_tl_options, $form['id_tl']['value']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["id_tl"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Nama', 'nama',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['nama']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["nama"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Kelas Kamar', 'kelas',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_dropdown($form['kelas'], $kelas_options, $form['kelas']['value']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["kelas"]) ?></div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <?= form_label('TT Bayangan', 'is_tt_bayangan',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <div class="form-check">
                                        <?= form_checkbox($form['is_tt_bayangan'], 1, isset($form['is_tt_bayangan']['value'])) ?>
                                        <?= form_label('Check Kelompok Khusus', 'is_tt_bayangan',  ['class' => 'form-check-label']); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="offset-sm-2 col-sm-10">
                                    <?= form_button($form['buttons']['back']) ?>
                                    <?= form_button($form['buttons']['submit']) ?>
                                </div>
                            </div>

                            <?= form_close() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>