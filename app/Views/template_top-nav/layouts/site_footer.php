<footer class="footer mt-auto py-3 text-right " style="box-shadow: 0 -5px 5px -5px rgba(0,0,0,.075)!important;">
    <div class="container-fluid">
        <span class="text-black-50 p-3 mb-5 bg-white "><small>Designed and built with all the love in the world by
                <a href="https://www.instagram.com/zmutclik/" class="text-reset">Fahrudin.Hariadi</a>@<a
                    href="http://rsud.pacitankab.go.id/" class="text-reset">RSUD dr DARSONO</a></small></span>
    </div>

</footer>