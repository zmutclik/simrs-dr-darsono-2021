<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?= csrf_meta() ?>
    
    <?php foreach ($link_tag as $item) : ?>
        <?= link_tag($item) ?>
    <?php endforeach ?>

    <title>SIMRS2021</title>
</head>