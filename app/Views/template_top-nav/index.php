<!doctype html>
<html lang="id" class="h-100">

<?= view('template/layouts/site_head') ?>

<body class="d-flex flex-column h-100">

    <?= view('template/layouts/site_header') ?>

    <main role="main" class="flex-shrink-0">

        <div class="container-fluid">
            <div class="row">

                <main role="main" class="col-md-12 ml-sm-auto flex-fill">

                    <?= $this->renderSection('content_header') ?>
                    <?= $this->renderSection('content') ?>

                </main>
            </div>
        </div>
    </main>




    <?= view('template/layouts/site_footer') ?>
    <script type="text/javascript"> var url_path = "<?= $uri ?>"; var myatoken = "<?= md5(csrf_hash()) ?>";</script>
    <?= view('template/layouts/site_scripts') ?>
    <input type="hidden" class="<?= md5(csrf_hash()) ?>" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>">
</body>

</html>