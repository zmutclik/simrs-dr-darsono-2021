
<header>
    <nav class="main-header navbar navbar-expand navbar-dark bg-dark">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>

        <?= $menus ?>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Messages Dropdown Menu -->
            <?= view('template/layouts/top_navbar_user') ?>
        </ul>
    </nav>
</header>