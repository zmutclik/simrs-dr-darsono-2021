<li class="nav-item dropdown user-menu">
    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
        <img src="<?= site_url($karyawan['img_profile']) ?>" class="user-image img-circle elevation-2" alt="User Image" onerror="this.onerror=null;this.src='<?= site_url('img/avatar2.png') ?>';">
        <span class="d-none d-md-inline"><?= $karyawan['nama'] ?></span>
    </a>
    <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right text-light">
        <!-- User image -->
        <li class="user-header bg-dark">
            <img src="<?= site_url($karyawan['img_profile']) ?>" class="img-circle elevation-2" alt="User Image" onerror="this.onerror=null;this.src='<?= site_url('img/avatar2.png') ?>';">

            <p>
                <?= $karyawan['nama'] ?> - <?= $karyawan['jabatan'] ?>
                <small>Bekerja dari <?= $karyawan['since'] ?></small>
            </p>
        </li>
</li>
<!-- Menu Footer-->
<li class="user-footer">
    <a href="<?= site_url('profile') ?>" class="btn btn-default text-dark btn-flat">Profile</a>
    <a href="<?= site_url('logout') ?>" class="btn btn-default text-dark btn-flat float-right">Sign out</a>
</li>