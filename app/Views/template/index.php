<!DOCTYPE html>
<html>

<?= view('template/layouts/site_head') ?>

<body class="hold-transition sidebar-mini sidebar-collapse">
  <!-- Site wrapper -->
  <div class="wrapper">
    <!-- Navbar -->
    <?= view('template/layouts/top_navbar') ?>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <?= view('template/layouts/left_sidebar') ?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <?= $this->renderSection('content_header') ?>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <?= $this->renderSection('content') ?>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


    <?= view('template/layouts/footer') ?>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->


  <script type="text/javascript">
    var url_path = "<?= $uri ?>";
    var myatoken = "<?= md5(csrf_hash()) ?>";
  </script>
  <?= view('template/layouts/site_scripts') ?>
  <input type="hidden" class="<?= md5(csrf_hash()) ?>" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>">
</body>

</html>