<?php

/**
 * Settings routes file.
 */

$routes->group('', ['namespace' => 'Settings\Controllers'], function($routes) {
	// PROFILE
	$routes->get('profile', 'ProfileController::index');
	$routes->post('profile/update', 'ProfileController::update');
});

