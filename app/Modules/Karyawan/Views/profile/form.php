<?= $this->extend('template/index') ?>

<?= $this->section('content_header') ?>
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h3">Profile</h1>
    <div class="btn-toolbar mb-2 mb-md-0">
    </div>
</div>
<?= $this->endSection() ?>


<?= $this->section('content') ?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img src="<?= site_url($karyawan['img_profile']) ?>" class="profile-user-img img-fluid img-circle" alt="User profile picture" onerror="this.onerror=null;this.src='img/avatar2.png';">
                        </div>

                        <h3 class="profile-username text-center"><?= $karyawan['nama'] ?></h3>

                        <p class="text-muted text-center"><?= $karyawan['jabatan'] ?></p>

                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

                <!-- About Me Box -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">About Me</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        -
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Settings</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                                <i class="fas fa-times"></i></button>
                        </div>
                    </div>

                    <!-- /.card-header -->

                    <div class="card-body">
                        <div class="tab-content">
                            <div class="active tab-pane" id="settings">
                                <?= form_open('profile/update', ['class' => 'needs-validation']); ?>
                                <div class="form-group row">
                                    <?= form_label('Nama Lengkap', 'nama',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_input($form['nama']) ?>
                                        <div class="invalid-feedback"><?= esc($errors['nama']) ?></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <?= form_label('Alamat', 'alamat',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_input($form['alamat']) ?>
                                        <div class="invalid-feedback"><?= esc($errors['alamat']) ?></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Jenis Kelamin</label>
                                    <div class="custom-control custom-radio">
                                        <?= form_radio($form['jklL']) ?>
                                        <?= form_label('Laki-laki', 'jklL',  ['class' => 'form-check-label']); ?>
                                    </div>
                                    <div class="custom-control custom-radio">
                                        <?= form_radio($form['jklP']) ?>
                                        <?= form_label('Perempuan', 'jklP',  ['class' => 'form-check-label']); ?>
                                    </div>
                                    <div class="invalid-feedback"><?= esc($errors['jkl']) ?></div>
                                </div>
                                <div class="form-group row">
                                    <?= form_label('Jabatan', 'jabatan',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_input($form['jabatan']) ?>
                                        <div class="invalid-feedback"><?= esc($errors['jabatan']) ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Tanggal Masuk', 'tgl_masuk',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                        <?= form_input($form['tgl_masuk']) ?>
                                        <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                    <div class="invalid-feedback"><?= esc($errors['tgl_masuk']) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="offset-sm-2 col-sm-10">
                                    <button type="submit" class="btn btn-danger">Submit</button>
                                </div>
                            </div>
                            <?= form_close() ?>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<?= $this->endSection() ?>