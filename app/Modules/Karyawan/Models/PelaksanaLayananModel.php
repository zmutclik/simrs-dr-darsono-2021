<?php

namespace Karyawan\Models;

use CodeIgniter\Model;

class PelaksanaLayananModel extends Model
{
    protected $DBGroup          = 'DBKaryawan';
    protected $table            = 'karyawan_pelaksana_layanan';
    protected $allowedFields    = [];
    protected $validationRules  = [];

    // protected $useSoftDeletes   = true;
    // protected $useTimestamps    = true;
    
    public function options()
    {
        $options = $this->findAll();
        $data = [];
        // $data[''] = '';
        foreach ($options as $value)
            $data[$value['id']] = $value['nama'];
        return $data;
    }
}
