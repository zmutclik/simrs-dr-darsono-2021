<?php

namespace Karyawan\Models;

use CodeIgniter\Model;
use Karyawan\Models\KaryawanModel;
use Karyawan\Models\PelaksanaLayananModel;

class KaryawanTagsModel extends Model
{
    protected $DBGroup          = 'DBKaryawan';
    protected $table            = 'karyawan_tags';
    protected $allowedFields    = [];
    protected $validationRules  = [];

    // protected $useSoftDeletes   = true;
    // protected $useTimestamps    = true;

    public function options()
    {
        $options = $this->findAll();
        $data = [];
        // $data[''] = '';
        foreach ($options as $value)
            $data[$value['id']] = $value['nama'];
        return $data;
    }

    public function KaryawanByTag($tag)
    {
        $karyawanModel = new KaryawanModel();
        $PelaksanaLayananModel = new PelaksanaLayananModel();

        $data['pelaksana_layanan'] = $PelaksanaLayananModel->select('id,nama')->find($tag);

        $tags = $this->select('id_karyawan')->where('tag', $tag)->findAll();
        $id_karyawans = [];
        foreach ($tags as $value)
            $id_karyawans[] = $value['id_karyawan'];

        $data['options'][] = ['id' => '', 'text' => ''];
        $karyawan = [];
        if (!empty($id_karyawans))
            $karyawan = $karyawanModel->select('id,nama as text')->wherein('id', $id_karyawans)->findall();

        $data['options'] = array_merge($data['options'], $karyawan);
        return $data;
    }
}
