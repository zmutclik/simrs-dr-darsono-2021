<?php

namespace Karyawan\Models;

use CodeIgniter\Model;
use Karyawan\Models\PelaksanaLayananModel;
use Karyawan\Models\KaryawanTagsModel;

class KaryawanModel extends Model
{
    protected $DBGroup = 'DBKaryawan';
    protected $table      = 'karyawan';

    protected $allowedFields = [
        'username', 'email', 'nip', 'nama', 'alamat', 'jkl', 'jabatan', 'tgl_masuk', 'img_profile',
        'is_pelaksana_individu', 'is_dokter_spesialis',
    ];

    protected $validationRules = [
        'nama'      => 'required|min_length[3]|max_length[50]',
        // 'email'     => 'required|valid_email|is_unique[karyawan.email]',
        'alamat'    => 'required',
        'jkl'       => 'required',
        'jabatan'   => 'required',
        'tgl_masuk' => 'required',
    ];

    protected $useSoftDeletes   = true;
    protected $useTimestamps    = true;


    public function dpjp_options()
    {
        $KaryawanTags = new KaryawanTagsModel();
        $listDokters = $KaryawanTags->where('tag', 13)->findall();
        $listDokter = [];
        foreach ($listDokters as $key => $value)
            $listDokter[] = $value['id_karyawan'];

        $options = $this->whereIn('id', $listDokter)->findAll();
        $data = [];
        $data[''] = '';
        foreach ($options as $value)
            $data[$value['id']] = $value['nama'];
        return $data;
    }
}
