<?php

namespace Karyawan\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\I18n\Time;
use Karyawan\Models\KaryawanModel;

class ProfileController extends BaseController
{

	protected $form  = [
		'nama' => [
			'name'  => 'nama',
			'id'    => 'nama',
			'value' => null,
			'class' => 'form-control',
			'placeholder' => 'Nama lengkap',
		],
		'alamat' => [
			'name'  => 'alamat',
			'id'    => 'alamat',
			'value' => null,
			'class' => 'form-control',
			'placeholder' => 'Jl. . .',
		],
		'jklL' => [
			'name'  => 'jkl',
			'id'    => 'jklL',
			'value' => 'L',
			'class' => 'form-check-input',
		],
		'jklP' => [
			'name'  => 'jkl',
			'id'    => 'jklP',
			'value' => 'P',
			'class' => 'form-check-input',
		],
		'jabatan' => [
			'name'  => 'jabatan',
			'id'    => 'jabatan',
			'value' => null,
			'class' => 'form-control',
			'placeholder' => 'Jabatan',
		],
		'tgl_masuk' => [
			'name'  => 'tgl_masuk',
			'id'    => 'tgl_masuk',
			'value' => null,
			'class' => 'form-control datetimepicker-input',
			'data-target' => '#reservationdate',
		],
	];

	protected $fields = ['nama', 'alamat', 'jkl', 'jabatan', 'tgl_masuk'];

	protected $pathTemplate = "Settings\Views\profile";
	
	protected $model;
	
	protected $permission;
	
    public function __construct()
    {
        $this->model = new KaryawanModel();
	}
	
	public function before_template()
	{
		// $this->data['link_tag'][] = '';
		// $this->data['script_tag'][] = '';

		$this->data['link_tag'][] 	= 'node_modules/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css';

		$this->data['script_tag'][] = 'node_modules/moment/min/moment-with-locales.min.js';
		$this->data['script_tag'][] = 'node_modules/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js';
		$this->data['script_tag'][] = 'js/pages/setting/profile.js';
	}

	public function index()
	{
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');

		foreach ($this->data['karyawan'] as $key => $value) {
			$this->form[$key]['value'] = $this->data['karyawan'][$key];
		}
		$this->form['tgl_masuk']['value'] = Time::createFromFormat('Y-m-d', $this->form['tgl_masuk']['value'])->toLocalizedString('d-MM-Y');
		$this->form['jklL']['checked'] = $this->data['karyawan']['jkl'] == 'L';
		$this->form['jklP']['checked'] = $this->data['karyawan']['jkl'] == 'P';

		// dd(set_radio($this->data['karyawan']['jkl'], 'L', TRUE));
        $this->template($this->pathTemplate . '\form');
	}

	//--------------------------------------------------------------------

	public function update()
	{
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
			
		$id = $this->request->getPost('id');

		$data = [];
		foreach ($this->fields as $value) {
			$data[$value] = $this->request->getPost($value);
		}

		$data['tgl_masuk'] = Time::createFromFormat('j-m-Y', $data['tgl_masuk'])->toDateString();
		// dd($data);

		if ($this->model->update($id, $data) === false) {
			$this->data['errors'] = $this->model->errors();

			foreach ($this->model->errors() as $key => $value) {
				foreach ($this->form as $key_ => $item) {
					if ($this->form[$key_]['name'] == $key)
						$this->form[$key_]['class'] .= ' is-invalid';
				}
			}
		}

		foreach ($data as $key => $value) {
			$this->form[$key]['value'] = $data[$key];
		}

		$this->form['tgl_masuk']['value'] = Time::createFromFormat('Y-m-d', $data['tgl_masuk'])->toLocalizedString('d-MM-Y');
		$this->form['jklL']['checked'] = $data['jkl'] == 'L';
		$this->form['jklP']['checked'] = $data['jkl'] == 'P';

		// dd($this->data);
        $this->template($this->pathTemplate . '\form');
	}
}
