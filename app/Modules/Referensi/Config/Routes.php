<?php

/**
 * BillingTrx routes file.
 */

$routes->group('', ['namespace' => 'Referensi\Controllers'], function ($routes) {

	$routes->get('ICD', 'ICDController::ICD');

});
