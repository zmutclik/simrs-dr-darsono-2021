<?php

namespace Referensi\Controllers;

use App\Controllers\BaseController;
use Referensi\Models\ICDModel;

class ICDController extends BaseController
{

    protected $model;

    public function __construct()
    {
        $this->model = new ICDModel();
    }

    public function ICD()
    {
        $q =  $this->request->getGet('q');
        $arr = [];

        $arr = $this->model
            ->like("CONCAT(CODE,' ',CODE2,' ',STR)", "%$q%")
            ->where('SAB', 'ICD10_1998')
            ->orderBy('CODE2', 'asc')
            ->findAll(50);

        $data['results'] = [];
        foreach ($arr as $value) {
            $data['results'][] = ['id' => $value['CODE'], 'text' => '[ ' . $value['CODE'] . ' ] ' . $value['STR']];
        }

        return $this->response->setJSON($data);
    }
}
