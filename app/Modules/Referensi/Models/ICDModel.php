<?php
namespace Referensi\Models;

use CodeIgniter\Model;

class ICDModel extends Model
{
protected $DBGroup          = 'DBReferensi';
protected $table            = 'inacbg5';
protected $allowedFields    = [];
protected $validationRules  = [];

protected $useSoftDeletes   = false;
protected $useTimestamps    = false;
public $data            = [];
protected $afterInsert  = ['_afterInsert'];
/*
protected $beforeInsert = ['CreateBy'];
protected $beforeDelete = ['DeleteBy'];
protected function CreateBy(array $data){$data['data']['created_by'] = username();return $data;}
protected function DeleteBy(array $data){$data['data']['deleted_by'] = username();return $data;}
*/
protected function _afterInsert(array $data) { $this->data = $data['data']; }
}