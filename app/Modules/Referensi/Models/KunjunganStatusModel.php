<?php

namespace Referensi\Models;

use CodeIgniter\Model;

class KunjunganStatusModel extends Model
{
    protected $DBGroup          = 'DBReferensi';
    protected $table            = 'kunjungan_status';
    protected $allowedFields    = [];
    protected $validationRules  = [];

    protected $useSoftDeletes   = false;
    protected $useTimestamps    = false;

    public function options()
    {
        $options = $this->findAll();
        $data = [];
        foreach ($options as $value)
            $data[$value['nama']] = $value['nama'];
        return $data;
    }

    protected function _afterInsert(array $data)
    {
        $this->data = $data['data'];
    }
}
