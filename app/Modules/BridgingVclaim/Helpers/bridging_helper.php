<?php

if (!function_exists('Bridginghash')) {
    function Bridginghash()
    {
        date_default_timezone_set('UTC');
        $config             = config('BridgingVclaim\\Config\\Bridging');
        $time               = strval(time() - strtotime('1970‐01‐01 00:00:00'));
        $var_data           = $config->consumerID . '&' . $time;
        $var_secret         = $config->consumerSecret;

        $data['id']         = $config->consumerID;
        $data['time']       = $time;
        $data['signature']  = base64_encode(hash_hmac('sha256', $var_data, $var_secret, TRUE));
        return $data;
    }
}

if (!function_exists('getHeader')) {
    function getHeader($method = 'get')
    {
        $headers = [];
        $signature = Bridginghash();

        if ($method == 'get' || $method == 'delete' || $method == 'post-json')
            $headers['headers']['Accept'] = 'application/json';

        if ($method == 'post' || $method == 'delete')
            $headers['headers']['Content-type'] = 'Application/x‐www‐form‐urlencoded';

        if ($method == 'post-json')
            $headers['headers']['Content-type'] = 'application/json';


        $headers['headers']['X-Cons-ID']    =  $signature['id'];
        $headers['headers']['X-Timestamp']  =  $signature['time'];
        $headers['headers']['X-Signature']  =  $signature['signature'];

        return $headers;
    }
}
