<?php
namespace BridgingVclaim\Models\Referensi;

use CodeIgniter\Model;

class WilayahModel extends Model
{
protected $DBGroup          = 'DBBridgingVclaim';
protected $table            = 'referensi_wilayah';
protected $primaryKey       = 'kode';
protected $allowedFields    = ['kode','parent','nama'];
protected $validationRules  = [];

protected $useSoftDeletes   = false;
protected $useTimestamps    = false;
public $data            = [];
protected $afterInsert  = ['_afterInsert'];
/*
protected $beforeInsert = ['CreateBy'];
protected $beforeDelete = ['DeleteBy'];
protected function CreateBy(array $data){$data['data']['created_by'] = username();return $data;}
protected function DeleteBy(array $data){$data['data']['deleted_by'] = username();return $data;}
*/
protected function _afterInsert(array $data) { $this->data = $data['data']; }


}