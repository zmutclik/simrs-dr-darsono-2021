<?php

namespace BridgingVclaim\Models;

use CodeIgniter\Model;
use BridgingVclaim\Models\ReferensiDataModel;

class PesertaModel extends Model
{
    protected $DBGroup          = 'DBBridgingVclaim';
    protected $table            = 'peserta';
    protected $primaryKey       = 'noKartu';
    protected $allowedFields    = ['noKartu', 'nik', 'mr', 'noTelepon', 'nama', 'pisa', 'sex', 'tglLahir', 'hakKelas', 'jenisPeserta', 'statusPeserta', 'provUmum', 'cob'];
    protected $validationRules  = [];

    protected $useSoftDeletes   = false;
    protected $useTimestamps    = false;

    protected $afterFind        = ['_afterFind'];

    public function set_cob($data)
    {
        if (empty($data['noAsuransi']))
            return null;
        $db      = \Config\Database::connect($this->DBGroup);
        $builder = $db->table('peserta_cob');

        $builder->replace($data);
        return $data['noAsuransi'];
    }

    public function _afterFind(array $data)
    {
        $referensi =  new ReferensiDataModel();

        $data['data']['hakKelas']       = $referensi->where(['type' => 'hakKelas'       , 'kode' => $data['data']['hakKelas']])->first();
        $data['data']['jenisPeserta']   = $referensi->where(['type' => 'jenisPeserta'   , 'kode' => $data['data']['jenisPeserta']])->first();
        $data['data']['statusPeserta']  = $referensi->where(['type' => 'statusPeserta'  , 'kode' => $data['data']['statusPeserta']])->first();
        $data['data']['provUmum']       = $referensi->where(['type' => 'faskes'         , 'kode' => $data['data']['provUmum']])->first();

        return $data;
    }
}
