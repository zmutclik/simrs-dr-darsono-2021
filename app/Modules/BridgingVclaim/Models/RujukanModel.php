<?php

namespace BridgingVclaim\Models;

use CodeIgniter\Model;
use BridgingVclaim\Models\ReferensiDataModel;

class RujukanModel extends Model
{
    protected $DBGroup          = 'DBBridgingVclaim';
    protected $table            = 'rujukan';
    protected $primaryKey       = 'noKunjungan';
    protected $allowedFields    = ['noKunjungan', 'peserta', 'keluhan', 'tglKunjungan', 'pelayanan', 'poliRujukan', 'asalFaskes', 'provPerujuk', 'diagnosa'];
    protected $validationRules  = [];

    protected $useSoftDeletes   = false;
    protected $useTimestamps    = false;
    protected $afterFind        = ['_afterFind'];

    public function _afterFind(array $data)
    {
        
        $referensi =  new ReferensiDataModel();
        if (isset($data['id'])) {
            $data['data']['diagnosa']       = $referensi->where(['type' => 'diagnosa', 'kode' => $data['data']['diagnosa']])->first();
            $data['data']['pelayanan']      = $referensi->where(['type' => 'pelayanan', 'kode' => $data['data']['pelayanan']])->first();
            $data['data']['poliRujukan']    = $referensi->where(['type' => 'poliRujukan', 'kode' => $data['data']['poliRujukan']])->first();
            $data['data']['provPerujuk']    = $referensi->where(['type' => 'provPerujuk', 'kode' => $data['data']['provPerujuk']])->first();
        } else {
            foreach ($data['data'] as $key => $value) {
                $data['data'][$key]['diagnosa']       = $referensi->where(['type' => 'diagnosa', 'kode' => $value['diagnosa']])->first();
                $data['data'][$key]['pelayanan']      = $referensi->where(['type' => 'pelayanan', 'kode' => $value['pelayanan']])->first();
                $data['data'][$key]['poliRujukan']    = $referensi->where(['type' => 'poliRujukan', 'kode' => $value['poliRujukan']])->first();
                $data['data'][$key]['provPerujuk']    = $referensi->where(['type' => 'provPerujuk', 'kode' => $value['provPerujuk']])->first();
            }
        }
        return $data;
    }
}
