<?php

namespace BridgingVclaim\Models;

use CodeIgniter\Model;

class ReferensiDataModel extends Model
{
    protected $DBGroup          = 'DBBridgingVclaim';
    protected $table            = 'referensi_data';
    protected $allowedFields    = [];
    protected $validationRules  = [];

    protected $useSoftDeletes   = false;
    protected $useTimestamps    = false;

    protected $afterFind        = ['_afterFind'];

    public function crosscheck($type, $data)
    {
        $data['kode'] = empty($data['kode']) ? 0 : $data['kode'];

        if (isset($data['keterangan'])) {
            $data['nama'] = $data['keterangan'];
            unset($data['keterangan']);
        }
        if (isset($data['kdProvider'])) {
            $data['kode'] = $data['kdProvider'];
            $data['nama'] = $data['nmProvider'];
            unset($data['kdProvider']);
            unset($data['nmProvider']);
        }

        $this->replace(array_merge($data, array('type' => $type)));
        return $data['kode'];
    }

    public function _afterFind(array $data)
    {
        unset($data['data']['type']);
        return $data;
    }

}
