<?php

namespace BridgingVclaim\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use BridgingVclaim\Controllers\PesertaController;
use BridgingVclaim\Models\PesertaModel;
use BridgingVclaim\Models\RujukanModel;
use BridgingVclaim\Models\ReferensiDataModel;

class RujukanController extends BaseController
{
    use ResponseTrait;
    protected $model;
    protected $model_peserta;
    protected $referensi;

    public function __construct()
    {
        helper('BridgingVclaim\\Helpers\\bridging');
        $config = config('BridgingVclaim\\Config\\Bridging');

        $this->model            = new RujukanModel();
        $this->model_peserta    = new PesertaModel();
        $this->referensi        = new ReferensiDataModel();
        $this->client = \Config\Services::curlrequest($config->options);
    }

    public function rujukan($no) // Peserta & Rujukan
    {
        try {
            $header     = getHeader();
            $response   = $this->client->request('get', "Rujukan/{$no}", $header);
            $response   = $response->getBody();
            $response   = json_decode($response, true);

            $dataSave   = $this->save($response['response']);
            return $this->myRespond($dataSave);
        } catch (\Throwable $e) {
            try {
                return $this->myRespond($this->model->find($no));
            } catch (\Throwable $e) {
                // dd($e);
                $this->myRespond('', 404, 'Server BPJS Trowbel dan Belum ada Data.');
            }
        }
    }

    public function peserta($no) // Peserta & Rujukan
    {
        try {
            $header     = getHeader();
            $response   = $this->client->request('get', "Rujukan/Peserta/{$no}", $header);
            $response   = $response->getBody();
            $response   = json_decode($response, true);

            $dataSave   = $this->save($response['response']);
            return $this->myRespond($dataSave);
        } catch (\Throwable $e) {
            try {
                $rujukan = $this->model->where(['peserta' => $no, 'tglKunjungan>' => date('Y-m-d 01:01:01', strtotime('-89 days', strtotime(date('Y-m-d H:i:s'))))])->first();
                return $this->myRespond($rujukan);
            } catch (\Throwable $e) {
                // dd($e);
                $this->myRespond('', 404, 'Server BPJS Trowbel dan Belum ada Data.');
            }
        }
    }

    private function save(array $data)
    {
        $peserta        = new PesertaController();
        $peserta_data   = $peserta->save($data['rujukan']['peserta']);

        $save['asalFaskes']     = $data['asalFaskes'];
        $save['diagnosa']       = $this->referensi->crosscheck('diagnosa', $data['rujukan']['diagnosa']);
        $save['keluhan']        = $data['rujukan']['keluhan'];
        $save['noKunjungan']    = $data['rujukan']['noKunjungan'];
        $save['pelayanan']      = $this->referensi->crosscheck('pelayanan', $data['rujukan']['pelayanan']);
        $save['peserta']        = $peserta_data['noKartu'];
        $save['poliRujukan']    = $this->referensi->crosscheck('poliRujukan', $data['rujukan']['poliRujukan']);
        $save['provPerujuk']    = $this->referensi->crosscheck('provPerujuk', $data['rujukan']['provPerujuk']);
        $save['tglKunjungan']   = $data['rujukan']['tglKunjungan'];

        $this->model->replace($save);

        $respond   = $this->model->find($data['rujukan']['noKunjungan']);
        return $respond;
    }

    private function myRespond($data = [], $code = 200, $message = 'Sukses')
    {
        $respond['metaData']['code'] = $code;
        $respond['metaData']['message'] = $message;
        $respond['response'] = $data;

        return $this->respond($respond, $code, $message);
    }


    public function search($peserta)
    {
        $arr = [];
        $arr = $this->model
            ->where("peserta", $peserta)
            ->where('tglKunjungan>', date('Y-m-d 01:01:01', strtotime('-89 days', strtotime(date('Y-m-d H:i:s')))))
            ->orderBy('tglKunjungan', 'asc')
            ->findAll(50);

        $data['results'] = [];
        foreach ($arr as $value) {
            $data['results'][] = [
                'id' => $value['noKunjungan'],
                'text' => $value['noKunjungan'] . ' - ' . $value['poliRujukan']['nama'] . ' = ' . $value['keluhan'],
                'tglKunjungan' => $value['tglKunjungan'],
            ];
        }

        return $this->response->setJSON($data);
    }
}
