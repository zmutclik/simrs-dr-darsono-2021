<?php

namespace BridgingVclaim\Controllers;

use App\Controllers\BaseController;

class VclaimController extends BaseController
{
    public function __construct()
    {
        // $this->model = new ();
        helper('BridgingVclaim\\Helpers\\bridging');
        $config = config('BridgingVclaim\\Config\\Bridging');
        
        $this->client = \Config\Services::curlrequest($config->options);
    }

    public function hash_test()
    {
        $config = config('BridgingVclaim\\Config\\Bridging');
        $data = $config->consumerID;
        $secretKey = $config->consumerSecret;
        date_default_timezone_set('UTC');
        $tStamp = strval(time() - strtotime('1970‐01‐01 00:00:00'));
        $signature = hash_hmac('sha256', $data . "&" . $tStamp, $secretKey, TRUE);
        $encodedSignature = base64_encode($signature);
        echo "X-cons-id: " . $data . "<br>";
        echo "X-timestamp: " . $tStamp . "<br>";
        echo "X-signature: " . $encodedSignature . "<br>";
    }



    public function dpjp($jenis_pelayanan, $tgl_dpjp, $kode_spesialis)
    {
        $this->hash_header();
        $url = $this->get_url();

        $data = $this->ci->curl->simple_get($url . "/referensi/dokter/pelayanan/{$jenis_pelayanan}/tglPelayanan/{$tgl_dpjp}/Spesialis/{$kode_spesialis}");
        return json_decode($data, true);
    }

    public function poli($search)
    {
        $this->hash_header();
        $url = $this->get_url();

        $data = $this->ci->curl->simple_get($url . "/referensi/poli/{$search}");
        return json_decode($data, true);
    }


    public function diag_search($q)
    {
        $this->hash_header();
        $url = $this->get_url();
        $data = $this->ci->curl->simple_get($url . "/referensi/diagnosa/$q");
        return json_decode($data, true);
    }

    public function faskes($jenis_faskes, $q)
    { // $jenis_faskes = 1 = faskes 1 / 2 = rs
        $this->hash_header();
        $url = $this->get_url();
        $data = $this->ci->curl->simple_get($url . "/referensi/faskes/$q/$jenis_faskes");
        return json_decode($data, true);
    }

    public function monitoring_kunjungan($date, $jenis_pelayanan)
    {
        $this->hash_header();
        $url = $this->get_url();
        $data = $this->ci->curl->simple_get($url . "/Monitoring/Kunjungan/Tanggal/$date/JnsPelayanan/$jenis_pelayanan");
        return json_decode($data, true);
    }

    public function monitoring_klaim($date, $jenis_pelayanan, $status)
    {
        $this->hash_header();
        $url = $this->get_url();
        $data = $this->ci->curl->simple_get($url . "/Monitoring/Klaim/Tanggal/$date/JnsPelayanan/$jenis_pelayanan/Status/$status");
        return json_decode($data, true);
    }

    public function propinsi()
    {
        $this->hash_header();
        $url = $this->get_url();
        $data = $this->ci->curl->simple_get($url . "/referensi/propinsi");
        return json_decode($data, true);
    }

    public function kabupaten($id)
    {
        $this->hash_header();
        $url = $this->get_url();
        $data = $this->ci->curl->simple_get($url . "/referensi/kabupaten/propinsi/" . $id);
        return json_decode($data, true);
    }

    public function kecamatan($id)
    {
        $this->hash_header();
        $url = $this->get_url();
        $data = $this->ci->curl->simple_get($url . "/referensi/kecamatan/kabupaten/" . $id);
        return json_decode($data, true);
    }

    public function aplicare_kelas()
    {
        $this->hash_header();
        $url = $this->url_lama;

        $data = $this->ci->curl->simple_get($url . "/aplicaresws/rest/ref/kelas");

        //        $this->ci->curl->debug();

        return json_decode($data, true);
    }

    public function aplicare_bed_get()
    {
        $this->hash_header();
        $url = $this->url_lama;
        $ppk = $this->kode_ppk;

        $data = $this->ci->curl->simple_get($url . "/aplicaresws/rest/bed/read/{$ppk}/1/30");

        //        $this->ci->curl->debug();

        return json_decode($data, true);
    }

    public function aplicare_bed_delete($data)
    {
        $this->hash_header('post-json');
        $url = $this->url_lama;
        $ppk = $this->kode_ppk;

        $req_json = json_encode($data, JSON_PRETTY_PRINT);
        $data = $this->ci->curl->simple_post($url . "/aplicaresws/rest/bed/delete/{$ppk}", $req_json);
        //        $this->ci->curl->debug();
        return json_decode($data, true);
    }

    public function aplicare_bed_add($data)
    {
        $this->hash_header('post-json');
        $url = $this->url_lama;
        $ppk = $this->kode_ppk;

        $req_json = json_encode($data, JSON_PRETTY_PRINT);
        //        echo $req_json;
        $data = $this->ci->curl->simple_post($url . "/aplicaresws/rest/bed/create/{$ppk}", $req_json);
        //        $this->ci->curl->debug();
        return json_decode($data, true);
    }
}
