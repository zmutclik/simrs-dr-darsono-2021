<?php

namespace BridgingVclaim\Controllers\Referensi;

use App\Controllers\BaseController;

class PoliController extends BaseController
{
    public function __construct()
    {
        helper('BridgingVclaim\\Helpers\\bridging');
        $config = config('BridgingVclaim\\Config\\Bridging');

        $this->client = \Config\Services::curlrequest($config->options);
    }

    public function index($Parameter)
    {
        $response   = $this->client->request('get', "referensi/poli/{$Parameter}" , getHeader());
        return $response;
    }
}
