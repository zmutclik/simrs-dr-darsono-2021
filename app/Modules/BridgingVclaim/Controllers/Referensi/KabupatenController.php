<?php

namespace BridgingVclaim\Controllers\Referensi;

use App\Controllers\BaseController;
use BridgingVclaim\Models\Referensi\WilayahModel;

class KabupatenController extends BaseController
{
    protected $model;
    public function __construct()
    {
        $this->model = new WilayahModel();

        helper('BridgingVclaim\\Helpers\\bridging');
        $config = config('BridgingVclaim\\Config\\Bridging');

        $this->client = \Config\Services::curlrequest($config->options);
    }

    public function index($paramater1)
    {
        try {
            $link       = "referensi/kabupaten/propinsi/{$paramater1}";
            $response   = $this->client->request('get', $link, getHeader());
            $response   = $response->getBody();
            $response   = json_decode($response, true);

            $wilayah_   = $this->model->where('parent', $paramater1)->findAll();
            $wilayah    = [];

            foreach ($wilayah_ as $key => $value)
                $wilayah[$value['kode']] = $value;

            foreach ($response['response']['list'] as $key => $value) {
                $val =  ['kode' => $value['kode'], 'parent' => $paramater1, 'nama' => $value['nama']];
                if (!array_key_exists($value['kode'], $wilayah))
                    $this->model->insert($val);
            }
        } catch (\Throwable $th) {
            // throw $th;
        }

        //////////////////////////////////////////////////////////////////////////////////////////

        $q =  $this->request->getGet('q');
        $arr = [];

        $arr = $this->model
            ->like("nama", "%$q%")
            ->where("parent", $paramater1)
            ->orderBy('nama', 'asc')
            ->findAll(50);

        $data['results'] = [];
        foreach ($arr as $value) {
            $data['results'][] = ['id' => $value['kode'], 'text' => $value['nama']];
        }

        return $this->response->setJSON($data);
    }
}
