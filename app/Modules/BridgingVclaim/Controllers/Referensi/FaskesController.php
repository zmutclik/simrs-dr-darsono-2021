<?php

namespace BridgingVclaim\Controllers\Referensi;

use App\Controllers\BaseController;

class FaskesController extends BaseController
{
    public function __construct()
    {
        helper('BridgingVclaim\\Helpers\\bridging');
        $config = config('BridgingVclaim\\Config\\Bridging');

        $this->client = \Config\Services::curlrequest($config->options);
    }

    public function index($Parameter1, $Parameter2)
    {
        $link = "referensi/faskes/{$Parameter1}/{$Parameter2}";
        $response   = $this->client->request('get', $link, getHeader());
        return $response;
    }
}
