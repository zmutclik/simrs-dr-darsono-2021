<?php

namespace BridgingVclaim\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use BridgingVclaim\Models\PesertaModel;
use BridgingVclaim\Models\ReferensiDataModel;

class PesertaController extends BaseController
{
    use ResponseTrait;
    protected $model;
    protected $referensi;
    public function __construct()
    {
        helper('BridgingVclaim\\Helpers\\bridging');
        $config = config('BridgingVclaim\\Config\\Bridging');

        $this->model = new PesertaModel();
        $this->referensi = new ReferensiDataModel();
        $this->client = \Config\Services::curlrequest($config->options);
    }

    public function peserta($no, $nik = 'bpjs')
    {
        try {
            $nik_       = ($nik == 'nik') ? 'nik' : 'nokartu';
            $header     = getHeader();
            $response   = $this->client->request('get', "Peserta/$nik_/$no/tglSEP/" . date('Y-m-d'), $header);
            $response   = $response->getBody();
            $response   = json_decode($response, true);

            $dataSave   = $this->save($response['response']['peserta']);
            return $this->myRespond($dataSave);
        } catch (\Throwable $e) {
            try {
                if ($nik == 'nik')
                    return $this->myRespond($this->model->where('nik', $no)->first());
                else
                    return $this->myRespond($this->model->find($no));
            } catch (\Throwable $e) {
                // dd($e);
                $this->myRespond('', 404, 'Server BPJS Trowbel dan Belum ada Data.');
            }
        }
    }

    public function save($data)
    {
        $save['noKartu'] = $data['noKartu'];
        $save['nik'] = $data['nik'];
        $save['mr'] = $data['mr']['noMR'];
        $save['noTelepon'] = $data['mr']['noTelepon'];
        $save['nama'] = $data['nama'];
        $save['pisa'] = $data['pisa'];
        $save['sex'] = $data['sex'];
        $save['tglLahir'] = $data['tglLahir'];
        $save['hakKelas'] = $this->referensi->crosscheck('hakKelas', $data['hakKelas']);
        $save['jenisPeserta'] = $this->referensi->crosscheck('jenisPeserta', $data['jenisPeserta']);
        $save['statusPeserta'] = $this->referensi->crosscheck('statusPeserta', $data['statusPeserta']);
        $save['provUmum'] = $this->referensi->crosscheck('faskes', $data['provUmum']);
        $save['cob'] = $this->model->set_cob($data['cob']);

        $this->model->replace($save);

        $respond   = $this->model->find($data['noKartu']);
        return $respond;
    }

    private function myRespond($data = [], $code = 200, $message = 'Sukses')
    {
        $respond['metaData']['code'] = $code;
        $respond['metaData']['message'] = $message;
        $respond['response'] = $data;

        return $this->respond($respond, $code, $message);
    }
}
