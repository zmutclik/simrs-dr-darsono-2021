<?php

namespace BridgingVclaim\Controllers;

use App\Controllers\BaseController;

class SEPController extends BaseController
{
    public function __construct()
    {
        helper('BridgingVclaim\\Helpers\\bridging');
        $config = config('BridgingVclaim\\Config\\Bridging');

        $this->client = \Config\Services::curlrequest($config->options);
    }

    public function get($no_sep)
    {
        $response   = $this->client->request('get', "SEP/{$no_sep}", getHeader());
        return $response;
    }

    public function save()
    {
        // $data = $this->request->getPost('data');
        // $req_json = json_encode($data, JSON_PRETTY_PRINT);

        $parser = \Config\Services::parser();
        $data     = $this->request->getPost();
        $template = '
        {
            "request": {
               "t_sep": {
                  "noKartu": "{noKartu}",
                  "tglSep": "{tglSep}",
                  "ppkPelayanan": "{ppkPelayanan}",
                  "jnsPelayanan": "{jnsPelayanan}",
                  "klsRawat": "{klsRawat}",
                  "noMR": "{no_rm}",
                  "rujukan": {
                     "asalRujukan": "{asalRujukan}",
                     "tglRujukan": "{tglRujukan}",
                     "noRujukan": "{noRujukan}",
                     "ppkRujukan": "{ppkRujukan}"
                  },
                  "catatan": "{catatan}",
                  "diagAwal": "{diagAwal}",
                  "poli": {
                     "tujuan": "{poli}",
                     "eksekutif": "{eksekutif|default(0)}"
                  },
                  "cob": {
                     "cob": "{cob|default(0)}"
                  },
                  "katarak": {
                     "katarak": "{katarak|default(0)}"
                  },
                  "jaminan": {
                     "lakaLantas": "{lakaLantas|default(0)}",
                     "penjamin": {
                         "penjamin": "{penjaminlakalantas}",
                         "tglKejadian": "{tanggallakalantas}",
                         "keterangan": "{Keteranganlakalantas}",
                         "suplesi": {
                             "suplesi": "{suplesi|default(0)}",
                             "noSepSuplesi": "noSepSuplesi",
                             "lokasiLaka": {
                                 "kdPropinsi": "{kdPropinsi}",
                                 "kdKabupaten": "{kdKabupaten}",
                                 "kdKecamatan": "{kdKecamatan}"
                                 }
                         }
                     }
                  },
                  "skdp": {
                     "noSurat": "{skdp}",
                     "kodeDPJP": "{skdpDPJP}"
                  },
                  "noTelp": "{noTelp}",
                  "user": "{user}"
               }
            }
         }';

        echo $parser->setData($data)->renderString($template);

        // $response   = $this->client->setBody($request)->request('post', "SEP/1.1/insert", getHeader('post'));

        // return $request;
    }

    public function delete($data)
    {
        // $req_json = json_encode($data, JSON_PRETTY_PRINT);

        $response   = $this->client->setBody(json_encode($data, JSON_PRETTY_PRINT))->request('post', "SEP/Delete", getHeader('delete'));

        return $response;
    }

    public function pengajuan($data)
    {
        $response   = $this->client->setBody(json_encode($data, JSON_PRETTY_PRINT))->request('post', "SEP/pengajuanSEP", getHeader('post'));

        return $response;
    }

    public function sep_aprovel($data)
    {
        $response   = $this->client->setBody(json_encode($data, JSON_PRETTY_PRINT))->request('post', "SEP/aprovalSEP", getHeader('post'));

        return $response;
    }
}
