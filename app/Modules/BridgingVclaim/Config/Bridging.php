<?php

namespace BridgingVclaim\Config;

use CodeIgniter\Config\BaseConfig;

class Bridging extends BaseConfig
{
    public $consumerID     = '10961';
    public $consumerSecret = '2gG85C155D';
    public $kode_ppk       = "1312r001";

    // private $url           = "https://dvlp.bpjs-kesehatan.go.id/VClaim-rest/";
    // private $url           = "https://new-api.bpjs-kesehatan.go.id:8080/new-vclaim-rest/";
    // public $url            = "https://new-api.bpjs-kesehatan.go.id:8080/new-vclaim-rest/";
    // public $url_lama       = "http://api.bpjs-kesehatan.go.id";
    public $options = [
        'baseURI' => "https://new-api.bpjs-kesehatan.go.id:8080/new-vclaim-rest/",
        'timeout'  => 3,
        'debug' => true
    ];
}
