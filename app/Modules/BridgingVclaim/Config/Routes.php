<?php

/**
 * BridgingVclaim routes file.
 */

$routes->group('', ['namespace' => 'BridgingVclaim\Controllers'], function ($routes) {
	// VCLAIM/
	$routes->get('vclaim', 'VclaimController::index');
	$routes->get('vclaim/hash_test', 'VclaimController::hash_test');

	$routes->get('vclaim/peserta/(:num)', 'PesertaController::peserta/$1');
	$routes->get('vclaim/peserta/nik/(:num)', 'PesertaController::peserta/$1/nik');

	$routes->get('vclaim/rujukan/peserta/(:num)', 'RujukanController::peserta/$1');
	$routes->get('vclaim/rujukan/search/(:num)', 'RujukanController::search/$1');
	$routes->get('vclaim/rujukan/(:any)', 'RujukanController::rujukan/$1');

	$routes->get('vclaim/referensi/poli/(:any)', 'Referensi\PoliController::index/$1');
	$routes->get('vclaim/referensi/faskes/(:any)/(:num)', 'Referensi\FaskesController::index/$1/$2');

	$routes->get('vclaim/referensi/propinsi/', 'Referensi\PropinsiController::index');
	$routes->get('vclaim/referensi/kabupaten/(:any)', 'Referensi\KabupatenController::index/$1');
	$routes->get('vclaim/referensi/kecamatan/(:any)', 'Referensi\KecamatanController::index/$1');

	// $routes->get('vclaim/sep/(:any)', 'SEPController::get/$1');
	$routes->get('vclaim/sep/', 'SEPController::save');
});
