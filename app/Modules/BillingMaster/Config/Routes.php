<?php

/**
 * BillingTrx routes file.
 */

$routes->group('', ['namespace' => 'BillingMaster\Controllers'], function ($routes) {
	// PASIEN/
	$routes->get('pasien', 'PasienController::index');
	$routes->get('pasien/new', 'PasienController::Form/save');
	$routes->get('pasien/(:num)/edit', 'PasienController::Form/edit/$1');
	$routes->get('pasien/(:num)/show', 'PasienController::Show/$1');
	$routes->post('pasien/save', 'PasienController::Save');
	$routes->post('pasien/(:num)/edit', 'PasienController::Save/edit/$1');
	$routes->post('pasien/(:num)/delete', 'PasienController::Delete/$1');
	$routes->post('pasien/Table', 'PasienController::Table');
	$routes->get('pasien/uuid/(:num)', 'PasienController::getuuid/$1');

	$routes->get('pasien/penjamin/(:num)/(:num)', 'PasienPenjaminController::index/$1/$2');
});
