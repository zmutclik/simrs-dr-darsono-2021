<?= $this->extend("template/index") ?>

<?= $this->section('content_header') ?>
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h3">PASIEN</h1>
    <div class="btn-toolbar mb-2 mb-md-0"></div>
</div>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Form PASIEN</h3>
                        <div class="card-tools"><button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fas fa-minus"></i></button><button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove"><i class="fas fa-times"></i></button></div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <?= form_open($urlForm, $form['form']) ?>
                            <?= form_input($form['kode_wilayah']) ?>
                            <div class="form-group row">
                                <?= form_label('NO_RM', 'no_rm',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-2">
                                    <?= form_input($form['no_rm']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["no_rm"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('NIK', 'nik',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['nik']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["nik"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Nama', 'nama',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['nama']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["nama"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Jenis Kelamin</label>
                                <div class="custom-control custom-radio">
                                    <?= form_radio($form['jklL']) ?>
                                    <?= form_label('Laki-laki', 'jklL',  ['class' => 'form-check-label']); ?>
                                </div>
                                <div class="custom-control custom-radio">
                                    <?= form_radio($form['jklP']) ?>
                                    <?= form_label('Perempuan', 'jklP',  ['class' => 'form-check-label']); ?>
                                </div>
                                <div class="invalid-feedback"><?= esc($errors['jkl']) ?></div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Tempat, Tanggal Lahir', 'lahir_tempat',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-5">
                                    <?= form_input($form['lahir_tempat']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["lahir_tempat"]) ?></div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                        <?= form_input($form['lahir_tgl']) ?>
                                        <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                    <div class="invalid-feedback"><?= esc($errors["lahir_tgl"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Alamat, Rt, Rw', 'alamat',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-8">
                                    <?= form_input($form['alamat']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["alamat"]) ?></div>
                                </div>
                                <div class="col-sm-1">
                                    <?= form_input($form['rt']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["rt"]) ?></div>
                                </div>
                                <div class="col-sm-1">
                                    <?= form_input($form['rw']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["rw"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Provinsi, Kabupaten', 'provinsi',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-5">
                                    <?= form_dropdown($form['provinsi'], $provinsi_options, $form['provinsi']['value']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["provinsi"]) ?></div>
                                </div>
                                <div class="col-sm-5">
                                    <?= form_dropdown($form['kabupaten'], $kabupaten_options, $form['kabupaten']['value']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["kabupaten"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Kecamatan, Kelurahan', 'kecamatan',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-5">
                                    <?= form_dropdown($form['kecamatan'], $kecamatan_options, $form['kecamatan']['value']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["kecamatan"]) ?></div>
                                </div>
                                <div class="col-sm-5">
                                    <?= form_dropdown($form['kelurahan'], $kelurahan_options, $form['kelurahan']['value']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["kelurahan"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Telepon', 'tlp',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['tlp']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["tlp"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Agama', 'agama',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_dropdown($form['agama'], $agama_options, $form['agama']['value']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["agama"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Status Marital', 'marital',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_dropdown($form['marital'], $marital_options, $form['marital']['value']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["marital"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Pekerjaan', 'pekerjaan',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_dropdown($form['pekerjaan'], $pekerjaan_options, $form['pekerjaan']['value']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["pekerjaan"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Pendidikan', 'pendidikan',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_dropdown($form['pendidikan'], $pendidikan_options, $form['pendidikan']['value']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["pendidikan"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Suku/Etnis', 'suku_etnis',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_dropdown($form['suku_etnis'], $suku_etnis_options, $form['suku_etnis']['value']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["suku_etnis"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Bahasa Utama', 'bahasa',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_dropdown($form['bahasa'], $bahasa_options, $form['bahasa']['value']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["bahasa"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Nama Ibu', 'nama_ibu',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['nama_ibu']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["nama_ibu"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Nama Bapak', 'nama_bapak',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['nama_bapak']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["nama_bapak"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Alamat OrTu', 'alamat_ortu',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['alamat_ortu']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["alamat_ortu"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?= form_label('Nama Suami/Istri', 'nama_sumistri',  ['class' => 'col-sm-2 col-form-label']); ?>
                                <div class="col-sm-10">
                                    <?= form_input($form['nama_sumistri']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["nama_sumistri"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="offset-sm-2 col-sm-10">
                                    <?= form_button($form['buttons']['back']) ?>
                                    <?= form_button($form['buttons']['submit']) ?>
                                </div>
                            </div>
                            <?= form_close() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>