<?php

namespace BillingMaster\Models\TempatLayanan;

use CodeIgniter\Model;

class TempatLayananTTModel extends Model
{
    protected $DBGroup          = 'DBBillingMaster';
    protected $table            = 'tempat_layanan_tt';
    protected $allowedFields    = ['id', 'id_tl', 'nama', 'kelas', 'is_tt_bayangan'];
    protected $validationRules  = [
        'id_tl'    => 'required',
        'nama'    => 'required',
        'kelas'    => 'required',
    ];

    protected $useSoftDeletes   = true;
    protected $useTimestamps    = true;
}
