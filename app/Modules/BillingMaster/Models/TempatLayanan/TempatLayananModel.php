<?php

namespace BillingMaster\Models\TempatLayanan;

use CodeIgniter\Model;

class TempatLayananModel extends Model
{
    protected $DBGroup          = 'DBBillingMaster';
    protected $table            = 'tempat_layanan';
    protected $allowedFields    = ['id', 'nama', 'kode', 'type', 'jumlat_tt', 'jumlah_tt_bayangan'];
    protected $validationRules  = [
        'id_tl'             => 'required',
        'nama'              => 'required',
        'kelas'             => 'required',
    ];

    protected $useSoftDeletes   = true;
    protected $useTimestamps    = true;

    public function options($type = '')
    {
        if (!empty($type))
            $options = $this
                ->where('type', $type)
                ->findAll();
        else
            $options = $this->findAll();
        $data = [];
        $data[''] = '';
        foreach ($options as $value)
            $data[$value['id']] = $value['nama'];
        return $data;
    }
}
