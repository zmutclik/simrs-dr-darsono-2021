<?php

namespace BillingMaster\Models\_Setting;

use CodeIgniter\Model;

class SettingTempatLayananTindakanModel extends Model
{
    protected $DBGroup          = 'DBBillingMaster';
    protected $table            = '_setting_tempat_layanan_tindakan';
    protected $allowedFields    = ['id_tempat_layanan', 'id_tindakan', 'tindakan', 'tindakan_kelompok'];
    protected $validationRules  = [];

    protected $useSoftDeletes   = true;
    protected $useTimestamps    = true;

    protected $beforeInsert = ['CreateBy'];
    protected $beforeDelete = ['DeleteBy'];

    protected function CreateBy(array $data)
    {
        $data['data']['created_by'] = username();
        return $data;
    }

    protected function DeleteBy(array $data)
    {
        $data['data']['deleted_by'] = username();
        return $data;
    }
}
