<?php
namespace BillingMaster\Models\_Setting;

use CodeIgniter\Model;

class PasienPenjaminModel extends Model
{
protected $DBGroup          = 'DBBillingMaster';
protected $table            = 'pasien_penjamin';
protected $allowedFields    = [];
protected $validationRules  = [];

protected $useSoftDeletes   = false;
protected $useTimestamps    = false;
}