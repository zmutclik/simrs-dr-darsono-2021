<?php

namespace BillingMaster\Models\Pasien;

use CodeIgniter\Model;
use CodeIgniter\I18n\Time;

class PasienModel extends Model
{
    protected $DBGroup          = 'DBBillingMaster';
    protected $table            = 'pasien';
    protected $primaryKey       = 'no_rm';
    protected $allowedFields    = [
        'no_rm',
        'nik',
        'nama',
        'jkl',
        'lahir_tempat',
        'lahir_tgl',
        'lahir_berat',
        'alamat',
        'rt',
        'rw',
        'provinsi',
        'kabupaten',
        'kecamatan',
        'kelurahan',
        'kode_wilayah',
        'tlp',
        'agama',
        'marital',
        'pekerjaan',
        'pendidikan',
        'suku_etnis',
        'bahasa',
        'nama_ibu',
        'nama_bapak',
        'alamat_ortu',
        'nama_sumistri',
        'is_meninggal',
    ];
    protected $validationRules  = [
        'nama'          => 'required',
        'jkl'           => 'required',
        'lahir_tempat'  => 'required',
        'lahir_tgl'     => 'required',
        'alamat'        => 'required',
        'rt'            => 'required',
        'rw'            => 'required',
        'provinsi'      => 'required',
        'kabupaten'     => 'required',
        'kecamatan'     => 'required',
        'kelurahan'     => 'required',
        'tlp'           => 'required',

    ];

    protected $useSoftDeletes   = true;
    protected $useTimestamps    = true;

    protected $beforeInsert = ['CreateBy'];
    protected $beforeDelete = ['DeleteBy'];
    protected $beforeUpdate = ['_beforeUpdate'];
    protected $afterFind    = ['_afterFind'];
    protected $afterInsert  = ['_afterInsert'];

    public $data            = [];

    protected function CreateBy(array $data)
    {
        $data['data']['created_by'] = username();

        $db = \Config\Database::connect('DBMaster');
        $query = $db->query('SELECT LPAD(MAX(no_rm*1)+1,6,0) AS no_rm FROM pasien limit 1');
        $row = $query->getRow();
        $data['data']['no_rm'] = $row->no_rm;

        $wilayahModel   = new \App\Models\Libs\WilayahModel();
        $datawil        = $wilayahModel->find($data['data']['provinsi']);
        $data['data']['provinsi']   = $datawil['nama'];
        $datawil        = $wilayahModel->find($data['data']['kabupaten']);
        $data['data']['kabupaten']   = $datawil['nama'];
        $datawil        = $wilayahModel->find($data['data']['kecamatan']);
        $data['data']['kecamatan']   = $datawil['nama'];
        $datawil        = $wilayahModel->find($data['data']['kelurahan']);
        $data['data']['kelurahan']   = $datawil['nama'];

        $data['data']['uuid'] = uuid();
        // dd($data);
        return $data;
    }

    protected function DeleteBy(array $data)
    {
        $data['data']['deleted_by'] = username();
        return $data;
    }

    protected function _afterFind(array $data)
    {
        $kode_wilayah =  explode(".", $data['data']['kode_wilayah']);
        // if (!empty($data['data']['kode_wilayah'])) {
        //     $data['data']['provinsi']   = $kode_wilayah[0];
        //     $data['data']['kabupaten']   = $kode_wilayah[0] . '.' . $kode_wilayah[1];
        //     $data['data']['kecamatan']   = $kode_wilayah[0] . '.' . $kode_wilayah[1] . '.' . $kode_wilayah[2];
        //     $data['data']['kelurahan']   = $kode_wilayah[0] . '.' . $kode_wilayah[1] . '.' . $kode_wilayah[2] . '.' . $kode_wilayah[3];
        // }

        //alamat,', RT.',rt,' RW.',rw,', ',kelurahan, ',Kec. ',kecamatan,',- ',kabupaten
        $data['data']['alamat_'] = $data['data']['alamat']
            . ', RT.' . $data['data']['rt']
            . ', RW.' . $data['data']['rw']
            . ' - ' . $data['data']['kelurahan']
            . ' Kec. ' . $data['data']['kecamatan']
            . ' - ' . $data['data']['kabupaten'];

        $time =  Time::parse($data['data']['lahir_tgl']);
        $data['data']['lahir'] = $data['data']['lahir_tempat'] . ', ' . $time->toLocalizedString('d MMM yyyy');
        $data['data']['umur']  = $time->humanize();
        $data['data']['lahir_umur']  = $data['data']['lahir'] . ' - ' . $time->humanize();

        $data['data']['kelamin'] = ($data['data']['jkl'] == 'L') ? 'Laki-laki' : 'Perempuan';
        return $data;
    }

    protected function _beforeUpdate(array $data)
    {
        $wilayahModel   = new \App\Models\Libs\WilayahModel();
        $datawil        = $wilayahModel->find($data['data']['provinsi']);
        $data['data']['provinsi']   = $datawil['nama'];
        $datawil        = $wilayahModel->find($data['data']['kabupaten']);
        $data['data']['kabupaten']   = $datawil['nama'];
        $datawil        = $wilayahModel->find($data['data']['kecamatan']);
        $data['data']['kecamatan']   = $datawil['nama'];
        $datawil        = $wilayahModel->find($data['data']['kelurahan']);
        $data['data']['kelurahan']   = $datawil['nama'];
        // dd($data);
        return $data;
    }


    protected function _afterInsert(array $data)
    {
        $this->data = $data['data'];
    }
}
