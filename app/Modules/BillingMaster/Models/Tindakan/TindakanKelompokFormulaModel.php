<?php

namespace BillingMaster\Models\Tindakan;

use CodeIgniter\Model;

class TindakanKelompokFormulaModel extends Model
{
    protected $DBGroup          = 'DBBillingMaster';
    protected $table            = 'tindakan_kelompok_formula';
    protected $allowedFields    = ['id_kelompok', 'nama', 'type', 'formula', 'formula_prosen', 'hidden'];
    protected $validationRules  = [
        'id_kelompok'    => 'required',
        'nama'    => 'required',
        'type'    => 'required',
        'formula'    => 'required',
    ];

    protected $useSoftDeletes   = true;
    protected $useTimestamps    = true;

    public $type_options = [
        '' => '',
        'Ruangan' => 'Ruangan',
        'Kelompok' => 'Kelompok/Tim',
        'Individu' => 'Individu',
        'Jasa Tidak Langsung' => 'Jasa Tidak Langsung',
        'Biaya Operasional' => 'Biaya Operasional',
    ];
}
