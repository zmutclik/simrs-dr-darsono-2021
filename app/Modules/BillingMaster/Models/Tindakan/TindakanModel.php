<?php

namespace BillingMaster\Models\Tindakan;

use CodeIgniter\Model;
use BillingMaster\Models\TindakanKelompokModel;

class TindakanModel extends Model
{
    protected $DBGroup          = 'DBBillingMaster';
    protected $table            = 'tindakan';
    protected $allowedFields    = [
        'id',
        'kode_panggil',
        'id_kelompok',
        'nama',
        'type_kelas',
        'tarif_3',
        'tarif_2',
        'tarif_1',
        'tarif_vip',
    ];
    protected $validationRules  = [
        'kode_panggil' => 'required',
        'id_kelompok'  => 'required',
        'nama'         => 'required',
        'type_kelas'   => 'required',

    ];

    protected $useSoftDeletes   = true;
    protected $useTimestamps    = true;

    public $type_kelas  = ['non' => 'Non Kelas', 'kelas' => 'Kelas'];

    public function id_kelompok()
    {
        $model = new TindakanKelompokModel();
        $options = $model->findAll();
        $data = [];
        $data[''] = '';
        foreach ($options as $value)
            $data[$value['id']] = $value['nama'];
        return $data;
    }
    
    public function options()
    {
        $options = $this->findAll();
        $data = [];
        $data[''] = '';
        foreach ($options as $value)
            $data[$value['id']] = $value['nama'];
        return $data;
    }
}
