<?php

namespace BillingMaster\Models\Tindakan;

use CodeIgniter\Model;

class TindakanKelompokModel extends Model
{
    protected $DBGroup          = 'DBBillingMaster';
    protected $table            = 'tindakan_kelompok';
    protected $allowedFields    = ['id', 'nama', 'jenis_tindakan','uuid'];
    protected $validationRules  = [
        'nama'             => 'required',
        'jenis_tindakan'   => 'required',

    ];

    protected $useSoftDeletes   = true;
    protected $useTimestamps    = true;

    public $jenis_tindakan = [
        'ADMINISTRASI'  => 'Administrasi',
        'TINDAKAN MEDIS'=> 'Tindakan Medis',
        'LABORATORIUM'  => 'Laboratorium',
        'RADIOLOGI'     => 'Radiologi',
        'TINDAKAN OK'   => 'Tindakan OK',
    ];
}
