<?php

namespace BillingMaster\Models;

use CodeIgniter\Model;

class PenjaminModel extends Model
{
    protected $DBGroup          = 'DBBillingMaster';
    protected $table            = 'penjamin';
    protected $allowedFields    = [];
    protected $validationRules  = [];

    protected $useSoftDeletes   = true;
    protected $useTimestamps    = true;
    
    public function options()
    {
        $options = $this->findAll();
        $data = [];
        // $data[''] = '';
        foreach ($options as $value)
            $data[$value['id']] = $value['nama'];
        return $data;
    }
}
