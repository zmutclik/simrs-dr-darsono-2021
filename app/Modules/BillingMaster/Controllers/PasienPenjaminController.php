<?php

namespace BillingMaster\Controllers;

use App\Controllers\BaseController;
use BillingMaster\Models\PasienPenjaminModel;

class PasienPenjaminController extends BaseController
{
    protected $model;

    protected $permission;

    public function __construct()
    {
        $this->model = new PasienPenjaminModel();
    }

    public function index($no_rm, $penjamin)
    {
        $arr = $this->model
            ->where('id_penjamin', $penjamin)
            ->where('no_rm', $no_rm)
            ->findAll();

        $data['metaData']['code'] = 200;
        $data['metaData']['message'] = 'Sukses';
        $data['metaData']['count'] = 0;
        $data['response'] = [];

        foreach ($arr as $item) {
            $data['metaData']['count']++;
            $data['response']['no_anggota'] = $item['no_anggota'];
            $data['response']['nama'] = $item['nama'];
        }

        return $this->response->setJSON($data);
    }
}
