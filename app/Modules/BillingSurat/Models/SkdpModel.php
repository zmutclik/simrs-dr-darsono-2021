<?php
namespace App\Models;

use CodeIgniter\Model;

class SkdpModel extends Model
{
protected $DBGroup          = 'DBBillingSurat';
protected $table            = 'keterangan_berobat';
protected $allowedFields    = [];
protected $validationRules  = [];

protected $useSoftDeletes   = false;
protected $useTimestamps    = false;
}