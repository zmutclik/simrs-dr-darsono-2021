<?php

namespace BillingSurat\Controllers;

use App\Controllers\BaseController;
use App\Libraries\Datatables;
// use App\Models\ 0000659930409;

class SkdpController extends BaseController
{
    protected $form = [
        'form' => ['class' => 'needs-validation',],
        'buttons' => ['back' => ['type' => 'button', 'class' => 'btn btn-danger hBack', 'content' => 'KEMBALI/BATAL',], 'submit' => ['type' => 'submit', 'class' => 'btn btn-danger', 'content' => 'SIMPAN',], 'delete' => ['type' => 'button', 'class' => 'btn btn-danger', 'content' => 'HAPUS',], 'edit' => ['type' => 'button', 'class' => 'btn btn-danger', 'content' => 'KOREKSI',],]
    ];

    protected $fields = [];

    protected $pathTemplate = "pages/skdpcontroller";

    protected $model;

    protected $permission;

    // public function __construct() { $this->model = new 0000659930409(); } 

    public function before_template()
    {
        $this->tag_datatables();
        $this->data['script_tag'][] = 'js/pages/SkdpController.js';
    }

    public function index()
    {
        // if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        // $this->template($this->pathTemplate . 'list');
    }

    public function Form($action = 'save', $id = null)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->data['urlForm'] = my_uri((isset($id)) ? $id . '/' . $action : $action);
        if (!empty($id)) {
            foreach ($this->model->find($id) as $key => $value) $this->form[$key]['value'] = $value;
        }
        $this->template($this->pathTemplate . 'form');
    }

    public function Save($action = 'save', $id = null)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->data['urlForm'] = my_uri((isset($id)) ? $id . '/' . $action : $action);
        $this->form['form']['class'] = 'was-validated';
        $data = [];
        foreach ($this->fields as $value) $data[$value] = $this->request->getPost($value);
        if ($action == 'save') if ($this->model->insert($data) === false) $this->data['errors'] = $this->model->errors();
        else {
            $this->form['buttons']['submit']['class'] = 'invisible';
            $this->form['buttons']['back']['content'] = 'KEMBALI';
        }
        if ($action == 'edit') if ($this->model->update($id, $data) === false) $this->data['errors'] = $this->model->errors();
        else $this->form['buttons']['back']['content'] = 'KEMBALI';
        foreach ($this->data['errors'] as $key => $value) foreach ($this->form as $key_ => $item) if (!empty($this->form[$key_]['name'])) if ($this->form[$key_]['name'] == $key) $this->form[$key_]['class'] .= ' is-invalid';
        foreach ($data as $key => $value) $this->form[$key]['value'] = $data[$key];
        $this->template($this->pathTemplate . 'form');
    }

    public function Delete($id)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        if ($this->request->isAJAX()) {
            $this->model->delete($id);
            $json['token'] = csrf_hash();
            return $this->response->setJSON($json);
        }
    }

    public function Show($id)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        foreach ($this->model->find($id) as $key => $value) $this->form[$key]['value'] = $value;
        $this->form['buttons']['back']['content'] = 'KEMBALI';
        $this->form['buttons']['edit']['onClick'] = "form_edit($id)";
        $this->form['buttons']['delete']['onClick'] = "form_delete($id,true)";
        $this->template($this->pathTemplate . 'show');
    }

    public function Table()
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        if ($this->request->isAJAX()) {
            $datatables = new Datatables();
            /* $datatables->SetDB('DBMaster'); */

            $datatables->from('auth_permissions')
                ->select("id,name,description")
                ->where('deleted_at', null)
                ->att_column('DT_RowId', '$1', 'id');

            $data['result'] = $datatables->generate();
            $json = json_decode($data['result'], true);
            $json['token'] = csrf_hash();
            return $this->response->setJSON($json);
        }
    }



    public function search($no_rm)
    {
        $db = \Config\Database::connect('DBBillingSurat');

        $query = $db->query("SELECT id,CONCAT(LPAD(id_tempat_layanan, 2, 0),LPAD(NO, 4, 0))nomer,no_rm,b.nama tempat_layanan,c.nama tindak_lanjut_tempat_layanan,a.tindak_lanjut_tanggal FROM keterangan_berobat a
        LEFT JOIN _referensi_data b ON b.kode=a.id_tempat_layanan AND b.`type`='tempat_layanan'
        LEFT JOIN _referensi_data c ON c.kode=a.tindak_lanjut_id_tempat_layanan AND b.`type`='tempat_layanan'
        WHERE no_rm='$no_rm' 
        AND a.tindak_lanjut_tanggal <= CURDATE() + INTERVAL 2 DAY 
        LIMIT 10");

        $data['results'] = [];
        foreach ($query->getResult('array') as $value) {
            $data['results'][] = [
                'id' => $value['id'],
                'text' => $value['nomer'] . ' - ' . $value['tempat_layanan'] . ' > ' . $value['tindak_lanjut_tempat_layanan'],
            ];
        }

        return $this->response->setJSON($data);
    }
}
