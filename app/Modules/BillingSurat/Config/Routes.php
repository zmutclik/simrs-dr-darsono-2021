<?php

/**
 * BillingTrx routes file.
 */

$routes->group('', ['namespace' => 'BillingSurat\Controllers'], function ($routes) {

	// SURAT SKDP/
	$routes->get('surat/skdp', 'SkdpController::index');
	$routes->get('surat/skdp/search/(:num)', 'SkdpController::search/$1');

});
