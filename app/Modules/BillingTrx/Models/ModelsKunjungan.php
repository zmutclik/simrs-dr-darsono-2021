<?php

namespace BillingTrx\Models;

use BillingTrx\Models\Kunjungan\KunjunganModel;
use BillingTrx\Models\Kunjungan\KunjunganPenjaminModel;
use BillingTrx\Models\Kunjungan\KunjunganKeuanganModel;
use BillingTrx\Models\Kunjungan\KunjunganRMModel;
use BillingTrx\Models\Kunjungan\KunjunganJKNModel;
use BillingTrx\Models\Kunjungan\KunjunganKecelakaanModel;
use BillingTrx\Models\Kunjungan\KunjunganDokterModel;

use BillingTrx\Models\KunjunganTL\KunjTLModel;

use BillingMaster\Models\Pasien\PasienModel;
use BillingMaster\Models\PenjaminModel;
use BillingMaster\Models\TempatLayanan\TempatLayananModel;
use App\Models\Libs\KasusModel;
use App\Models\Libs\AsalMasukModel;
use Referensi\Models\KunjunganStatusModel;

use BridgingVclaim\Models\Referensi\WilayahModel;

class ModelsKunjungan
{
    public $kunjungan;
    public $kunjungan_keuangan;
    public $kunjungan_penjamin;
    public $kunjungan_rm;
    public $kunjungan_jkn;
    public $kunjungan_kecelakaan;
    public $KunjunganDokter;

    public $pasien;
    public $tempat_layanan;
    public $penjamin;

    public $referensi_kunjunganStatus;
    public $referensi_asalMasuk;
    public $referensi_kasus;

    public $kunjungan_tl;

    public $vclaim_wilayah;

    public function __construct()
    {
        $this->kunjungan            = new KunjunganModel();
        $this->kunjungan_penjamin   = new KunjunganPenjaminModel();
        $this->kunjungan_keuangan   = new KunjunganKeuanganModel();
        $this->kunjungan_rm         = new KunjunganRMModel();
        $this->kunjungan_jkn        = new KunjunganJKNModel();
        $this->kunjungan_kecelakaan = new KunjunganKecelakaanModel();
        $this->KunjunganDokter      = new KunjunganDokterModel();

        $this->pasien           = new PasienModel();
        $this->tempat_layanan   = new TempatLayananModel();
        $this->penjamin         = new PenjaminModel();

        $this->referensi_kunjunganStatus= new KunjunganStatusModel();
        $this->referensi_asalMasuk      = new AsalMasukModel();
        $this->referensi_kasus          = new KasusModel();

        $this->kunjungan_tl     = new KunjTLModel();

        $this->vclaim_wilayah   = new WilayahModel();

    }
}
