<?php

namespace BillingTrx\Models;

use BillingTrx\Models\KunjunganTLTindakan\KunjunganTLTindakanModel;
use BillingTrx\Models\KunjunganTLTindakan\KunjunganTLTindakanKeuanganModel;
use BillingTrx\Models\KunjunganTLTindakan\KunjunganTLTindakanPelaksanaModel;
use BillingMaster\Models\_Setting\SettingTempatLayananTindakanModel;
use BillingMaster\Models\Tindakan\TindakanKelompokModel;
use BillingMaster\Models\Tindakan\TindakanKelompokFormulaModel;
use Karyawan\Models\KaryawanTagsModel;
class ModelsTindakan
{

    public $tindakan;
    public $tindakan_keuangan;
    public $tindakan_pelaksana;

    public $SettingTempatLayananTindakan;

    public $masterTindakanKelompok;
    public $masterTindakanKelompokFormula;

    public $KaryawanTags;

    public function __construct()
    {
        $this->tindakan = new KunjunganTLTindakanModel();
        $this->tindakan_keuangan = new KunjunganTLTindakanKeuanganModel();
        $this->tindakan_pelaksana = new KunjunganTLTindakanPelaksanaModel();

        $this->SettingTempatLayananTindakan = new SettingTempatLayananTindakanModel();

        $this->masterTindakanKelompok = new TindakanKelompokModel();
        $this->masterTindakanKelompokFormula = new TindakanKelompokFormulaModel();

        $this->KaryawanTags = new KaryawanTagsModel();
    }
}
