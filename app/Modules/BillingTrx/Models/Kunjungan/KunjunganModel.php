<?php

namespace BillingTrx\Models\Kunjungan;

use CodeIgniter\Model;
use BillingTrx\Models\Kunjungan\KunjunganKeuanganModel;

class KunjunganModel extends Model
{
    protected $DBGroup          = 'DBBillingTrx';
    protected $table            = 'kunjungan';
    protected $allowedFields    = ['no_rm','uuid', 'is_pulang', 'tgl_mulai', 'tgl_selesai', 'perawatan_kelas', 'is_inap', 'status', 'keterangan'];
    protected $validationRules  = [
        'no_rm'          => 'required|min_length[6]',
        'tgl_mulai'      => 'required',
        'perawatan_kelas' => 'required',
    ];

    protected $useSoftDeletes   = true;
    protected $useTimestamps    = true;
    public $data            = [];
    protected $afterInsert  = ['_afterInsert'];

    protected $beforeInsert = ['CreateBy'];
    protected $beforeDelete = ['DeleteBy'];

    protected function CreateBy(array $data)
    {
        $data['data']['created_by'] = username();
        $data['data']['uuid'] = uuid();
        return $data;
    }

    protected function DeleteBy(array $data)
    {
        $data['data']['deleted_by'] = username();
        return $data;
    }

    protected function _afterInsert(array $data)
    {
        $keuangan   = new KunjunganKeuanganModel();
        $keuangan->insert(array('id_kunjungan' => $data['id']));
        
        $this->data = $data['data'];
    }
}
