<?php

namespace BillingTrx\Models\Kunjungan;

use CodeIgniter\Model;

class KunjunganJKNModel extends Model
{
    protected $DBGroup          = 'DBBillingTrx';
    protected $table            = 'kunjungan_jkn';
    protected $allowedFields    = [
        'id_kunjungan',
        'no_anggota',
        'no_rujukan',
        'skdp',
        'sep_rj',
        'sep_ri'
    ];
    protected $validationRules  = [];

    protected $useSoftDeletes   = false;
    protected $useTimestamps    = false;
}
