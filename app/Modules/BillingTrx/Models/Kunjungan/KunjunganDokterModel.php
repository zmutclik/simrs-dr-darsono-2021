<?php

namespace BillingTrx\Models\Kunjungan;

use CodeIgniter\Model;

class KunjunganDokterModel extends Model
{
    protected $DBGroup          = 'DBBillingTrx';
    protected $table            = 'kunjungan_dokter';
    protected $allowedFields    = [
        'id_kunjungan',
        'id_karyawan',
        'is_dpjp',
        'nama',
    ];
    protected $validationRules  = [];

    protected $useSoftDeletes   = false;
    protected $useTimestamps    = false;

    public function DokterDPJP($id_kunjungan)
    {
        $data = $this->where(['id_kunjungan' => $id_kunjungan, 'is_dpjp' => 'Y'])->first();
        if (empty($data))
            return '';
        else
            return $data['id_karyawan'];
    }
}
