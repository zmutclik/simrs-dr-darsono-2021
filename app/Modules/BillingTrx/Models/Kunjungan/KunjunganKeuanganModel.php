<?php

namespace BillingTrx\Models\Kunjungan;

use CodeIgniter\Model;

class KunjunganKeuanganModel extends Model
{
    protected $DBGroup          = 'DBBillingTrx';
    protected $table            = 'kunjungan_keuangan';
    protected $allowedFields    = ['id_kunjungan', 'biaya_lain', 'perawatan', 'farmasi', 'farmasi_dpho', 'jaminan', 'potongan_tindakan', 'potongan_subsidi', 'total_biaya'];
    protected $validationRules  = [
        'id_kunjungan'      => 'required',
    ];

    protected $useSoftDeletes   = false;
    protected $useTimestamps    = false;
}
