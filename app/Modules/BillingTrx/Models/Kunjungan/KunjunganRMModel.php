<?php

namespace BillingTrx\Models\Kunjungan;

use CodeIgniter\Model;

class KunjunganRMModel extends Model
{
    protected $DBGroup          = 'DBBillingTrx';
    protected $table            = 'kunjungan_rekam_medis';
    protected $allowedFields    = [
        'id_kunjungan',
        'keluhan',
        'kasus',
        'kecelakaan',
        'gawat_darurat',
        'is_kunjungan_baru',
        'masuk_asal',
        'masuk_diag',
        'keluar_cara',
        'keluar_keadaan',
        'keluar_dirujuk',
        'alasan_kontrol',
        'rencana_tindakanlanjut',
    ];
    protected $validationRules  = [];

    protected $useSoftDeletes   = false;
    protected $useTimestamps    = false;
}
