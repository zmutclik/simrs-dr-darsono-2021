<?php

namespace BillingTrx\Models\Kunjungan;

use CodeIgniter\Model;

class KunjunganPenjaminModel extends Model
{
    protected $DBGroup          = 'DBBillingTrx';
    protected $table            = 'kunjungan_penjamin';
    protected $allowedFields    = ['id_kunjungan', 'id_penjamin', 'penjamin', 'penjamin_jenis', 'penjamin_type', 'penjamin_kelas', 'urutan'];
    protected $validationRules  = [];

    protected $useSoftDeletes   = false;
    protected $useTimestamps    = false;
}
