<?php

namespace BillingTrx\Models\Kunjungan;

use CodeIgniter\Model;

class KunjunganKecelakaanModel extends Model
{
    protected $DBGroup          = 'DBBillingTrx';
    protected $table            = 'kunjungan_kecelakaan';
    protected $allowedFields    = [
        'id_kunjungan',
        'tanggal',
        'kdPenjamin',
        'penjamin',
        'kdPropinsi',
        'provinsi',
        'kdKabupaten',
        'kabupaten',
        'kdKecamatan',
        'kecamatan',
        'keterangan',
    ];
    protected $validationRules  = [
        'id_kunjungan'  => 'required',
        'tanggal'       => 'required',
        'kdPenjamin'    => 'required',
        'kdPropinsi'    => 'required',
        'kdKabupaten'   => 'required',
        'kdKecamatan'   => 'required',
    ];

    protected $useSoftDeletes   = false;
    protected $useTimestamps    = false;
}
