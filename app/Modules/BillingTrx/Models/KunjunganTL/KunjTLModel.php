<?php

namespace BillingTrx\Models\KunjunganTL;

use App\Models\Kunjungan\TindakanModel as KunjunganTindakanModel;
use CodeIgniter\Model;
use BillingTrx\Models\KunjunganTL\KunjTLKeuanganModel;
use BillingMaster\Models\Tindakan\TindakanKelompokFormulaModel;
use BillingMaster\Models\_Setting\DefaultTempatLayananTindakanModel;
use BillingMaster\Models\Tindakan\TindakanModel;
use BillingMaster\Models\PelaksanaLayananModel;
use BillingTrx\Models\KunjunganTLTindakan\KunjunganTLTindakanModel;
use BillingTrx\Models\KunjunganTLTindakan\KunjunganTLTindakanPelaksanaModel;

class KunjTLModel extends Model
{
    protected $DBGroup          = 'DBBillingTrx';
    protected $table            = 'kunjungan_tl';
    protected $allowedFields    = ['id', 'id_kunjungan', 'id_tempat_layanan', 'tempat_layanan', 'kelas', 'tgl_mulai', 'tgl_selesai', 'dpjp', 'id_asalkonsul', 'id_asalpindahtt', 'penunjang_id_perujuk', 'penunjang_dokterperujuk', 'penunjang_is_inap', 'is_baru', 'is_selesai'];
    protected $validationRules  = [
        'id_kunjungan'      => 'required',
        'id_tempat_layanan' => 'required',
        'kelas'             => 'required',
        'tgl_mulai'         => 'required',
    ];

    protected $useSoftDeletes   = true;
    protected $useTimestamps    = true;
    public $data            = [];

    protected $afterInsert  = ['_afterInsert'];
    protected $beforeInsert = ['CreateBy'];
    protected $beforeDelete = ['DeleteBy'];

    protected function CreateBy(array $data)
    {
        $data['data']['created_by'] = username();
        $data['data']['uuid'] = uuid();
        return $data;
    }

    protected function DeleteBy(array $data)
    {
        $data['data']['deleted_by'] = username();
        return $data;
    }

    protected function _afterInsert(array $data)
    {
        $KunjunganTLTindakanPelaksana   = new KunjunganTLTindakanPelaksanaModel();
        $DefaultTempatLayananTindakan   = new DefaultTempatLayananTindakanModel();
        $TindakanKelompokFormula        = new TindakanKelompokFormulaModel();
        $KunjunganTLTindakan            = new KunjunganTLTindakanModel();
        $KunjTLKeuangan                 = new KunjTLKeuanganModel();

        $masterTindakan                 = new TindakanModel();
        $masterPelaksanaLayanan         = new PelaksanaLayananModel();

        $KunjTLKeuangan->insert(array('id_kunjungan_tl' => $data['id']));

        $DefaultTindakan = $DefaultTempatLayananTindakan
        ->where(['id_tempat_layanan' => $data['data']['id_tempat_layanan']])->findAll();

        foreach ($DefaultTindakan as $tindakan) {
            $tindakan__ = $masterTindakan->find($tindakan['id_tindakan']);
            $id_trxTindakan = $KunjunganTLTindakan->insert([
                'id_kunjungan_tl'   => $data['id'],
                'id_kunjungan'      => $data['data']['id_kunjungan'],
                'id_tindakan'       => $tindakan['id_tindakan'],
                'tindakan'          => $tindakan__['nama'],
                'tanggal'           => $data['data']['tgl_mulai'],
                'kelas'             => $data['data']['kelas'],
                'qty'               => 1,
            ]);
            
            $TindakanFormula = $TindakanKelompokFormula->where('id_kelompok', $KunjunganTLTindakan->data['tindakan']['id_kelompok'])->findAll();
            foreach ($TindakanFormula as $value) {
                $arr['id_kunjungan_tl_tindakan']    = $id_trxTindakan;
                $arr['id_pelaksana_layanan']        = $value['id_pelaksana_layanan'];
                $arr['pelaksana_layanan']           = $value['pelaksana_layanan'];
                $arr['id_pelaksana']                = 0;
                $arr['pelaksana']                   = 0;
                $arr['type']                        = $value['type'];
                $arr['formula']                     = $value['formula'];
                $arr['nilai']                       = $KunjunganTLTindakan->data['data']['biaya'] * $value['formula_prosen'];

                // dd($arr);
                $KunjunganTLTindakanPelaksana->insert($arr);
                // dd($KunjunganTLTindakanPelaksana->errors());
            }
        }


        $this->data = $data['data'];
    }



    public function tempat_layanan_options($id_kunjungan)
    {
        $options = $this->where('id_kunjungan', $id_kunjungan)->findAll();
        $data = [];
        // $data[''] = '';
        foreach ($options as $value)
            $data[$value['uuid']] = $value['tempat_layanan'];
        return $data;
    }
}
