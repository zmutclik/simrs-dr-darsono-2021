<?php

namespace BillingTrx\Models\KunjunganTL;

use CodeIgniter\Model;

class KunjTLKeuanganModel extends Model
{
    protected $DBGroup          = 'DBBillingTrx';
    protected $table            = 'kunjungan_tl_keuangan';
    protected $allowedFields    = ['id_kunjungan_tl', 'total_biaya', 'total_jaminan', 'total_potongan'];
    protected $validationRules  = [
        'id_kunjungan_tl'      => 'required',
    ];

    protected $useSoftDeletes   = false;
    protected $useTimestamps    = false;
}
