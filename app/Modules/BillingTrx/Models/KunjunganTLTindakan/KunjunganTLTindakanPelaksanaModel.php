<?php

namespace BillingTrx\Models\KunjunganTLTindakan;

use CodeIgniter\Model;

class KunjunganTLTindakanPelaksanaModel extends Model
{
    protected $DBGroup          = 'DBBillingTrx';
    protected $table            = 'kunjungan_tl_tindakan_pelaksana';
    protected $allowedFields    = ['id_kunjungan_tl_tindakan', 'id_pelaksana_layanan', 'pelaksana_layanan', 'id_pelaksana', 'pelaksana', 'type', 'formula', 'nilai'];
    protected $validationRules  = [
        'id_kunjungan_tl_tindakan' => 'required',
        // 'id_pelaksana_layanan' => 'required',
        // 'id_pelaksana' => 'required',
    ];

    protected $useSoftDeletes   = true;
    protected $useTimestamps    = true;
    public $data            = [];


    protected $afterInsert  = ['_afterInsert'];
    protected $beforeInsert = ['_beforeInsert'];
    protected $beforeDelete = ['_beforeDelete'];

    protected function _beforeInsert(array $data)
    {
        $data['data']['created_by'] = username();
        return $data;
    }

    protected function _beforeDelete(array $data)
    {
        $data['data']['deleted_by'] = username();
        return $data;
    }

    protected function _afterInsert(array $data)
    {
    }
}
