<?php

namespace BillingTrx\Models\KunjunganTLTindakan;

use CodeIgniter\Model;
use BillingMaster\Models\TindakanModel;
use BillingTrx\Models\KunjunganTLTindakan\KunjunganTLTindakanKeuanganModel;

class KunjunganTLTindakanModel extends Model
{
    protected $DBGroup          = 'DBBillingTrx';
    protected $table            = 'kunjungan_tl_tindakan';
    protected $allowedFields    = [
        'id_kunjungan_tl',
        'uuid',
        'id_kunjungan',
        'id_tindakan',
        'tindakan',
        'tanggal',
        'is_cito',
        'kelas',
        'qty',
        'harga',
        'keterangan',
        'is_verifikasi',
        'is_tempat_layanan',
    ];
    protected $validationRules  = [
        'id_kunjungan_tl'   => 'required',
        'id_kunjungan'      => 'required',
        'id_tindakan'       => 'required',
        'tanggal'           => 'required',
        'kelas'             => 'required',
        'qty'               => 'required',
    ];

    protected $useSoftDeletes   = true;
    protected $useTimestamps    = true;
    public $data            = [];

    protected $afterInsert  = ['_afterInsert'];
    protected $beforeInsert = ['CreateBy'];
    protected $beforeDelete = ['DeleteBy'];

    protected function CreateBy(array $data)
    {
        $this->data['qty']          = $data['data']['qty'];
        $this->data['kelas']        = $data['data']['kelas'];
        $this->data['id_kunjungan'] = $data['data']['id_kunjungan'];

        unset($data['data']['qty']);
        unset($data['data']['kelas']);
        unset($data['data']['id_kunjungan']);
        
        $data['data']['uuid'] = uuid();

        $data['data']['created_by'] = username();
        return $data;
    }

    protected function DeleteBy(array $data)
    {
        $data['data']['deleted_by'] = username();
        return $data;
    }

    protected function _afterInsert(array $data)
    {
        $KunjunganTLTindakanKeuangan = new KunjunganTLTindakanKeuanganModel();
        $Tindakan                    = new TindakanModel();

        $tindakan_val                = $Tindakan->find($data['data']['id_tindakan']);
        $tindakan_tarif              = $tindakan_val['tarif_' . $this->data['kelas']];

        $KunjunganTLTindakanKeuangan->insert([
            'id_kunjungan_tl_tindakan'  => $data['id'],
            'id_kunjungan_tl'           => $data['data']['id_kunjungan_tl'],
            'id_kunjungan'              => $this->data['id_kunjungan'],
            'biaya'                     => $tindakan_tarif,
            'qty'                       => $this->data['qty'],
            'sub_total'                 => $this->data['qty'] * $tindakan_tarif,
        ]);

        $data['tindakan']   = $tindakan_val;
        $data['data']['biaya']      = $tindakan_tarif;
        $data['data']['qty']        = $this->data['qty'];
        $data['data']['sub_total']  = $this->data['qty'] * $tindakan_tarif;

        $this->data = $data;
    }
}
