<?php

namespace BillingTrx\Models\KunjunganTLTindakan;

use CodeIgniter\Model;

class KunjunganTLTindakanKeuanganModel extends Model
{
    protected $DBGroup          = 'DBBillingTrx';
    protected $table            = 'kunjungan_tl_tindakan_keuangan';
    protected $allowedFields    = [
        'id_kunjungan_tl_tindakan',
        'id_kunjungan_tl',
        'id_kunjungan',
        'biaya',
        'qty',
        'sub_total',
        'nilai_jaminan',
        'nilai_potongan',
    ];
    protected $validationRules  = [
        'id_kunjungan_tl_tindakan'  => 'required',
        'id_kunjungan_tl'           => 'required',
        'id_kunjungan'              => 'required',
        'biaya'                     => 'required',
        'qty'                       => 'required',
    ];

    protected $useSoftDeletes   = false;
    protected $useTimestamps    = false;
}
