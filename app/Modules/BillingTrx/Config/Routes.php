<?php

/**
 * BillingTrx routes file.
 */

$routes->group('', ['namespace' => 'BillingTrx\Controllers'], function ($routes) {

	// DAFTAR IGD POLI/
	$routes->get('kunjungan/daftar', 'Daftar\DaftarController::index');
	$routes->get('kunjungan/daftar/(:uuid)', 'Daftar\DaftarController::form/$1');
	$routes->post('kunjungan/daftar/(:uuid)/save', 'Daftar\DaftarController::Save');
	// $routes->get('kunjungan/daftar/new', 'Daftar\DaftarController::Form/save');
	// $routes->get('kunjungan/daftar/(:num)/edit', 'Daftar\DaftarController::Form/edit/$1');
	// $routes->get('kunjungan/daftar/(:num)/show', 'Daftar\DaftarController::Show/$1');
	// $routes->post('kunjungan/daftar/(:num)/edit', 'Daftar\DaftarController::Save/edit/$1');
	// $routes->post('kunjungan/daftar/(:num)/delete', 'Daftar\DaftarController::Delete/$1');
	// $routes->post('kunjungan/daftar/Table', 'Daftar\DaftarController::Table');

	// TINDAKAN/
	$routes->get('kunjungan/tindakan', 'Tindakan\TindakanController::index');
	$routes->get('kunjungan/tindakan/(:uuid)', 'Tindakan\TindakanController::Form/$1');
	$routes->post('kunjungan/tindakan/(:uuid)/Table', 'Tindakan\TindakanController::Table/$1');
	$routes->get('kunjungan/tindakan/(:uuid)/tindakan', 'Tindakan\TindakanController::tindakan/$1');
	$routes->get('kunjungan/tindakan/(:uuid)/tindakanKelompok', 'Tindakan\TindakanController::tindakan_pelaksana/$1');

});
