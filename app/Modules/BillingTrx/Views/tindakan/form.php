<?= $this->extend("template/index") ?>

<?= $this->section('content_header') ?>
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-0 pb-1 mb-0 border-bottom">
    <h1 class="h3">Entry Tindakan</h1>
    <div class="btn-toolbar mb-2 mb-md-0"></div>
</div>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-8">
                <div class="card">
                    <div class="row">
                        <div class="col-bg-4 p-3 border text-center">
                            <div class="display-4"><strong><?= $pasien['no_rm'] ?></strong></div>
                            <div class="card-text"><?= $pasien['kelamin'] ?> - <?= $pasien['umur'] ?></div>
                            <div class="card-text"><?= $pasien['lahir'] ?></div>
                            <div class="h3"><?= $pasien['tlp'] ?></div>
                        </div>
                        <div class="col-md-8">
                            <div class="card-body ">
                                <div class="h2"><?= $pasien['nama'] ?></div>
                                <div class="card-text"><?= $pasien['alamat_'] ?></div>

                                <div class="card-text"><small class="text-muted">Ibu : <?= $pasien['nama_ibu'] ?></small></div>
                                <div class="card-text"><small class="text-muted">Bapak : <?= $pasien['nama_bapak'] ?></small></div>
                                <div class="card-text"><small class="text-muted">Suami/Istri : <?= $pasien['nama_sumistri'] ?></small></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">

                <div class="card">
                    <div class="card-body row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <?= form_label('Penjamin', 'penjamin',  ['class' => 'col-sm-3 col-form-label']); ?>
                                <div class="col-sm-9">
                                    <?= form_input($form['penjamin']) ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <?= form_label('T.Layanan', 'tempat_layanan',  ['class' => 'col-sm-3 col-form-label']); ?>
                                <div class="col-sm-9">
                                    <?= form_dropdown($form['tempat_layanan'], $tempat_layanan_options, $form['tempat_layanan']['value']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["tempat_layanan"]) ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="row">
                                <?= form_label('DPJP', 'dokter_dpjp',  ['class' => 'col-sm-3 col-form-label']); ?>
                                <div class="col-sm-9">
                                    <?= form_dropdown($form['dokter_dpjp'], $dokter_dpjp_options, $form['dokter_dpjp']['value']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["dokter_dpjp"]) ?></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-5">
                <?= form_open($urlForm, $form['form']) ?>
                <input type="hidden" class="<?= md5(csrf_hash()) ?>" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>">

                <div class="card">
                    <div class="card-body">
                        <div class="row">

                            <div class="form-group row col-md-12">
                                <?= form_label('Tanggal', 'tindakan_tanggal',  ['class' => 'col-sm-3 col-form-label']); ?>
                                <div class="col-sm-9">

                                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                        <?= form_input($form['tindakan_tanggal']) ?>
                                        <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                    <div class="invalid-feedback"><?= esc($errors["tindakan_tanggal"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row col-md-12">
                                <?= form_label('Tindakan', 'tindakan',  ['class' => 'col-sm-3 col-form-label']); ?>
                                <div class="col-sm-9">
                                    <?= form_dropdown($form['tindakan'], $tindakan_options, $form['tindakan']['value']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["tindakan"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row col-md-12">
                                <?= form_label('Jumlah', 'tindakan_jumlah',  ['class' => 'col-sm-3 col-form-label']); ?>
                                <div class="col-sm-3">
                                    <?= form_input($form['tindakan_jumlah']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["tindakan_jumlah"]) ?></div>
                                </div>
                            </div>
                            <div class="form-group row col-md-12">
                                <?= form_label('Biaya', 'tindakan_biaya',  ['class' => 'col-sm-3 col-form-label']); ?>
                                <div class="col-sm-8">
                                    <?= form_input($form['tindakan_biaya']) ?>
                                    <div class="invalid-feedback"><?= esc($errors["tindakan_biaya"]) ?></div>
                                </div>
                            </div>
                            <div class="form_tindakan_pelaksana row col-md-12">

                            </div>
                            <div class="form-group row col-12 justify-content-end">

                                <?= form_button($form['buttons']['tambah']) ?>
                                <?= form_button($form['buttons']['batal']) ?>
                                <?= form_button($form['buttons']['submit']) ?>
                            </div>
                        </div>

                    </div>

                </div>

                <?= form_close() ?>
            </div>

            <div class="col-7">
                <div class="card">
                    <div class="card-body">
                        <table id="data_table" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>RUANGAN</th>
                                    <th>TANGAL</th>
                                    <th>TINDAKAN</th>
                                    <th>JML</th>
                                    <th>BIAYA</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="dataTables_empty">Loading data from server</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>