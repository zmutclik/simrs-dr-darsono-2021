<?= $this->extend('template/index') ?>

<?= $this->section('content_header') ?>
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h3">Pendaftaran Rawat Jalan / IGD</h1>
    <div class="btn-toolbar mb-2 mb-md-0"></div>
</div>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <?= form_open($urlForm, $form['form']) ?>
                <input type="hidden" class="<?= md5(csrf_hash()) ?>" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Data Pasien</h3>
                        <div class="card-tools"><button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fas fa-minus"></i></button><button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove"><i class="fas fa-times"></i></button></div>
                    </div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <?= form_label('NORM', 'no_rm',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="input-group col-sm-3">
                                        <?= form_input($form['no_rm']) ?>
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary" type="button" data-toggle="modal" data-target="#FormSearchPasien" style="border: 1px solid #ced4da;">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="invalid-feedback"><?= esc($errors["no_rm"]) ?></div>
                                </div>
                                <div class="form-group row">
                                    <?= form_label('Nama', 'nama',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_input($form['nama']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["nama"]) ?></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <?= form_label('Lahir', 'lahir',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_input($form['lahir']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["lahir"]) ?></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <?= form_label('Umur', 'umur',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_input($form['umur']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["umur"]) ?></div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <?= form_label('Telp', 'telp',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_input($form['telp']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["telp"]) ?></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <?= form_label('Nama Ibu', 'nama_ibu',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_input($form['nama_ibu']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["nama_ibu"]) ?></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <?= form_label('Nama Bapak', 'nama_bapak',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_input($form['nama_bapak']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["nama_bapak"]) ?></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <?= form_label('Suami / Istri', 'nama_sumistri',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_input($form['nama_sumistri']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["nama_sumistri"]) ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <?= form_label('Alamat', 'alamat_',  ['class' => 'col-sm-1 col-form-label']); ?>
                                    <div class="col-sm-11">
                                        <?= form_input($form['alamat_']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["alamat_"]) ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Penjamin Pasien</h3>
                        <div class="card-tools"><button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fas fa-minus"></i></button><button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove"><i class="fas fa-times"></i></button></div>
                    </div>
                    <div class="card-body" id="panelpenjamin">
                        <div class="row">

                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <?= form_label('Penjamin', 'id_penjamin',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-8">
                                        <?= form_dropdown($form['id_penjamin'], $id_penjamin_options, $form['id_penjamin']['value']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["id_penjamin"]) ?></div>
                                    </div>
                                    <div class="col-sm-2">
                                        <?= form_dropdown($form['penjamin_kelas'], $penjamin_kelas_options, $form['penjamin_kelas']['value']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["penjamin_kelas"]) ?></div>
                                    </div>
                                </div>
                                <div class="form-group row panelbpjs">
                                    <?= form_label('NoBPJS', 'no_anggota',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-5">
                                        <?= form_input($form['no_anggota']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["no_anggota"]) ?></div>
                                    </div>
                                    <div class="col-sm-5">
                                        <?= form_input($form['penjamin_jenis']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["penjamin_jenis"]) ?></div>
                                    </div>
                                </div>

                                <div class="form-group row panelbpjs">
                                    <?= form_label('Penjamin COB', 'penjamin_cob',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <div class="form-check">
                                            <?= form_checkbox($form['penjamin_cob'], 1, isset($form['penjamin_cob']['value'])) ?>
                                            <?= form_label('Check Penjamin COB', 'penjamin_cob',  ['class' => 'form-check-label']); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row panelbpjs">
                                    <?= form_label('SEP RJ', 'sep_rj',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10 input-group">
                                        <?= form_input($form['sep_rj']) ?>
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary" type="button" id="btn_seprj">BUAT</button>
                                        </div>
                                        <div class="invalid-feedback"><?= esc($errors["sep_rj"]) ?></div>
                                    </div>
                                </div>


                            </div>

                            <div class="col-lg-6">
                                <div class="form-group row panelbpjs">
                                    <?= form_label('NoRujukan', 'no_rujukan',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-7">
                                        <?= form_dropdown($form['no_rujukan'], $no_rujukan_options, $form['no_rujukan']['value']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["no_rujukan"]) ?></div>
                                    </div>
                                    <div class="col-sm-3">
                                        <?= form_input($form['tgl_rujukan']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["tgl_rujukan"]) ?></div>
                                    </div>
                                </div>

                                <div class="form-group row panelbpjs">
                                    <?= form_label('Kontrol', 'skdp',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-6">
                                        <?= form_dropdown($form['skdp'], $skdp_options, $form['skdp']['value']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["skdp"]) ?></div>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= form_dropdown($form['skdp_dr_perujuk'], $skdp_options, $form['skdp_dr_perujuk']['value']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["skdp_dr_perujuk"]) ?></div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <?= form_label('LakaLantas', 'isLakaLantas',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <div class="form-check">
                                            <?= form_checkbox($form['isLakaLantas'], 1, FALSE) ?>
                                            <?= form_label('Check LakaLantas', 'isLakaLantas',  ['class' => 'form-check-label']); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row panelbpjslaka">
                                    <?= form_label('Tanggal', 'tglKejadian',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">

                                        <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                            <?= form_input($form['tglKejadian']) ?>
                                            <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                        <div class="invalid-feedback"><?= esc($errors["tglKejadian"]) ?></div>
                                    </div>
                                </div>


                                <div class="form-group row panelbpjslaka">
                                    <?= form_label('Laka Lantas', 'lakaLantas',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_dropdown($form['lakaLantas'], $lakaLantas_options, $form['lakaLantas']['value']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["lakaLantas"]) ?></div>
                                    </div>
                                </div>

                                <div class="form-group row panelbpjslaka">
                                    <?= form_label('Lokasi Prov', 'kdPropinsi',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_dropdown($form['kdPropinsi'], $kdPropinsi_options, $form['kdPropinsi']['value']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["kdPropinsi"]) ?></div>
                                    </div>
                                </div>
                                <div class="form-group row panelbpjslaka">
                                    <?= form_label('Lokasi Kab', 'kdKabupaten',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_dropdown($form['kdKabupaten'], $kdKabupaten_options, $form['kdKabupaten']['value']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["kdKabupaten"]) ?></div>
                                    </div>
                                </div>
                                <div class="form-group row panelbpjslaka">
                                    <?= form_label('Lokasi Kec', 'kdKecamatan',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_dropdown($form['kdKecamatan'], $kdKecamatan_options, $form['kdKecamatan']['value']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["kdKecamatan"]) ?></div>
                                    </div>
                                </div>
                                <div class="form-group row panelbpjslaka">
                                    <?= form_label('Keterangan', 'keterangan_kecelakaan',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_input($form['keterangan_kecelakaan']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["keterangan_kecelakaan"]) ?></div>
                                    </div>
                                </div>
                                <div class="form-group row panelbpjs">
                                    <?= form_label('Suplesi', 'suplesi',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-1">
                                        <div class="form-check">
                                            <?= form_checkbox($form['suplesi'], 1, isset($form['suplesi']['value'])) ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 panelbpjs">
                                        <?= form_dropdown($form['noSepSuplesi'], $noSepSuplesi_options, $form['noSepSuplesi']['value']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["noSepSuplesi"]) ?></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Kunjungan Rawat Jalan</h3>
                        <div class="card-tools"><button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fas fa-minus"></i></button><button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove"><i class="fas fa-times"></i></button></div>
                    </div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group row col-md-12">
                                    <?= form_label('Tanggal', 'tgl_mulai',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">

                                        <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                            <?= form_input($form['tgl_mulai']) ?>
                                            <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                        <div class="invalid-feedback"><?= esc($errors["tgl_mulai"]) ?></div>
                                    </div>
                                </div>

                                <div class="form-group row col-md-12">
                                    <?= form_label('Asal Masuk', 'masuk_asal',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_dropdown($form['masuk_asal'], $masuk_asal_options, $form['masuk_asal']['value']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["masuk_asal"]) ?></div>
                                    </div>
                                </div>

                                <div class="form-group row col-md-12">
                                    <?= form_label('Diag Masuk', 'masuk_diag',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_dropdown($form['masuk_diag'], $masuk_diag_options, $form['masuk_diag']['value']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["masuk_diag"]) ?></div>
                                    </div>
                                </div>

                                <div class="form-group row col-md-12">
                                    <?= form_label('Keluhan', 'keluhan',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_input($form['keluhan']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["keluhan"]) ?></div>
                                    </div>
                                </div>

                            </div>


                            <div class="col-6">
                                <div class="form-group row">
                                    <?= form_label('Kasus', 'kasus',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_dropdown($form['kasus'], $kasus_options, $form['kasus']['value']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["kasus"]) ?></div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <?= form_label('Tempat Layanan', 'id_tempat_layanan',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_dropdown($form['id_tempat_layanan'], $id_tempat_layanan_options, $form['id_tempat_layanan']['value']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["id_tempat_layanan"]) ?></div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <?= form_label('Status', 'kunjungan_status',  ['class' => 'col-sm-2 col-form-label']); ?>
                                    <div class="col-sm-10">
                                        <?= form_dropdown($form['kunjungan_status'], $kunjungan_status_options, $form['kunjungan_status']['value']) ?>
                                        <div class="invalid-feedback"><?= esc($errors["kunjungan_status"]) ?></div>
                                    </div>
                                </div>


                            </div>

                            <div class="row col-12 form-btn" style="display: none;">
                                <div class="col-11"></div>
                                <div class="col-1 float-right">
                                    <?= form_button($form['buttons']['submit']) ?>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <?= form_close() ?>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="FormSearchPasien" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pencarian Pasien</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="FormSearch" class="col-sm-2 col-form-label">Penelusuran</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="FormSearch">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="FormSearchAlamat" class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="FormSearchAlamat">
                    </div>
                </div>
                <table id="searchPasien_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>PASIEN</th>
                            <th>ALAMAT</th>
                            <th>UMUR</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="dataTables_empty">Loading data from server</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection() ?>