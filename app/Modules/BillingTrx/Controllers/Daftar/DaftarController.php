<?php

namespace BillingTrx\Controllers\Daftar;

use CodeIgniter\I18n\Time;
use App\Controllers\BaseController;
use App\Libraries\Datatables;

use BillingTrx\Models\ModelsKunjungan;
use PDO;

class DaftarController extends BaseController
{
    protected $form = [
        'form' => [
            'id' => 'form',
            'class' => 'needs-validation',
        ],
        /// PASIEN ///
        'no_rm' => [
            'name' => 'no_rm',
            'id' => 'no_rm',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'NO Rekam Medis',
            'required' => '',
            'maxlength' => '6',
        ],
        'telp' => [
            'id' => 'telp',
            'name' => 'telp',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => '081',
            'required' => '',
        ],
        'nama' => [
            'id' => 'nama',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Nama Pasien',
            'readonly' => ''
        ],
        'nama_ibu' => [
            'id' => 'nama_ibu',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Nama IBU Pasien',
            'readonly' => ''
        ],
        'nama_bapak' => [
            'id' => 'nama_bapak',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Nama  BAPAK Pasien',
            'readonly' => ''
        ],
        'nama_sumistri' => [
            'id' => 'nama_sumistri',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Nama Suami/Istri Pasien',
            'readonly' => ''
        ],
        'lahir' => [
            'id' => 'lahir',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Tempat, Tanggal Lahir',
            'readonly' => ''
        ],
        'umur' => [
            'id' => 'umur',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Umur Pasien',
            'readonly' => ''
        ],
        'alamat_' => [
            'id' => 'alamat',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Alamat Lengkap',
            'readonly' => ''
        ],

        /// PENJAMIN
        'id_penjamin' => [
            'name' => 'id_penjamin',
            'id' => 'id_penjamin',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'penjamin_kelas' => [
            'name' => 'penjamin_kelas',
            'id' => 'penjamin_kelas',
            'value' => 2,
            'class' => 'form-control',
            'required' => '',
        ],


        'no_anggota' => [
            'name' => 'no_anggota',
            'id' => 'no_anggota',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'No Kartu BPJS',
        ],
        'penjamin_jenis' => [
            'name' => 'penjamin_jenis',
            'id' => 'penjamin_jenis',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Jenis Keanggotaan',
            'readonly' => ''
        ],
        'no_rujukan' => [
            'name' => 'no_rujukan',
            'id' => 'no_rujukan',
            'value' => null,
            'class' => 'form-control',
        ],
        'kode_tempat_layanan' => [
            'name' => 'kode_tempat_layanan',
            'id' => 'kode_tempat_layanan',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'KODE',
            'readonly' => ''
        ],
        'tgl_rujukan' => [
            'name' => 'tgl_rujukan',
            'id' => 'tgl_rujukan',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Tgl Rujukan',
            'readonly' => ''
        ],
        'skdp' => [
            'name' => 'skdp',
            'id' => 'skdp',
            'value' => null,
            'class' => 'form-control',
        ],
        'skdp_dr_perujuk' => [
            'name' => 'skdp_dr_perujuk',
            'id' => 'skdp_dr_perujuk',
            'value' => null,
            'class' => 'form-control',
        ],
        'penjamin_cob' => [
            'type' => 'checkbox',
            'name' => 'penjamin_cob',
            'id' => 'penjamin_cob',
            'value'   => null,
            'class' => 'form-check-input',
        ],
        'sep_rj' => [
            'name' => 'sep_rj',
            'id' => 'sep_rj',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'No SEP Rawat Jalan',
        ],
        'sep_ri' => [
            'name' => 'sep_ri',
            'id' => 'sep_ri',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'No SEP Rawat Inap',
        ],

        /// PENJAMIN KASUS KECELAKAAN
        'isLakaLantas' => [
            'type' => 'checkbox',
            'name' => 'isLakaLantas',
            'id' => 'isLakaLantas',
            'value'   => 1,
            'class' => 'form-check-input',
        ],
        'tglKejadian' => [
            'name' => 'tglKejadian',
            'id' => 'tglKejadian',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Tanggal Kejadian',
        ],
        'lakaLantas' => [
            'name' => 'lakaLantas',
            'id' => 'lakaLantas',
            'value' => null,
            'class' => 'form-control',
        ],
        'kdPropinsi' => [
            'name' => 'kdPropinsi',
            'id' => 'kdPropinsi',
            'value' => null,
            'class' => 'form-control',
        ],
        'kdKabupaten' => [
            'name' => 'kdKabupaten',
            'id' => 'kdKabupaten',
            'value' => null,
            'class' => 'form-control',
        ],
        'kdKecamatan' => [
            'name' => 'kdKecamatan',
            'id' => 'kdKecamatan',
            'value' => null,
            'class' => 'form-control',
        ],
        'keterangan_kecelakaan' => [
            'name' => 'keterangan_kecelakaan',
            'id' => 'keterangan_kecelakaan',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Keterangan Kecelakaan',
        ],
        'suplesi' => [
            'type' => 'checkbox',
            'name' => 'suplesi',
            'id' => 'suplesi',
            'value'   => null,
            'class' => 'form-check-input',
        ],
        'noSepSuplesi' => [
            'name' => 'noSepSuplesi',
            'id' => 'noSepSuplesi',
            'value' => null,
            'class' => 'form-control',
        ],



        'tgl_mulai' => [
            'name' => 'tgl_mulai',
            'id' => 'tgl_mulai',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Tanggal Daftar',
            'required' => '',
        ],
        'masuk_asal' => [
            'name' => 'masuk_asal',
            'id' => 'masuk_asal',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'masuk_diag' => [
            'name' => 'masuk_diag',
            'id' => 'masuk_diag',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'kasus' => [
            'name' => 'kasus',
            'id' => 'kasus',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'id_tempat_layanan' => [
            'name' => 'id_tempat_layanan',
            'id' => 'id_tempat_layanan',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'id_tindakan' => [
            'name' => 'id_tindakan',
            'id' => 'id_tindakan',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'keluhan' => [
            'name' => 'keluhan',
            'id' => 'keluhan',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Keluhan Pasien',
            'required' => '',
        ],
        'kunjungan_status' => [
            'name' => 'kunjungan_status',
            'id' => 'kunjungan_status',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],

        'buttons' => [
            'back' => ['type' => 'button', 'class' => 'btn btn-danger hBack', 'content' => 'KEMBALI/BATAL',], 'submit' => ['type' => 'button', 'id' => 'btn_submit', 'class' => 'btn btn-danger  float-right', 'content' => 'SIMPAN',], 'delete' => ['type' => 'button', 'class' => 'btn btn-danger', 'content' => 'HAPUS',], 'edit' => ['type' => 'button', 'class' => 'btn btn-danger', 'content' => 'KOREKSI',],
        ]
    ];

    protected $fields = [
        /// PASIEN
        'no_rm', 'telp', 'nama', 'alamat', 'alamat_', 'lahir', 'umur', 'nama_ibu', 'nama_bapak', 'nama_sumistri',
        /// PENJAMIN
        'id_penjamin', 'penjamin_kelas', 'no_anggota', 'penjamin_jenis', 'no_rujukan', 'kode_tempat_layanan', 'tgl_rujukan', 'skdp', 'skdp_dr_perujuk', 'penjamin_cob',
        'sep_rj', 'sep_ri',
        /// PENJAMIN KASUS KECELAKAAN
        'isLakaLantas', 'tglKejadian', 'lakaLantas', 'kdPropinsi', 'kdKabupaten', 'kdKecamatan', 'keterangan_kecelakaan', 'suplesi', 'noSepSuplesi',
        /// KUNJUNGAN
        'tgl_mulai', 'masuk_asal', 'masuk_diag', 'kasus', 'id_tempat_layanan', 'id_tindakan', 'keluhan', 'kunjungan_status',
    ];

    protected $pathTemplate = "BillingTrx\Views\daftar\\";

    protected $model;

    protected $permission;

    public function __construct()
    {
        $this->model = new ModelsKunjungan();
    }

    public function before_template()
    {
        $this->tag_datatables();
        $this->data['link_tag'][] = 'node_modules/select2/dist/css/select2.min.css';
        $this->data['link_tag'][]     = 'node_modules/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css';

        $this->data['script_tag'][] = 'node_modules/moment/min/moment-with-locales.min.js';
        $this->data['script_tag'][] = 'node_modules/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js';
        $this->data['script_tag'][] = 'node_modules/select2/dist/js/select2.min.js';
        $this->data['script_tag'][] = 'node_modules/jquery-validation/dist/jquery.validate.js';
        $this->data['script_tag'][] = 'js/pages/BillingTrx/pendaftaran/Daftar.js';
        $this->data['script_tag'][] = 'js/pages/BillingTrx/pendaftaran/Daftar.submit.js';

        $this->data['masuk_asal_options']   = $this->model->referensi_asalMasuk->options();
        $this->data['kasus_options']        = $this->model->referensi_kasus->options();
        $this->data['masuk_diag_options']   = [];

        $this->data['id_tempat_layanan_options']    = $this->model->tempat_layanan->options('Rawat Jalan');
        $this->data['id_penjamin_options']          = $this->model->penjamin->options();
        $this->data['penjamin_kelas_options']       = ['1' => 'Kls.1', '2' => 'Kls.2', '3' => 'Kls.3'];
        $this->data['kunjungan_status_options']     = $this->model->referensi_kunjunganStatus->options();

        $this->data['no_rujukan_options']       = ['' => ''];
        $this->data['skdp_options']             = ['' => ''];
        $this->data['skdp_dr_perujuk_options']  = ['' => ''];
        $this->data['lakaLantas_options']       = ['' => '', '1' => 'Jasa raharja PT', '2' => 'BPJS Ketenagakerjaan', '3' => 'TASPEN PT', '3' => 'ASABRI PT'];

        $this->data['kdPropinsi_options']   = ['' => ''];
        $this->data['kdKabupaten_options']  = ['' => ''];
        $this->data['kdKecamatan_options']  = ['' => ''];
        $this->data['noSepSuplesi_options'] = ['' => ''];
    }

    public function index()
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');

        $this->data['urlForm'] = my_uri('');
        $this->template($this->pathTemplate . 'form');
    }

    public function form($uuid)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');

        $this->data['urlForm'] = my_uri('save');
        $this->form['tgl_mulai']['value'] = date("d M Y H:i");

        try {
            foreach ($this->model->pasien->where('uuid', $uuid)->first() as $key => $value)
                if (in_array($key, $this->fields))
                    $this->form[$key]['value'] = $value;
        } catch (\Throwable $th) {
            throw $th;
        }

        $this->template($this->pathTemplate . 'form');
    }

    public function Save()
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');

        $this->before_template();

        $this->data['urlForm'] = 'save';
        $this->form['form']['class'] = 'was-validated';
        $input = [];
        $data = [];
        foreach ($this->fields as $value) $input[$value] = $this->request->getPost($value);

        $penjamin       = $this->model->penjamin->find($input['id_penjamin']);
        $tempat_layanan = $this->model->tempat_layanan->find($input['id_tempat_layanan']);
        $time           = Time::parse($input['tgl_mulai']);
        $time_kecelakaan = Time::parse($input['tglKejadian']);

        $data['kunjungan']['no_rm']             = $input['no_rm'];
        $data['kunjungan']['tgl_mulai']         = $time->toDateTimeString();
        $data['kunjungan']['perawatan_kelas']   = $tempat_layanan['kelas_default'];
        $data['kunjungan']['status']            = $input['kunjungan_status'];
        $data['kunjungan']['id']                = $this->model->kunjungan->insert($data['kunjungan']);

        // $data['kunjungan']['keterangan']        = $input['keterangan'];

        $data['kunjungan_penjamin']['id_kunjungan']     = $data['kunjungan']['id'];
        $data['kunjungan_penjamin']['id_penjamin']      = $input['id_penjamin'];
        $data['kunjungan_penjamin']['penjamin']         = $penjamin['nama'];
        $data['kunjungan_penjamin']['penjamin_jenis']   = $input['penjamin_jenis'];
        $data['kunjungan_penjamin']['penjamin_type']    = $penjamin['type'];
        $data['kunjungan_penjamin']['urutan']           = 1;

        $this->model->kunjungan_penjamin->insert($data['kunjungan_penjamin']);

        $data['kunjungan_rekam_medis']['id_kunjungan']  = $data['kunjungan']['id'];
        $data['kunjungan_rekam_medis']['keluhan']       = $input['keluhan'];
        $data['kunjungan_rekam_medis']['kasus']         = $input['kasus'];
        $data['kunjungan_rekam_medis']['kecelakaan']    = (empty($input['isLakaLantas'])) ? 0 : $input['isLakaLantas'];
        $data['kunjungan_rekam_medis']['masuk_asal']    = $input['masuk_asal'];
        $data['kunjungan_rekam_medis']['masuk_diag']    = $input['masuk_diag'];

        $this->model->kunjungan_rm->insert($data['kunjungan_rekam_medis']);

        $data['kunjungan_jkn']['id_kunjungan']  = $data['kunjungan']['id'];
        $data['kunjungan_jkn']['no_anggota']    = $input['no_anggota'];
        $data['kunjungan_jkn']['no_rujukan']    = $input['no_rujukan'];
        $data['kunjungan_jkn']['skdp']          = $input['skdp'];
        $data['kunjungan_jkn']['sep_rj']        = $input['sep_rj'];
        $data['kunjungan_jkn']['sep_ri']        = $input['sep_ri'];

        if ($data['kunjungan_penjamin']['id_penjamin'] == 2)
            $this->model->kunjungan_jkn->insert($data['kunjungan_jkn']);


        if ($data['kunjungan_rekam_medis']['kecelakaan'] == 1) {

            $data['kasus_kecelakaan']['id_kunjungan']   = $data['kunjungan']['id'];
            $data['kasus_kecelakaan']['tanggal']        = $time_kecelakaan->toDateTimeString();
            $data['kasus_kecelakaan']['kdPenjamin']     = $input['lakaLantas'];
            $data['kasus_kecelakaan']['penjamin']       = $this->data['lakaLantas_options'][$input['lakaLantas']];

            $data['kasus_kecelakaan']['kdPropinsi']     = $input['kdPropinsi'];
            $data['kasus_kecelakaan']['provinsi']       = $this->model->vclaim_wilayah->find($input['kdPropinsi']);
            $data['kasus_kecelakaan']['provinsi']       = $data['kasus_kecelakaan']['provinsi']['nama'];

            $data['kasus_kecelakaan']['kdKabupaten']    = $input['kdKabupaten'];
            $data['kasus_kecelakaan']['kabupaten']      = $this->model->vclaim_wilayah->find($input['kdKabupaten']);
            $data['kasus_kecelakaan']['kabupaten']      = $data['kasus_kecelakaan']['kabupaten']['nama'];

            $data['kasus_kecelakaan']['kdKecamatan']    = $input['kdKecamatan'];
            $data['kasus_kecelakaan']['kecamatan']      = $this->model->vclaim_wilayah->find($input['kdKecamatan']);
            $data['kasus_kecelakaan']['kecamatan']      = $data['kasus_kecelakaan']['kecamatan']['nama'];

            $data['kasus_kecelakaan']['keterangan']     = $input['keterangan_kecelakaan'];

            $this->model->kunjungan_kecelakaan->insert($data['kasus_kecelakaan']);
        }

        $data['kunjungan_tl']['id_kunjungan']       = $data['kunjungan']['id'];
        $data['kunjungan_tl']['id_tempat_layanan']  = $input['id_tempat_layanan'];
        $data['kunjungan_tl']['tempat_layanan']     = $tempat_layanan['nama'];
        $data['kunjungan_tl']['kelas']              = $tempat_layanan['kelas_default'];
        $data['kunjungan_tl']['tgl_mulai']          = $time->toDateTimeString();

        $this->model->kunjungan_tl->insert($data['kunjungan_tl']);


        dd($data);
        // dd($data);
        // if ($this->model->insert($data) === false) $this->data['errors'] = $this->model->errors();
        // else {
        //     $this->form['buttons']['submit']['class'] = 'invisible';
        // }

        // foreach ($this->data['errors'] as $key => $value)
        //     foreach ($this->form as $key_ => $item)
        //         if (!empty($this->form[$key_]['name']))
        //             if ($this->form[$key_]['name'] == $key) $this->form[$key_]['class'] .= ' is-invalid';

        // foreach ($data as $key => $value)
        //     $this->form[$key]['value'] = $data[$key];

        // $this->template($this->pathTemplate . 'form');
    }

    public function Table()
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        if ($this->request->isAJAX()) {
            $datatables = new Datatables();
            $datatables->SetDB('DBBillingMaster');

            $datatables->from('kunjungan')
                ->select("id,no_rm,tgl_mulai")
                ->where('deleted_at', null)
                ->att_column('DT_RowId', '$1', 'id');

            $data['result'] = $datatables->generate();
            $json = json_decode($data['result'], true);
            $json['token'] = csrf_hash();
            return $this->response->setJSON($json);
        }
    }
}
