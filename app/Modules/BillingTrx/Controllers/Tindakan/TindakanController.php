<?php

namespace BillingTrx\Controllers\Tindakan;

use App\Controllers\BaseController;
use App\Libraries\Datatables;

use BillingTrx\Models\ModelsKunjungan;
use BillingTrx\Models\ModelsTindakan;
use Karyawan\Models\KaryawanModel;

class TindakanController extends BaseController
{
    protected $form = [
        'form' => ['id' => 'form', 'class' => 'needs-validation',],

        'tindakan_tanggal' => [
            'name' => 'tanggal',
            'id' => 'tanggal',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Tanggal Tindakan',
            'required' => '',
        ],
        'tindakan' => [
            'name' => 'tindakan',
            'id' => 'tindakan',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],

        'tindakan_pelaksana' => [
            'name' => 'tindakan_pelaksana',
            'id' => 'tindakan_pelaksana',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'tindakan_jumlah' => [
            'name' => 'tindakan_jumlah',
            'id' => 'tindakan_jumlah',
            'type' => 'number',
            'value' => null,
            'class' => 'form-control ',
            'placeholder' => '0',
            'required' => '',
        ],
        'tindakan_biaya' => [
            'name' => 'tindakan_biaya',
            'id' => 'tindakan_biaya',
            'value' => null,
            'class' => 'form-control readonly',
            'placeholder' => '0',
            'readonly' => '',
        ],


        'penjamin' => [
            'name' => 'penjamin',
            'id' => 'penjamin',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => '0',
            'readonly' => '',
        ],
        'tempat_layanan' => [
            'name' => 'tempat_layanan',
            'id' => 'tempat_layanan',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'dokter_dpjp' => [
            'name' => 'dokter_dpjp',
            'id' => 'dokter_dpjp',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],


        'buttons' => [
            'tambah' => ['id' => 'btn_tambah', 'type' => 'button', 'class' => 'btn btn-primary mx-1', 'content' => 'TAMBAH',],
            'submit' => ['id' => 'btn_submit', 'type' => 'button', 'class' => 'btn btn-success mx-1', 'content' => 'SIMPAN',],
            'batal' => ['id' => 'btn_batal', 'type' => 'button', 'class' => 'btn btn-secondary mx-1', 'content' => 'BATAL',],
        ]
    ];

    protected $fields = [
        'tempat_layanan', 'dokter_dpjp', 'penjamin',
        'tindakan_tanggal', 'tindakan', 'tindakan_pelaksana', 'tindakan_jumlah', 'tindakan_biaya',
    ];

    protected $pathTemplate = "BillingTrx\Views\\tindakan\\";

    protected $model;
    protected $modelKunjungan;
    protected $modelKaryawan;

    protected $permission;

    public function __construct()
    {
        $this->model = new ModelsTindakan();
        $this->modelKunjungan = new ModelsKunjungan();
        $this->modelKaryawan = new KaryawanModel();
    }

    public function before_template()
    {
        $this->tag_datatables();
        $this->data['link_tag'][] = 'node_modules/select2/dist/css/select2.min.css';
        $this->data['link_tag'][]     = 'node_modules/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css';

        $this->data['script_tag'][] = 'node_modules/moment/min/moment-with-locales.min.js';
        $this->data['script_tag'][] = 'node_modules/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js';
        $this->data['script_tag'][] = 'node_modules/select2/dist/js/select2.min.js';
        $this->data['script_tag'][] = 'node_modules/jquery-validation/dist/jquery.validate.js';
        $this->data['script_tag'][] = 'js/datatables.js';
        $this->data['script_tag'][] = 'js/pages/BillingTrx/tindakan/submit.js';
        $this->data['script_tag'][] = 'js/pages/BillingTrx/tindakan/main.js';

        if (!isset($this->data['tempat_layanan_options']))
            $this->data['tempat_layanan_options']   = [];
        $this->data['tindakan_options']   = [];
        $this->data['dokter_dpjp_options']   = $this->modelKaryawan->dpjp_options();
        $this->data['tindakan_pelaksana_options']   = [];

        if (!isset($this->data['pasien']))
            $this->data['pasien'] = $this->modelKunjungan->pasien->find('000000');
    }

    public function index()
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');

        $this->data['urlForm'] = my_uri('');
        $this->template($this->pathTemplate . 'form');
    }

    public function form($uuid)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');

        $this->data['urlForm'] = my_uri('save');
        $this->form['tindakan_tanggal']['value'] = date("d M Y H:i");

        try {

            $data['kunjungan_tl'] = $this->modelKunjungan->kunjungan_tl->where('uuid', $uuid)->first();
            $data['kunjungan'] =  $this->modelKunjungan->kunjungan->find($data['kunjungan_tl']['id_kunjungan']);
            $data['penjamin'] =  $this->modelKunjungan->kunjungan_penjamin->where(['id_kunjungan' => $data['kunjungan_tl']['id_kunjungan']])->first();
            $data['pasien'] =  $this->modelKunjungan->pasien->find($data['kunjungan']['no_rm']);
            $this->data['pasien'] = $data['pasien'];

            #### isi options ####
            $this->data['tempat_layanan_options'] = $this->modelKunjungan->kunjungan_tl->tempat_layanan_options($data['kunjungan_tl']['id_kunjungan']);
            $this->form['tempat_layanan']['value'] = $uuid;
            $this->form['dokter_dpjp']['value'] = $this->modelKunjungan->KunjunganDokter->DokterDPJP($data['kunjungan_tl']['id_kunjungan']);
            $this->form['penjamin']['value'] = $data['penjamin']['penjamin'];

            ####
        } catch (\Throwable $th) {
            // return redirect()->route('kunjungan/tindakan');
            throw $th;
        }

        foreach ($data['pasien'] as $key => $value)
            if (in_array($key, $this->fields))
                $this->form[$key]['value'] = $value;

        // dd($data);
        $this->template($this->pathTemplate . 'form');
    }

    public function tindakan($uuid)
    {

        $kunjungan_tl = $this->modelKunjungan->kunjungan_tl->where('uuid', $uuid)->first();

        $db      = \Config\Database::connect('DBBillingMaster');
        $query = $db->query("SELECT d.uuid,c.* FROM _setting_tempat_layanan_tindakan a LEFT JOIN tindakan c ON a.id_tindakan=c.id  LEFT JOIN tindakan_kelompok d ON c.id_kelompok=d.id WHERE a.id_tempat_layanan='{$kunjungan_tl['id_tempat_layanan']}'");

        $data = [['id' => '', 'text' => '', 'uuid' => '',]];

        foreach ($query->getResult('array') as $value) {
            $data[] = [
                'id' => $value['id'],
                'text' => $value['nama'],
                'uuid' => $value['uuid'],
                'tarif' => $value['tarif_' . $kunjungan_tl['kelas']],
            ];
        }
        return $this->response->setJSON($data);
    }

    public function tindakan_pelaksana($uuid)
    {
        $tindakan_kelompok =  $this->model->masterTindakanKelompok->where(['uuid' => $uuid])->first();
        $tindakan_kelompok_formula = $this->model->masterTindakanKelompokFormula->where(['id_kelompok' => $tindakan_kelompok['id'], 'hidden' => 0])->findall();
        $data = [];

        foreach ($tindakan_kelompok_formula as $key => $value) {
            $data[] = $this->model->KaryawanTags->KaryawanByTag($value['id_pelaksana_layanan']);
        }

        return $this->response->setJSON($data);
    }

    public function Table()
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        if ($this->request->isAJAX()) {
            $datatables = new Datatables();
            $datatables->SetDB('DBBillingTrx');

            $datatables->from('kunjungan_tl_tindakan a')
                ->select("d.tempat_layanan,a.id,a.tindakan,a.tanggal,a.is_cito,a.harga, 
                c.qty,c.sub_total,c.nilai_jaminan,c.nilai_potongan
                ,GROUP_CONCAT(if(b.pelaksana=0,b.pelaksana_layanan,b.pelaksana)) pelaksana
                ,b.`type`
                ")
                ->join('kunjungan_tl_tindakan_pelaksana b', "a.id=b.id_kunjungan_tl_tindakan  AND b.`type` NOT IN ('Biaya Operasional','Jasa Tidak Langsung')", 'left')
                ->join('kunjungan_tl_tindakan_keuangan c', 'a.id=c.id_kunjungan_tl_tindakan', 'left')
                ->join('kunjungan_tl d', 'a.id_kunjungan_tl=d.id', 'left')
                // ->whereNotIn('b.`type`', ['Biaya Operasional','Jasa Tidak Langsung'])
                ->where('a.deleted_at', null)
                ->group_by('a.id');

            $datatables->att_column('DT_RowId', '$1', 'id')->order_by('d.tgl_mulai', 'desc')->order_by('a.tanggal', 'desc')->order_by('a.id', 'desc');

            $data['result'] = $datatables->generate();
            $json = json_decode($data['result'], true);
            $json['token'] = csrf_hash();
            return $this->response->setJSON($json);
        }
    }
}
