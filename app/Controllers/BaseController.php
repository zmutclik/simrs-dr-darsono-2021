<?php

namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;
use CodeIgniter\I18n\Time;
use Karyawan\Models\KaryawanModel;

class BaseController extends Controller
{

	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = ['html', 'form', 'auth', 'url', 'menus', 'my_uri', 'uuid'];

	protected $data = [];

	protected $form  = [];

	protected $fields = [];

	/**
	 * Constructor.
	 */
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// E.g.:
		// $this->session = \Config\Services::session();

		$karyawan = new KaryawanModel();
		$this->data['karyawan'] = $karyawan->find(user_id());

		$this->data['karyawan']['since'] = Time::createFromFormat('Y-m-d', $this->data['karyawan']['tgl_masuk'])->humanize();

		$this->data['menus'] 	= menus_build();
		$this->data['errors'] 	= [];

		$this->data['uri'] 		= my_uri();

		$this->data['link_tag'][] = 'node_modules/@fortawesome/fontawesome-free/css/all.min.css';
		$this->data['link_tag'][] = 'css/font-awesome-4.7.0/css/font-awesome.css';
		$this->data['link_tag'][] = 'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css';
		$this->data['link_tag'][] = 'node_modules/admin-lte/dist/css/adminlte.min.css';
		$this->data['link_tag'][] = 'node_modules/sweetalert2/dist/sweetalert2.min.css';
		$this->data['link_tag'][] = 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700';
		$this->data['link_tag'][] = 'css/superfish.css';
		$this->data['link_tag'][] = 'css/main.css';

		$this->data['script_tag'][] = 'node_modules/jquery/dist/jquery.min.js';
		$this->data['script_tag'][] = 'node_modules/popper.js/dist/umd/popper.min.js';
		$this->data['script_tag'][] = 'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js';
		$this->data['script_tag'][] = 'node_modules/admin-lte/dist/js/adminlte.min.js';
		$this->data['script_tag'][] = 'js/sweetalert.min.js';
		$this->data['script_tag'][] = 'js/loadingoverlay.min.js';
		$this->data['script_tag'][] = 'node_modules/superfish/dist/js/superfish.js';
		$this->data['script_tag'][] = 'js/superfish.click.js';
		$this->data['script_tag'][] = 'node_modules/moment/min/moment-with-locales.min.js';
		$this->data['script_tag'][] = 'node_modules/sweetalert2/dist/sweetalert2.all.min.js';
		$this->data['script_tag'][] = 'js/main.js';
	}

	public function before_template()
	{
	}

	protected function template($views)
	{
		$this->before_template();

		$this->data['form'] = $this->form;

		foreach ($this->fields as $item)
			if (!isset($this->data['errors'][$item]))
				$this->data['errors'][$item] = '';

		echo view($views, $this->data);
	}

	protected function tag_datatables()
	{

		$this->data['link_tag'][]   = 'node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css';
		$this->data['link_tag'][]   = 'node_modules/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css';
		$this->data['link_tag'][]   = 'node_modules/datatables.net-fixedcolumns-bs4/css/fixedColumns.bootstrap4.min.css';
		$this->data['link_tag'][]   = 'node_modules/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css';
		$this->data['link_tag'][]   = 'node_modules/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css';
		$this->data['link_tag'][]   = 'node_modules/datatables.net-rowgroup-bs4/css/rowGroup.bootstrap4.min.css';
		$this->data['link_tag'][]   = 'node_modules/datatables.net-scroller-bs4/css/scroller.bootstrap4.min.css';
		$this->data['link_tag'][]   = 'node_modules/datatables.net-select-bs4/css/select.bootstrap4.min.css';

		$this->data['script_tag'][] = 'node_modules/jszip/dist/jszip.min.js';
		$this->data['script_tag'][] = 'node_modules/pdfmake/build/pdfmake.min.js';
		$this->data['script_tag'][] = 'node_modules/pdfmake/build/vfs_fonts.js';
		$this->data['script_tag'][] = 'node_modules/datatables.net/js/jquery.dataTables.min.js';
		$this->data['script_tag'][] = 'node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js';
		$this->data['script_tag'][] = 'node_modules/datatables.net-buttons/js/dataTables.buttons.min.js';
		$this->data['script_tag'][] = 'js/buttons.bootstrap4.js';
		$this->data['script_tag'][] = 'node_modules/datatables.net-buttons/js/buttons.colVis.min.js';
		$this->data['script_tag'][] = 'node_modules/datatables.net-buttons/js/buttons.flash.js';
		$this->data['script_tag'][] = 'node_modules/datatables.net-buttons/js/buttons.html5.min.js';
		$this->data['script_tag'][] = 'node_modules/datatables.net-buttons/js/buttons.print.min.js';
		$this->data['script_tag'][] = 'node_modules/datatables.net-fixedcolumns/js/dataTables.fixedColumns.min.js';
		$this->data['script_tag'][] = 'node_modules/datatables.net-fixedheader-bs4/js/fixedHeader.bootstrap4.min.js';
		$this->data['script_tag'][] = 'node_modules/datatables.net-responsive/js/dataTables.responsive.min.js';
		$this->data['script_tag'][] = 'node_modules/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js';
		$this->data['script_tag'][] = 'node_modules/datatables.net-rowgroup-bs4/js/rowGroup.bootstrap4.min.js';
		$this->data['script_tag'][] = 'node_modules/datatables.net-scroller-bs4/js/scroller.bootstrap4.min.js';
		$this->data['script_tag'][] = 'node_modules/datatables.net-select-bs4/js/select.bootstrap4.min.js';
		$this->data['script_tag'][] = 'js/datatables.js';
	}


}
