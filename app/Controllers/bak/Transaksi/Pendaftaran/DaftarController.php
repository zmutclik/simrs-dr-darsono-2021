<?php

namespace App\Controllers\Transaksi\Pendaftaran;

use App\Controllers\BaseController;
use App\Libraries\Datatables;
use App\Models\Kunjungan\KunjModel;
use App\Models\Kunjungan\KunjRMModel;
use App\Models\Kunjungan\KunjKeuanganModel;
use App\Models\Kunjungan\PasienModel;

class DaftarController extends BaseController
{
    protected $form = [
        'form' => ['class' => 'needs-validation',],
        'no_rm' => [
            'name' => 'no_rm',
            'id' => 'no_rm',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'NO Rekam Medis',
            'required' => '',
            'maxlength' => '6',
        ],
        'telp' => [
            'id' => 'telp',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => '081',
            'readonly' => ''
        ],
        'nama' => [
            'id' => 'nama',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Nama Pasien',
            'readonly' => ''
        ],
        'alamat' => [
            'id' => 'alamat',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Alamat Lengkap',
            'readonly' => ''
        ],
        'tgl_mulai' => [
            'name' => 'tgl_mulai',
            'id' => 'tgl_mulai',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Tanggal Daftar',
            'required' => '',
        ],
        'masuk_asal' => [
            'name' => 'masuk_asal',
            'id' => 'masuk_asal',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'masuk_diag' => [
            'name' => 'masuk_diag',
            'id' => 'masuk_diag',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'kasus' => [
            'name' => 'kasus',
            'id' => 'kasus',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'id_tempat_layanan' => [
            'name' => 'id_tempat_layanan',
            'id' => 'id_tempat_layanan',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'id_tindakan' => [
            'name' => 'id_tindakan',
            'id' => 'id_tindakan',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'buttons' => ['back' => ['type' => 'button', 'class' => 'btn btn-danger hBack', 'content' => 'KEMBALI/BATAL',], 'submit' => ['type' => 'submit', 'class' => 'btn btn-danger  float-right', 'content' => 'SIMPAN',], 'delete' => ['type' => 'button', 'class' => 'btn btn-danger', 'content' => 'HAPUS',], 'edit' => ['type' => 'button', 'class' => 'btn btn-danger', 'content' => 'KOREKSI',],]
    ];

    protected $fields = [
        'no_rm', 'telp', 'nama', 'alamat', 'tgl_mulai', 'masuk_asal', 'masuk_diag', 'kasus', 'id_tempat_layanan', 'id_tindakan'
    ];

    protected $pathTemplate = "pages/transaksi/pendaftaran/daftar/";

    protected $model;
    protected $pasienModel;

    protected $permission;

    public function __construct()
    {
        $this->model = new KunjModel();
        $this->pasienModel = new PasienModel();
    }

    public function before_template()
    {
        $this->tag_datatables();
        $this->data['link_tag'][] = 'node_modules/select2/dist/css/select2.min.css';
        $this->data['link_tag'][]     = 'node_modules/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css';

        $this->data['script_tag'][] = 'node_modules/moment/min/moment-with-locales.min.js';
        $this->data['script_tag'][] = 'node_modules/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js';
        $this->data['script_tag'][] = 'node_modules/select2/dist/js/select2.min.js';
        $this->data['script_tag'][] = 'js/pages/transaksi/pendaftaran/Daftar.js';

        $model = new \App\Models\Libs\AsalMasukModel();
        $this->data['masuk_asal_options'] = $model->options();
        $model = new \App\Models\Libs\KasusModel();
        $this->data['kasus_options'] = $model->options();
        $this->data['masuk_diag_options'] = [];
        $model = new \App\Models\SettingTempatLayananModel();
        $this->data['id_tempat_layanan_options'] = $model->options();
        $model = new \App\Models\SettingTindakanModel();
        $this->data['id_tindakan_options'] = $model->options();
    }

    public function index()
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');

        $norm = $this->request->getGet('norm');

        $this->data['urlForm'] = my_uri('save?norm=' . $norm);
        $this->form['tgl_mulai']['value'] = date("d M Y h:i");

        if (!empty($norm)) {
            try {
                $model = new \App\Models\Kunjungan\PasienModel();
                foreach ($model->find($norm) as $key => $value)
                    if (in_array($key,$this->fields))
                        $this->form[$key]['value'] = $value;
            } catch (\Throwable $th) {
                //throw $th;
            }
        }
        // dd($this->form);
        $this->template($this->pathTemplate . 'form');
    }

    public function Save()
    {
        // if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');

        $this->data['urlForm'] = 'save';
        $this->form['form']['class'] = 'was-validated';
        $data = [];
        foreach ($this->fields as $value) $data[$value] = $this->request->getPost($value);

        dd($data);
        // if ($this->model->insert($data) === false) $this->data['errors'] = $this->model->errors();
        // else {
        //     $this->form['buttons']['submit']['class'] = 'invisible';
        // }

        // foreach ($this->data['errors'] as $key => $value)
        //     foreach ($this->form as $key_ => $item)
        //         if (!empty($this->form[$key_]['name']))
        //             if ($this->form[$key_]['name'] == $key) $this->form[$key_]['class'] .= ' is-invalid';

        // foreach ($data as $key => $value)
        //     $this->form[$key]['value'] = $data[$key];

        // $this->template($this->pathTemplate . 'form');
    }

    public function Table()
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        if ($this->request->isAJAX()) {
            $datatables = new Datatables();
            $datatables->SetDB('DBTrx');

            $datatables->from('kunjungan')
                ->select("id,no_rm,tgl_mulai")
                ->where('deleted_at', null)
                ->att_column('DT_RowId', '$1', 'id');

            $data['result'] = $datatables->generate();
            $json = json_decode($data['result'], true);
            $json['token'] = csrf_hash();
            return $this->response->setJSON($json);
        }
    }

    public function ICD()
    {
        $model = new \App\Models\Libs\ICDModel();
        $q =  $this->request->getGet('q');
        $arr = [];

        $arr = $model
            ->like("CONCAT(CODE,' ',CODE2,' ',STR)", "%$q%")
            ->where('SAB', 'ICD10_1998')
            ->orderBy('CODE2', 'asc')
            ->findAll(50);

        $data['results'] = [];
        foreach ($arr as $value) {
            $data['results'][] = ['id' => $value['CODE'], 'text' => '[ ' . $value['CODE'] . ' ] ' . $value['STR']];
        }

        return $this->response->setJSON($data);
    }
}
