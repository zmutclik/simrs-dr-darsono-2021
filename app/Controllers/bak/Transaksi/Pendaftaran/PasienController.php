<?php

namespace App\Controllers\Transaksi\Pendaftaran;

use App\Controllers\BaseController;
use App\Libraries\Datatables;
use App\Models\Kunjungan\PasienModel;
use App\Models\Libs\WilayahModel;
use CodeIgniter\I18n\Time;

class PasienController extends BaseController
{
    protected $form = [
        'form' => ['class' => 'needs-validation',],
        'no_rm' => [
            'name' => 'no_rm',
            'id' => 'no_rm',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Nomor Rekam Medis',
            'readonly' => '',
        ],
        'nik' => [
            'name' => 'nik',
            'id' => 'nik',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => '000000',
            'required' => '',
        ],
        'nama' => [
            'name' => 'nama',
            'id' => 'nama',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Nama Pasien',
            'required' => '',
        ],
        'jklL' => [
            'name'  => 'jkl',
            'id'    => 'jklL',
            'value' => 'L',
            'class' => 'form-check-input',
        ],
        'jklP' => [
            'name'  => 'jkl',
            'id'    => 'jklP',
            'value' => 'P',
            'class' => 'form-check-input',
        ],
        'lahir_tempat' => [
            'name' => 'lahir_tempat',
            'id' => 'lahir_tempat',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Kota Lahir',
            'required' => '',
        ],
        'lahir_tgl' => [
            'name' => 'lahir_tgl',
            'id' => 'lahir_tgl',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => '-',
            'required' => '',
        ],
        'alamat' => [
            'name' => 'alamat',
            'id' => 'alamat',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Nama Dusun / Nama Jalan',
            'required' => '',
        ],
        'rt' => [
            'name' => 'rt',
            'id' => 'rt',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'RT 00',
            'required' => '',
        ],
        'rw' => [
            'name' => 'rw',
            'id' => 'rw',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'RW 00',
            'required' => '',
        ],
        'provinsi' => [
            'name' => 'provinsi',
            'id' => 'provinsi',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Provinsi',
            'required' => '',
        ],
        'kabupaten' => [
            'name' => 'kabupaten',
            'id' => 'kabupaten',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'kecamatan' => [
            'name' => 'kecamatan',
            'id' => 'kecamatan',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'kelurahan' => [
            'name' => 'kelurahan',
            'id' => 'kelurahan',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'kode_wilayah' => [
            'type'  => 'hidden',
            'name' => 'kode_wilayah',
            'id' => 'kode_wilayah',
            'value' => null,
        ],
        'tlp' => [
            'name' => 'tlp',
            'id' => 'tlp',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => '081..',
            'required' => '',
        ],
        'buttons' => ['back' => ['type' => 'button', 'class' => 'btn btn-danger hBack', 'content' => 'KEMBALI/BATAL',], 'submit' => ['type' => 'submit', 'class' => 'btn btn-danger', 'content' => 'SIMPAN',], 'delete' => ['type' => 'button', 'class' => 'btn btn-danger', 'content' => 'HAPUS',], 'edit' => ['type' => 'button', 'class' => 'btn btn-danger', 'content' => 'KOREKSI',],]
    ];

    protected $fields = ['no_rm', 'nama', 'jkl', 'lahir_tempat', 'lahir_tgl', 'alamat', 'rt', 'rw', 'provinsi', 'kabupaten', 'kecamatan', 'kelurahan', 'kode_wilayah', 'tlp'];

    protected $pathTemplate = "pages/transaksi/pendaftaran/pasien/";

    protected $model;

    protected $permission;

    public function __construct()
    {
        $this->model = new PasienModel();
    }

    public function before_template()
    {
        $this->tag_datatables();

        $this->data['link_tag'][] = 'node_modules/select2/dist/css/select2.min.css';
        $this->data['link_tag'][]     = 'node_modules/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css';

        $this->data['script_tag'][] = 'node_modules/moment/min/moment-with-locales.min.js';
        $this->data['script_tag'][] = 'node_modules/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js';
        $this->data['script_tag'][] = 'js/pages/transaksi/Pasien.js';
        $this->data['script_tag'][] = 'node_modules/select2/dist/js/select2.min.js';

        if (!isset($this->data['provinsi_options']))
            $this->data['provinsi_options']   = [];
        if (!isset($this->data['kabupaten_options']))
            $this->data['kabupaten_options']  = [];
        if (!isset($this->data['kecamatan_options']))
            $this->data['kecamatan_options']  = [];
        if (!isset($this->data['kelurahan_options']))
            $this->data['kelurahan_options']  = [];
    }

    public function index()
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->template($this->pathTemplate . 'list');
    }

    public function Form($action = 'save', $id = null)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->data['urlForm'] = my_uri((isset($id)) ? $id . '/' . $action : $action);
        if (!empty($id)) {
            foreach ($this->model->find($id) as $key => $value) $this->form[$key]['value'] = $value;

            // $time = Time::createFromFormat('Y-m-j', $this->form['lahir_tgl']['value']);
            // $this->form['lahir_tgl']['value'] = $time->toLocalizedString('dd MMM yyyy'); 

            $this->form['jklL']['checked'] = $this->data['karyawan']['jkl'] == 'L';
            $this->form['jklP']['checked'] = $this->data['karyawan']['jkl'] == 'P';

            $wilayahModel                                       = new \App\Models\Libs\WilayahModel();
            if (!empty($this->form['provinsi']['value'])) {
                $dataWil                                            = $wilayahModel->find($this->form['provinsi']['value']);
                $this->data['provinsi_options'][$dataWil['kode']]   = $dataWil['nama'];
            }
            if (!empty($this->form['kabupaten']['value'])) {
                $dataWil                                            = $wilayahModel->find($this->form['kabupaten']['value']);
                $this->data['kabupaten_options'][$dataWil['kode']]  = $dataWil['nama'];
            }
            if (!empty($this->form['kecamatan']['value'])) {
                $dataWil                                            = $wilayahModel->find($this->form['kecamatan']['value']);
                $this->data['kecamatan_options'][$dataWil['kode']]  = $dataWil['nama'];
            }
            if (!empty($this->form['kelurahan']['value'])) {
                $dataWil                                            = $wilayahModel->find($this->form['kelurahan']['value']);
                $this->data['kelurahan_options'][$dataWil['kode']]  = $dataWil['nama'];
            }
        }
        $this->template($this->pathTemplate . 'form');
    }

    public function Save($action = 'save', $id = null)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->data['urlForm'] = my_uri((isset($id)) ? $id . '/' . $action : $action);
        $this->form['form']['class'] = 'was-validated';
        $data = [];
        foreach ($this->fields as $value) $data[$value] = $this->request->getPost($value);

        $time = Time::createFromFormat('j M Y', $data['lahir_tgl']);
        $data['lahir_tgl'] = $time->toDateString();

        if ($action == 'save') if ($this->model->insert($data) === false) $this->data['errors'] = $this->model->errors();
        else {
            $this->form['buttons']['submit']['class'] = 'invisible';
            $this->form['buttons']['back']['content'] = 'KEMBALI';
        }
        if ($action == 'edit') if ($this->model->update($id, $data) === false) $this->data['errors'] = $this->model->errors();
        else $this->form['buttons']['back']['content'] = 'KEMBALI';
        foreach ($this->data['errors'] as $key => $value) foreach ($this->form as $key_ => $item) if (!empty($this->form[$key_]['name'])) if ($this->form[$key_]['name'] == $key) $this->form[$key_]['class'] .= ' is-invalid';
        foreach ($data as $key => $value) $this->form[$key]['value'] = $data[$key];

        // $time = Time::createFromFormat('Y-m-j', $this->form['lahir_tgl']['value']);
        // $this->form['lahir_tgl']['value'] = $time->toLocalizedString('dd MMM yyyy'); 

        $this->form['jklL']['checked'] = $this->data['karyawan']['jkl'] == 'L';
        $this->form['jklP']['checked'] = $this->data['karyawan']['jkl'] == 'P';

        $wilayahModel                                       = new \App\Models\Libs\WilayahModel();
        $dataWil                                            = $wilayahModel->find($this->form['provinsi']['value']);
        $this->data['provinsi_options'][$dataWil['kode']]   = $dataWil['nama'];
        $dataWil                                            = $wilayahModel->find($this->form['kabupaten']['value']);
        $this->data['kabupaten_options'][$dataWil['kode']]  = $dataWil['nama'];
        $dataWil                                            = $wilayahModel->find($this->form['kecamatan']['value']);
        $this->data['kecamatan_options'][$dataWil['kode']]  = $dataWil['nama'];
        $dataWil                                            = $wilayahModel->find($this->form['kelurahan']['value']);
        $this->data['kelurahan_options'][$dataWil['kode']]  = $dataWil['nama'];

        $this->template($this->pathTemplate . 'form');
    }

    public function Delete($id)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        if ($this->request->isAJAX()) {
            $this->model->delete($id);
            $json['token'] = csrf_hash();
            return $this->response->setJSON($json);
        }
    }

    public function Show($id)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        foreach ($this->model->find($id) as $key => $value) $this->form[$key]['value'] = $value;
        $this->form['buttons']['back']['content'] = 'KEMBALI';
        $this->form['buttons']['edit']['onClick'] = "form_edit($id)";
        $this->form['buttons']['delete']['onClick'] = "form_delete($id,true)";
        $this->template($this->pathTemplate . 'show');
    }

    public function Table()
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        if ($this->request->isAJAX()) {
            $datatables = new Datatables();
            $datatables->SetDB('DBMaster');

            $datatables->from('pasien')
                ->select("no_rm AS id, no_rm,nik
                ,concat('[ ',no_rm,' ] ',nama) AS pasien
                ,concat(alamat,', RT.',rt,' RW.',rw,', ',kelurahan, ',Kec. ',kecamatan,',- ',kabupaten ) AS alamat_pasien
                ,tlp
                ,concat(lahir_tempat,', ',lahir_tgl) AS lahir
                ,date_format(now(), '%Y') - date_format(lahir_tgl, '%Y') - (date_format(now(), '00-%m-%d') < date_format(lahir_tgl, '00-%m-%d')) AS umur
                ")
                ->where('deleted_at', null);

            $searchfield = $this->request->getPost('searchnama');
            if (!empty($searchfield)) 
                $datatables->where_like('nama', $searchfield);

            $searchfield = $this->request->getPost('searchalamat');
            if (!empty($searchfield)) 
                $datatables->where_like("concat(alamat,', RT.',rt,' RW.',rw,', ',kelurahan, ',Kec. ',kecamatan,',- ',kabupaten )", $searchfield);

            $datatables->att_column('DT_RowId', '$1', 'id');

            $data['result'] = $datatables->generate();
            $json = json_decode($data['result'], true);
            $json['token'] = csrf_hash();
            return $this->response->setJSON($json);
        }
    }

    public function Wilayah()
    {
        $model = new WilayahModel();
        $q =  $this->request->getGet('q');
        $kode =  $this->request->getGet('kode');
        $parent =  $this->request->getGet('parent');
        $arr = [];
        if (($kode > 2 && !empty($parent)) || $kode == 2) {
            $arr = $model
                ->like('nama', "%$q%")
                ->like('kode', "$parent%")
                ->where('CHAR_LENGTH(kode)', $kode)
                ->orderBy('nama', 'asc')
                ->findAll();
        }

        $data['results'] = [];
        foreach ($arr as $value) {
            $data['results'][] = ['id' => $value['kode'], 'text' => $value['nama']];
        }

        return $this->response->setJSON($data);
    }
}
