<?php

namespace App\Controllers\Setting;

use App\Controllers\BaseController;
use App\Libraries\Datatables;
use App\Models\SettingMenusModel;

class MenusController extends BaseController
{
    protected $form = [
        'form' => ['class' => 'needs-validation'],
        'name' => [
            'name' => 'name',
            'id' => 'name',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Nama Menu',
            'required' => '',
        ],
        'parent' => [
            'name' => 'parent',
            'id' => 'parent',
            'value' => null,
            'class' => 'form-control',
        ],
        'link' => [
            'name' => 'link',
            'id' => 'link',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => '',
            'required' => '',
        ],
        'icon' => [
            'name' => 'icon',
            'id' => 'icon',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => '',
            'required' => '',
        ],
        'number' => [
            'name' => 'number',
            'id' => 'number',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => '',
            'required' => '',
        ],

        'buttons' => ['back' => ['type' => 'button', 'class' => 'btn btn-danger hBack', 'content' => 'KEMBALI/BATAL'], 'submit' => ['type' => 'submit', 'class' => 'btn btn-danger', 'content' => 'SIMPAN'], 'delete' => ['type' => 'button', 'class' => 'btn btn-danger', 'content' => 'HAPUS'], 'edit' => ['type' => 'button', 'class' => 'btn btn-danger', 'content' => 'KOREKSI']],
    ];

    protected $fields = ['parent', 'name', 'link', 'number'];

    protected $pathTemplate = "pages/settings/menus/";

    protected $model;

    protected $permission = "setting_menus";

    public function __construct()
    {
        $this->model = new SettingMenusModel();
    }

    public function before_template()
    {
        $this->tag_datatables();
        $this->data['script_tag'][] = 'js/pages/setting/menus.js';

        $parent_options = $this->model->findAll();
        $this->data['parent_options'][''] = '';
        foreach ($parent_options as $value)
            $this->data['parent_options'][$value['id']] = $value['name'];
    }

    public function index()
    {
        if (!empty($this->permission)) {
            if (!has_permission($this->permission)) {
                return redirect()->route('/');
            }
        }

        $this->template($this->pathTemplate . 'list');
    }

    public function Form($action = 'save', $id = null)
    {
        if (!empty($this->permission)) {
            if (!has_permission($this->permission)) {
                return redirect()->route('/');
            }
        }

        $this->data['urlForm'] = my_uri((isset($id)) ? $id . '/' . $action : $action);
        if (!empty($id)) {
            foreach ($this->model->find($id) as $key => $value) {
                $this->form[$key]['value'] = $value;
            }
        }
        $this->template($this->pathTemplate . 'form');
    }

    public function Save($action = 'save', $id = null)
    {
        if (!empty($this->permission)) {
            if (!has_permission($this->permission)) {
                return redirect()->route('/');
            }
        }

        $this->data['urlForm'] = my_uri((isset($id)) ? $id . '/' . $action : $action);
        $this->form['form']['class'] = 'was-validated';
        $data = [];
        foreach ($this->fields as $value) {
            $data[$value] = $this->request->getPost($value);
        }
        if (empty($data['parent']))
            $data['parent'] = null;

        if ($action == 'save') {
            if ($this->model->insert($data) === false) {
                $this->data['errors'] = $this->model->errors();
            } else {
                $this->form['buttons']['submit']['class'] = 'invisible';
                $this->form['buttons']['back']['content'] = 'KEMBALI';
            }
        }

        if ($action == 'edit') {
            if ($this->model->update($id, $data) === false) {
                $this->data['errors'] = $this->model->errors();
            } else {
                $this->form['buttons']['back']['content'] = 'KEMBALI';
            }
        }

        foreach ($this->data['errors'] as $key => $value) {
            foreach ($this->form as $key_ => $item) {
                if (!empty($this->form[$key_]['name'])) {
                    if ($this->form[$key_]['name'] == $key) {
                        $this->form[$key_]['class'] .= ' is-invalid';
                    }
                }
            }
        }

        foreach ($data as $key => $value) {
            $this->form[$key]['value'] = $data[$key];
        }

        $this->template($this->pathTemplate . 'form');
    }

    public function Delete($id)
    {
        if (!empty($this->permission)) {
            if (!has_permission($this->permission)) {
                return redirect()->route('/');
            }
        }

        if ($this->request->isAJAX()) {
            $this->model->delete($id);
            $json['token'] = csrf_hash();
            return $this->response->setJSON($json);
        }
    }

    public function Show($id)
    {
        if (!empty($this->permission)) {
            if (!has_permission($this->permission)) {
                return redirect()->route('/');
            }
        }

        foreach ($this->model->find($id) as $key => $value) {
            $this->form[$key]['value'] = $value;
        }

        if (!empty($this->form['parent']['value'])) {
            $arr = $this->model->find($this->form['parent']['value']);
            $this->form['parent']['value'] = $arr['name'];
        }

        $this->form['buttons']['back']['content'] = 'KEMBALI';
        $this->form['buttons']['edit']['onClick'] = "form_edit($id)";
        $this->form['buttons']['delete']['onClick'] = "form_delete($id,true)";
        $this->template($this->pathTemplate . 'show');
    }

    public function Table()
    {
        if (!empty($this->permission)) {
            if (!has_permission($this->permission)) {
                return redirect()->route('/');
            }
        }

        if ($this->request->isAJAX()) {
            $datatables = new Datatables();
            $datatables->from('menus a')
                ->select("a.id as id, CONCAT(b.name) as parent_,a.parent as parent, a.name as name, a.link as link, a.icon as icon, a.slug as slug, a.number as number")
                ->join("menus b", "a.parent=b.id", "left")
                ->where('a.deleted_at',null)
                ->att_column('DT_RowId', '$1', 'id');
            $data['result'] = $datatables->generate();
            $json = json_decode($data['result'], true);
            $json['token'] = csrf_hash();
            return $this->response->setJSON($json);
        }
    }
}
