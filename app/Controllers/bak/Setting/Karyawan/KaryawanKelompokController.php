<?php

namespace App\Controllers\Setting\Karyawan;

use App\Controllers\BaseController;
use App\Libraries\Datatables;
use App\Models\KaryawanKelompokModel;

class KaryawanKelompokController extends BaseController
{
    protected $form = [
        'form' => ['class' => 'needs-validation',],
        'nama' => [
            'name' => 'nama',
            'id' => 'nama',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Nama Kelokpok',
            'required' => '',
        ],
        'is_khusus' => [
            'name' => 'is_khusus',
            'id' => 'is_khusus',
            'value'   => null,
            'class' => 'form-check-input',
        ],

        'buttons' => ['back' => ['type' => 'button', 'class' => 'btn btn-danger hBack', 'content' => 'KEMBALI/BATAL',], 'submit' => ['type' => 'submit', 'class' => 'btn btn-danger', 'content' => 'SIMPAN',], 'delete' => ['type' => 'button', 'class' => 'btn btn-danger', 'content' => 'HAPUS',], 'edit' => ['type' => 'button', 'class' => 'btn btn-danger', 'content' => 'KOREKSI',],]
    ];

    protected $fields = ['nama', 'is_khusus'];

    protected $pathTemplate = "pages/settings/karyawankelompok/";

    protected $model;

    protected $permission;

    public function __construct()
    {
        $this->model = new KaryawanKelompokModel();
    }

    public function before_template()
    {
        $this->tag_datatables();
        $this->data['script_tag'][] = 'js/pages/setting/KaryawanKelompok.js';
    }

    public function index()
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->template($this->pathTemplate . 'list');
    }

    public function Form($action = 'save', $id = null)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->data['urlForm'] = my_uri((isset($id)) ? $id . '/' . $action : $action);
        if (!empty($id)) {
            foreach ($this->model->find($id) as $key => $value) $this->form[$key]['value'] = $value;
        }
        if ($this->form['is_khusus']['value'] == 0)
            unset($this->form['is_khusus']['value']);
        $this->template($this->pathTemplate . 'form');
    }

    public function Save($action = 'save', $id = null)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->data['urlForm'] = my_uri((isset($id)) ? $id . '/' . $action : $action);
        $this->form['form']['class'] = 'was-validated';
        $data = [];
        foreach ($this->fields as $value) $data[$value] = $this->request->getPost($value);
        if (empty($data['is_khusus'])) $data['is_khusus'] = 0;
        // dd($data);
        if ($action == 'save') if ($this->model->insert($data) === false) $this->data['errors'] = $this->model->errors();
        else {
            $this->form['buttons']['submit']['class'] = 'invisible';
            $this->form['buttons']['back']['content'] = 'KEMBALI';
        }
        if ($action == 'edit') if ($this->model->update($id, $data) === false) $this->data['errors'] = $this->model->errors();
        else $this->form['buttons']['back']['content'] = 'KEMBALI';
        foreach ($this->data['errors'] as $key => $value) foreach ($this->form as $key_ => $item) if (!empty($this->form[$key_]['name'])) if ($this->form[$key_]['name'] == $key) $this->form[$key_]['class'] .= ' is-invalid';
        foreach ($data as $key => $value) $this->form[$key]['value'] = $data[$key];
        if ($this->form['is_khusus']['value'] == 0)
            unset($this->form['is_khusus']['value']);
        $this->template($this->pathTemplate . 'form');
    }

    public function Delete($id)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        if ($this->request->isAJAX()) {
            $this->model->delete($id);
            $json['token'] = csrf_hash();
            return $this->response->setJSON($json);
        }
    }

    public function Show($id)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        foreach ($this->model->find($id) as $key => $value) $this->form[$key]['value'] = $value;
        $this->form['buttons']['back']['content'] = 'KEMBALI';
        $this->form['buttons']['edit']['onClick'] = "form_edit($id)";
        $this->form['buttons']['delete']['onClick'] = "form_delete($id,true)";
        $this->template($this->pathTemplate . 'show');
    }

    public function Table()
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        if ($this->request->isAJAX()) {
            $datatables = new Datatables();
            $datatables->SetDB('DBMaster');
            $datatables
                ->from('karyawan_kelompok')
                ->select("id,nama,if(is_khusus=1,'Kelompok Khusus','-') AS is_khusus")
                ->att_column('DT_RowId', '$1', 'id');
            $data['result'] = $datatables->generate();
            $json = json_decode($data['result'], true);
            $json['token'] = csrf_hash();
            return $this->response->setJSON($json);
        }
    }
}
