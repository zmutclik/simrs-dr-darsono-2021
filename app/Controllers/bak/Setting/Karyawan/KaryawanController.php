<?php

namespace App\Controllers\Setting\Karyawan;

use App\Controllers\BaseController;
use App\Libraries\Datatables;
use App\Models\KaryawanModel;
use CodeIgniter\I18n\Time;

class KaryawanController extends BaseController
{
    protected $form = [
        'form' => ['class' => 'needs-validation',],
        'username' => [
            'name' => 'username',
            'id' => 'username',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'UserName',
            'required' => '',
        ],
        'email' => [
            'name' => 'email',
            'id' => 'email',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'email@example.com',
            'required' => '',
        ],
        'nama' => [
            'name' => 'nama',
            'id' => 'nama',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Nama Lengkap',
            'required' => '',
        ],
        'alamat' => [
            'name' => 'alamat',
            'id' => 'alamat',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Alamat',
            'required' => '',
        ],
        'jklL' => [
            'name'  => 'jkl',
            'id'    => 'jklL',
            'value' => 'L',
            'class' => 'form-check-input',
        ],
        'jklP' => [
            'name'  => 'jkl',
            'id'    => 'jklP',
            'value' => 'P',
            'class' => 'form-check-input',
        ],
        'jabatan' => [
            'name' => 'jabatan',
            'id' => 'jabatan',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Nama Jabatan',
            'required' => '',
        ],
        'tgl_masuk' => [
            'name' => 'tgl_masuk',
            'id' => 'tgl_masuk',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => '__-__-____',
            'required' => '',
        ],
        'buttons' => ['back' => ['type' => 'button', 'class' => 'btn btn-danger hBack', 'content' => 'KEMBALI/BATAL',], 'submit' => ['type' => 'submit', 'class' => 'btn btn-danger', 'content' => 'SIMPAN',], 'delete' => ['type' => 'button', 'class' => 'btn btn-danger', 'content' => 'HAPUS',], 'edit' => ['type' => 'button', 'class' => 'btn btn-danger', 'content' => 'KOREKSI',],]
    ];

    protected $fields = ['nama', 'alamat', 'email', 'jkl', 'jabatan', 'tgl_masuk'];

    protected $pathTemplate = "pages/settings/karyawan/";

    protected $model;

    protected $permission;

    public function __construct()
    {
        $this->model = new KaryawanModel();
    }

    public function before_template()
    {
        $this->tag_datatables();
		$this->data['link_tag'][] 	= 'node_modules/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css';

		$this->data['script_tag'][] = 'node_modules/moment/min/moment-with-locales.min.js';
		$this->data['script_tag'][] = 'node_modules/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js';
        $this->data['script_tag'][] = 'js/pages/setting/Karyawan.js';
    }

    public function index()
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->template($this->pathTemplate . 'list');
    }

    public function Form($action = 'save', $id = null)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->data['urlForm'] = my_uri((isset($id)) ? $id . '/' . $action : $action);
        if (!empty($id)) {
            foreach ($this->model->find($id) as $key => $value) $this->form[$key]['value'] = $value;
        }

		$this->form['tgl_masuk']['value'] = Time::createFromFormat('Y-m-d', $this->form['tgl_masuk']['value'])->toLocalizedString('d-MM-Y');
		$this->form['jklL']['checked'] = $this->data['karyawan']['jkl'] == 'L';
        $this->form['jklP']['checked'] = $this->data['karyawan']['jkl'] == 'P';
        
        $this->template($this->pathTemplate . 'form');
    }

    public function Save($action = 'save', $id = null)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->data['urlForm'] = my_uri((isset($id)) ? $id . '/' . $action : $action);
        $this->form['form']['class'] = 'was-validated';
        $data = [];
        foreach ($this->fields as $value) $data[$value] = $this->request->getPost($value);

        $data['tgl_masuk'] = Time::createFromFormat('j-m-Y', $data['tgl_masuk'])->toDateString();
        
        if ($action == 'save') if ($this->model->insert($data) === false) $this->data['errors'] = $this->model->errors();
        else {
            $this->form['buttons']['submit']['class'] = 'invisible';
            $this->form['buttons']['back']['content'] = 'KEMBALI';
        }
        if ($action == 'edit') if ($this->model->update($id, $data) === false) $this->data['errors'] = $this->model->errors();
        else $this->form['buttons']['back']['content'] = 'KEMBALI';
        foreach ($this->data['errors'] as $key => $value) foreach ($this->form as $key_ => $item) if (!empty($this->form[$key_]['name'])) if ($this->form[$key_]['name'] == $key) $this->form[$key_]['class'] .= ' is-invalid';
        foreach ($data as $key => $value) $this->form[$key]['value'] = $data[$key];

		$this->form['tgl_masuk']['value'] = Time::createFromFormat('Y-m-d', $data['tgl_masuk'])->toLocalizedString('d-MM-Y');
		$this->form['jklL']['checked'] = $data['jkl'] == 'L';
        $this->form['jklP']['checked'] = $data['jkl'] == 'P';
        
        $this->template($this->pathTemplate . 'form');
    }

    public function Delete($id)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        if ($this->request->isAJAX()) {
            $this->model->delete($id);
            $json['token'] = csrf_hash();
            return $this->response->setJSON($json);
        }
    }

    public function Show($id)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        foreach ($this->model->find($id) as $key => $value) $this->form[$key]['value'] = $value;
        $this->form['buttons']['back']['content'] = 'KEMBALI';
        $this->form['buttons']['edit']['onClick'] = "form_edit($id)";
        $this->form['buttons']['delete']['onClick'] = "form_delete($id,true)";
        $this->template($this->pathTemplate . 'show');
    }

    public function Table()
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        if ($this->request->isAJAX()) {
            $datatables = new Datatables();
            $datatables->SetDB('DBMaster');
            $datatables
                ->from('karyawan')
                ->select("id,username,email,nama,alamat,jabatan,tgl_masuk")
                ->att_column('DT_RowId', '$1', 'id');
            $data['result'] = $datatables->generate();
            $json = json_decode($data['result'], true);
            $json['token'] = csrf_hash();
            return $this->response->setJSON($json);
        }
    }
}
