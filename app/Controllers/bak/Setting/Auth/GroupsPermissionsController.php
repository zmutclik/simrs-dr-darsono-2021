<?php

namespace App\Controllers\Setting\Auth;

use App\Controllers\BaseController;
use App\Libraries\Datatables;
use App\Models\AuthGroupsPermissionsModel;
use App\Models\AuthGroupsModel;
use App\Models\AuthPermissionsModel;

class GroupsPermissionsController extends BaseController
{
    protected $form = [
        'form' => ['class' => 'needs-validation',],
        'group_id' => [
            'name' => 'group_id',
            'id' => 'group_id',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'permission_id' => [
            'name' => 'permission_id',
            'id' => 'permission_id',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'buttons' => ['back' => ['type' => 'button', 'class' => 'btn btn-danger hBack', 'content' => 'KEMBALI/BATAL',], 'submit' => ['type' => 'submit', 'class' => 'btn btn-danger', 'content' => 'SIMPAN',], 'delete' => ['type' => 'button', 'class' => 'btn btn-danger', 'content' => 'HAPUS',], 'edit' => ['type' => 'button', 'class' => 'btn btn-danger', 'content' => 'KOREKSI',],]
    ];

    protected $fields = ['group_id', 'permission_id'];

    protected $pathTemplate = "pages/settings/auth/groupspermissions/";

    protected $model;

    protected $permission = 'auth_groups_permissions';

    public function __construct()
    {
        $this->model = new AuthGroupsPermissionsModel();
    }

    public function before_template()
    {
        $this->tag_datatables();
        $this->data['script_tag'][] = 'js/pages/auth/GroupsPermissions.js';

        $model = new AuthGroupsModel();
        $options = $model->findAll();
        $this->data['group_id_options'][''] = '';
        foreach ($options as $value)
            $this->data['group_id_options'][$value['id']] = $value['name'];

        $model = new AuthPermissionsModel();
        $options = $model->findAll();
        $this->data['permission_id_options'][''] = '';
        foreach ($options as $value)
            $this->data['permission_id_options'][$value['id']] = $value['name'];
    }

    public function index()
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->template($this->pathTemplate . 'list');
    }

    public function Form($action = 'save', $id = null)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->data['urlForm'] = my_uri((isset($id)) ? $id . '/' . $action : $action);
        if (!empty($id)) {
            foreach ($this->model->find($id) as $key => $value) $this->form[$key]['value'] = $value;
        }
        $this->template($this->pathTemplate . 'form');
    }

    public function Save($action = 'save', $id = null)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->data['urlForm'] = my_uri((isset($id)) ? $id . '/' . $action : $action);
        $this->form['form']['class'] = 'was-validated';
        $data = [];
        foreach ($this->fields as $value) $data[$value] = $this->request->getPost($value);
        if ($action == 'save') if ($this->model->insert($data) === false) $this->data['errors'] = $this->model->errors();
        else {
            $this->form['buttons']['submit']['class'] = 'invisible';
            $this->form['buttons']['back']['content'] = 'KEMBALI';
        }
        if ($action == 'edit') if ($this->model->update($id, $data) === false) $this->data['errors'] = $this->model->errors();
        else $this->form['buttons']['back']['content'] = 'KEMBALI';
        foreach ($this->data['errors'] as $key => $value) foreach ($this->form as $key_ => $item) if (!empty($this->form[$key_]['name'])) if ($this->form[$key_]['name'] == $key) $this->form[$key_]['class'] .= ' is-invalid';
        foreach ($data as $key => $value) $this->form[$key]['value'] = $data[$key];
        $this->template($this->pathTemplate . 'form');
    }

    public function Delete($id)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        if ($this->request->isAJAX()) {
            $this->model->delete($id);
            $json['token'] = csrf_hash();
            return $this->response->setJSON($json);
        }
    }

    public function Show($id)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        foreach ($this->model->find($id) as $key => $value) $this->form[$key]['value'] = $value;
        $this->form['buttons']['back']['content'] = 'KEMBALI';
        $this->form['buttons']['edit']['onClick'] = "form_edit($id)";
        $this->form['buttons']['delete']['onClick'] = "form_delete($id,true)";
        $this->template($this->pathTemplate . 'show');
    }

    public function Table()
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        if ($this->request->isAJAX()) {
            $datatables = new Datatables();
            $datatables
                ->from('auth_groups_permissions a')
                ->select("a.id as id,a.group_id as group_id,b.name as group,a.permission_id as permission_id,c.name as permission")
                ->join('auth_groups b', 'a.group_id=b.id', 'left')
                ->join('auth_permissions c', 'a.permission_id=c.id', 'left')
                ->att_column('DT_RowId', '$1', 'id');
            $data['result'] = $datatables->generate();
            $json = json_decode($data['result'], true);
            $json['token'] = csrf_hash();
            return $this->response->setJSON($json);
        }
    }
}
