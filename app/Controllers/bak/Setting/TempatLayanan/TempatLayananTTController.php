<?php

namespace App\Controllers\Setting\TempatLayanan;

use App\Controllers\BaseController;
use App\Libraries\Datatables;
use App\Models\SettingTempatLayananModel;
use App\Models\SettingTempatLayananTTModel;

class TempatLayananTTController extends BaseController
{
    protected $form = [
        'form' => ['class' => 'needs-validation',],
        'id_tl' => [
            'name' => 'id_tl',
            'id' => 'id_tl',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'nama' => [
            'name' => 'nama',
            'id' => 'nama',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Nama Kamar',
            'required' => '',
        ],
        'kelas' => [
            'name' => 'kelas',
            'id' => 'kelas',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'is_tt_bayangan' => [
            'name' => 'is_tt_bayangan',
            'id' => 'is_tt_bayangan',
            'value'   => null,
            'class' => 'form-check-input',
        ],
        'buttons' => ['back' => ['type' => 'button', 'class' => 'btn btn-danger hBack', 'content' => 'KEMBALI/BATAL',], 'submit' => ['type' => 'submit', 'class' => 'btn btn-danger', 'content' => 'SIMPAN',], 'delete' => ['type' => 'button', 'class' => 'btn btn-danger', 'content' => 'HAPUS',], 'edit' => ['type' => 'button', 'class' => 'btn btn-danger', 'content' => 'KOREKSI',],]
    ];

    protected $fields = ['id_tl', 'nama', 'kelas', 'is_tt_bayangan'];

    protected $pathTemplate = "pages/settings/tempatlayanantt/";

    protected $model;

    protected $permission;

    public function __construct()
    {
        $this->model = new SettingTempatLayananTTModel();
    }

    public function before_template()
    {
        $this->tag_datatables();
        $this->data['script_tag'][] = 'js/pages/setting/TempatLayananTT.js';

        $model = new SettingTempatLayananModel();
        $options = $model->findAll();
        $this->data['id_tl_options'][''] = '';
        foreach ($options as $value)
            $this->data['id_tl_options'][$value['id']] = $value['nama'];

        $this->data['kelas_options'] = ['3' => '3', '2' => '2', '1' => '1', 'VIP' => 'VIP'];
    }

    public function index()
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->template($this->pathTemplate . 'list');
    }

    public function Form($action = 'save', $id = null)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->data['urlForm'] = my_uri((isset($id)) ? $id . '/' . $action : $action);
        if (!empty($id)) {
            foreach ($this->model->find($id) as $key => $value) $this->form[$key]['value'] = $value;
        }
        if ($this->form['is_tt_bayangan']['value'] == 0)
            unset($this->form['is_tt_bayangan']['value']);
        $this->template($this->pathTemplate . 'form');
    }

    public function Save($action = 'save', $id = null)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->data['urlForm'] = my_uri((isset($id)) ? $id . '/' . $action : $action);
        $this->form['form']['class'] = 'was-validated';
        $data = [];
        foreach ($this->fields as $value) $data[$value] = $this->request->getPost($value);
        if (empty($data['is_tt_bayangan'])) $data['is_tt_bayangan'] = 0;
        if ($action == 'save') if ($this->model->insert($data) === false) $this->data['errors'] = $this->model->errors();
        else {
            $this->form['buttons']['submit']['class'] = 'invisible';
            $this->form['buttons']['back']['content'] = 'KEMBALI';
        }
        if ($action == 'edit') if ($this->model->update($id, $data) === false) $this->data['errors'] = $this->model->errors();
        else $this->form['buttons']['back']['content'] = 'KEMBALI';
        foreach ($this->data['errors'] as $key => $value) foreach ($this->form as $key_ => $item) if (!empty($this->form[$key_]['name'])) if ($this->form[$key_]['name'] == $key) $this->form[$key_]['class'] .= ' is-invalid';
        foreach ($data as $key => $value) $this->form[$key]['value'] = $data[$key];
        if ($this->form['is_tt_bayangan']['value'] == 0)
            unset($this->form['is_tt_bayangan']['value']);
        $this->template($this->pathTemplate . 'form');
    }

    public function Delete($id)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        if ($this->request->isAJAX()) {
            $this->model->delete($id);
            $json['token'] = csrf_hash();
            return $this->response->setJSON($json);
        }
    }

    public function Show($id)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        foreach ($this->model->find($id) as $key => $value) $this->form[$key]['value'] = $value;
        $this->form['buttons']['back']['content'] = 'KEMBALI';
        $this->form['buttons']['edit']['onClick'] = "form_edit($id)";
        $this->form['buttons']['delete']['onClick'] = "form_delete($id,true)";
        $this->template($this->pathTemplate . 'show');
    }

    public function Table()
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        if ($this->request->isAJAX()) {
            $datatables = new Datatables();
            $datatables->SetDB('DBMaster');
            $datatables
            ->from('tempat_layanan_tt a')
            ->select("a.id AS id,b.nama AS tempat_layanan,a.nama AS nama,a.kelas AS kelas,if(is_tt_bayangan=1,'TT Bayangan','-') AS is_tt_bayangan")
            ->join('tempat_layanan b','a.id_tl=b.id','left')
            ->where('a.deleted_at',null)
            ->att_column('DT_RowId', '$1', 'id');

            $data['result'] = $datatables->generate();
            $json = json_decode($data['result'], true);
            $json['token'] = csrf_hash();
            return $this->response->setJSON($json);
        }
    }
}
