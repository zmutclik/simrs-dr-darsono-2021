<?php

namespace App\Controllers\Setting\Tindakan;

use App\Controllers\BaseController;
use App\Libraries\Datatables;
use App\Models\SettingTindakanKelompokFormulaModel;
use App\Models\SettingTindakanModel;

class TindakanKelompokFormulaController extends BaseController
{
    protected $form = [
        'form' => ['class' => 'needs-validation',],
        'id_kelompok' => [
            'name' => 'id_kelompok',
            'id' => 'id_kelompok',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'nama' => [
            'name' => 'nama',
            'id' => 'nama',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Nama Pelaksana',
            'required' => '',
        ],
        'type' => [
            'name' => 'type',
            'id' => 'type',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'formula' => [
            'name' => 'formula',
            'id' => 'formula',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'TARIF*1',
            'required' => '',
        ],
        'hidden' => [
            'type' => 'checkbox',
            'name' => 'hidden',
            'id' => 'hidden',
            'value'   => null,
            'class' => 'form-check-input',
        ],
        'buttons' => ['back' => ['type' => 'button', 'class' => 'btn btn-danger hBack', 'content' => 'KEMBALI/BATAL',], 'submit' => ['type' => 'submit', 'class' => 'btn btn-danger', 'content' => 'SIMPAN',], 'delete' => ['type' => 'button', 'class' => 'btn btn-danger', 'content' => 'HAPUS',], 'edit' => ['type' => 'button', 'class' => 'btn btn-danger', 'content' => 'KOREKSI',],]
    ];

    protected $fields = ['id_kelompok', 'nama', 'type', 'formula', 'hidden'];

    protected $pathTemplate = "pages/settings/tindakankelompokformula/";

    protected $model;

    protected $permission;

    public function __construct()
    {
        $this->model = new SettingTindakanKelompokFormulaModel();
    }

    public function before_template()
    {
        $this->tag_datatables();
        $this->data['script_tag'][] = 'js/pages/setting/TindakanKelompokFormula.js';

        $model = new SettingTindakanModel();
        $this->data['id_kelompok_options'] = $model->id_kelompok();
        $this->data['type_options'] = $this->model->type_options;
    }

    public function index()
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->template($this->pathTemplate . 'list');
    }

    public function Form($action = 'save', $id = null)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->data['urlForm'] = my_uri((isset($id)) ? $id . '/' . $action : $action);
        if (!empty($id)) {
            foreach ($this->model->find($id) as $key => $value) {
                $this->form[$key]['value'] = $value;
                if (isset($this->form[$value]['type']))
                    if ($this->form[$key]['type'] == 'checkbox' && $this->form[$key]['value'] == 0)
                        unset($this->form[$key]['value']);
            }
        }
        $this->template($this->pathTemplate . 'form');
    }

    public function Save($action = 'save', $id = null)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->data['urlForm'] = my_uri((isset($id)) ? $id . '/' . $action : $action);
        $this->form['form']['class'] = 'was-validated';
        $data = [];
        foreach ($this->fields as $value) {
            $data[$value] = $this->request->getPost($value);
            if (isset($this->form[$value]['type']))
                if ($this->form[$value]['type'] == 'checkbox')
                    if (empty($data[$value])) $data[$value] = 0;
        }
        if ($action == 'save') if ($this->model->insert($data) === false) $this->data['errors'] = $this->model->errors();
        else {
            $this->form['buttons']['submit']['class'] = 'invisible';
            $this->form['buttons']['back']['content'] = 'KEMBALI';
        }
        if ($action == 'edit') if ($this->model->update($id, $data) === false) $this->data['errors'] = $this->model->errors();
        else $this->form['buttons']['back']['content'] = 'KEMBALI';
        foreach ($this->data['errors'] as $key => $value) foreach ($this->form as $key_ => $item) if (!empty($this->form[$key_]['name'])) if ($this->form[$key_]['name'] == $key) $this->form[$key_]['class'] .= ' is-invalid';
        foreach ($data as $key => $value) {
            $this->form[$key]['value'] = $data[$key];
            if (isset($this->form[$value]['type']))
                if ($this->form[$key]['type'] == 'checkbox' && $this->form[$key]['value'] == 0)
                    unset($this->form[$key]['value']);
        }
        $this->template($this->pathTemplate . 'form');
    }

    public function Delete($id)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        if ($this->request->isAJAX()) {
            $this->model->delete($id);
            $json['token'] = csrf_hash();
            return $this->response->setJSON($json);
        }
    }

    public function Show($id)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        foreach ($this->model->find($id) as $key => $value) $this->form[$key]['value'] = $value;
        $this->form['buttons']['back']['content'] = 'KEMBALI';
        $this->form['buttons']['edit']['onClick'] = "form_edit($id)";
        $this->form['buttons']['delete']['onClick'] = "form_delete($id,true)";
        $this->template($this->pathTemplate . 'show');
    }

    public function Table()
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        if ($this->request->isAJAX()) {
            $datatables = new Datatables();
            $datatables->SetDB('DBMaster');

            $datatables->from('tindakan_kelompok_pelaksana a')
                ->select("
                 a.id AS id
                ,b.nama AS kelompok
                ,a.nama AS nama
                ,a.type AS type
                ,formula , formula_prosen
                ")
                ->join('tindakan_kelompok b', 'a.id_kelompok=b.id', 'left')
                ->where('a.deleted_at', null)
                ->att_column('DT_RowId', '$1', 'id');

            $data['result'] = $datatables->generate();
            $json = json_decode($data['result'], true);
            $json['token'] = csrf_hash();
            return $this->response->setJSON($json);
        }
    }
}
