<?php

namespace App\Controllers\Setting\Tindakan;

use App\Controllers\BaseController;
use App\Libraries\Datatables;
use App\Models\SettingTindakanModel;

class TindakanController extends BaseController
{
    protected $form = [
        'form' => ['class' => 'needs-validation',],
        'kode_panggil' => [
            'name' => 'kode_panggil',
            'id' => 'kode_panggil',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'ABC123',
            'required' => '',
        ],
        'id_kelompok' => [
            'name' => 'id_kelompok',
            'id' => 'id_kelompok',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'nama' => [
            'name' => 'nama',
            'id' => 'nama',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => 'Tindakan',
            'required' => '',
        ],
        'type_kelas' => [
            'name' => 'type_kelas',
            'id' => 'type_kelas',
            'value' => null,
            'class' => 'form-control',
            'required' => '',
        ],
        'tarif_3' => [
            'name' => 'tarif_3',
            'id' => 'tarif_3',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => '0',
            'required' => '',
        ],
        'tarif_2' => [
            'name' => 'tarif_2',
            'id' => 'tarif_2',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => '0',
            'required' => '',
        ],
        'tarif_1' => [
            'name' => 'tarif_1',
            'id' => 'tarif_1',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => '0',
            'required' => '',
        ],
        'tarif_vip' => [
            'name' => 'tarif_vip',
            'id' => 'tarif_vip',
            'value' => null,
            'class' => 'form-control',
            'placeholder' => '0',
            'required' => '',
        ],

        'buttons' => ['back' => ['type' => 'button', 'class' => 'btn btn-danger hBack', 'content' => 'KEMBALI/BATAL',], 'submit' => ['type' => 'submit', 'class' => 'btn btn-danger', 'content' => 'SIMPAN',], 'delete' => ['type' => 'button', 'class' => 'btn btn-danger', 'content' => 'HAPUS',], 'edit' => ['type' => 'button', 'class' => 'btn btn-danger', 'content' => 'KOREKSI',],]
    ];

    protected $fields = ['kode_panggil','id_kelompok','nama','type_kelas','tarif_3','tarif_2','tarif_1','tarif_vip'];

    protected $pathTemplate = "pages/settings/tindakan/";

    protected $model;

    protected $permission;

    public function __construct()
    {
        $this->model = new SettingTindakanModel();
    }

    public function before_template()
    {
        $this->tag_datatables();
        $this->data['script_tag'][] = 'js/pages/setting/TindakanController.js';

        $this->data['id_kelompok_options'] = $this->model->id_kelompok();
        $this->data['type_kelas_options'] = $this->model->type_kelas;
    }

    public function index()
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->template($this->pathTemplate . 'list');
    }

    public function Form($action = 'save', $id = null)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->data['urlForm'] = my_uri((isset($id)) ? $id . '/' . $action : $action);
        if (!empty($id)) {
            foreach ($this->model->find($id) as $key => $value) $this->form[$key]['value'] = $value;
        }
        $this->template($this->pathTemplate . 'form');
    }

    public function Save($action = 'save', $id = null)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        $this->data['urlForm'] = my_uri((isset($id)) ? $id . '/' . $action : $action);
        $this->form['form']['class'] = 'was-validated';
        $data = [];
        foreach ($this->fields as $value) $data[$value] = $this->request->getPost($value);
        if ($action == 'save') if ($this->model->insert($data) === false) $this->data['errors'] = $this->model->errors();
        else {
            $this->form['buttons']['submit']['class'] = 'invisible';
            $this->form['buttons']['back']['content'] = 'KEMBALI';
        }
        if ($action == 'edit') if ($this->model->update($id, $data) === false) $this->data['errors'] = $this->model->errors();
        else $this->form['buttons']['back']['content'] = 'KEMBALI';
        foreach ($this->data['errors'] as $key => $value) foreach ($this->form as $key_ => $item) if (!empty($this->form[$key_]['name'])) if ($this->form[$key_]['name'] == $key) $this->form[$key_]['class'] .= ' is-invalid';
        foreach ($data as $key => $value) $this->form[$key]['value'] = $data[$key];
        $this->template($this->pathTemplate . 'form');
    }

    public function Delete($id)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        if ($this->request->isAJAX()) {
            $this->model->delete($id);
            $json['token'] = csrf_hash();
            return $this->response->setJSON($json);
        }
    }

    public function Show($id)
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        foreach ($this->model->find($id) as $key => $value) $this->form[$key]['value'] = $value;
        $this->form['buttons']['back']['content'] = 'KEMBALI';
        $this->form['buttons']['edit']['onClick'] = "form_edit($id)";
        $this->form['buttons']['delete']['onClick'] = "form_delete($id,true)";
        $this->template($this->pathTemplate . 'show');
    }

    public function Table()
    {
        if (!empty($this->permission)) if (!has_permission($this->permission)) return redirect()->route('/');
        if ($this->request->isAJAX()) {
            $datatables = new Datatables();
            $datatables->SetDB('DBMaster');
            $datatables
                ->from('tindakan a')
                ->select("a.id AS id
            ,a.kode_panggil AS kode_panggil
            ,a.id_kelompok AS id_kelompok
            ,b.nama AS kelompok
            ,a.nama AS nama
            ,a.type_kelas AS type_kelas
            ,tarif_3 , tarif_2 , tarif_1 , tarif_vip
            ")
                ->join('tindakan_kelompok b', 'a.id_kelompok=b.id', 'left')
                ->att_column('DT_RowId', '$1', 'id');
            $data['result'] = $datatables->generate();
            $json = json_decode($data['result'], true);
            $json['token'] = csrf_hash();
            return $this->response->setJSON($json);
        }
    }
}
